$(function(){


    /* |--------------------| */
    /* |-- FORM VALIDATOR --| */
    /* |--------------------| */

    var contactValidator = function(){
        this.email = 0;
        this.subject = 0;
        this.message = 0;
    };

    contactValidator = new contactValidator();


    /* |--------------------------| */
    /* |-- FORM VALIDATOR CHECK --| */
    /* |--------------------------| */

    function contactValidCheck(){
        var validator = true;

        for(index in contactValidator){
            if(contactValidator[index] == 0){
                validator = false;
            };
        };

        if(validator == true){
            $('#submit').css('background-color','#50A058');
        }else{
            $('#submit').css('background-color','#9B3937');
        };

        return validator;
    };


    /* |-----------------| */
    /* |-- EMAIL FIELD --| */
    /* |-----------------| */

    function emailCheck(){
        var data = $('#contact_email').serialize();

        $.post(
            'ajax/form_contact.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#contact_email').css('border','1px #50A058 solid');
                    contactValidator.email = 1;
                    $('#contact_email-error').fadeTo(400,0, function(){$('#contact_email-error').remove();});
                }else{
                    contactValidator.email = 0;
                    $('#contact_email').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#contact_email').on('input keyup change',function(){

        /* Connected member email */
        if(membreEmail != ''){
            $('#contact_email').val(membreEmail);
        }
        emailCheck();

        window.setTimeout(function(){
            contactValidCheck();
        },200)
    });



    /* Connected member email */

    if(membreEmail != ''){
        window.setTimeout(function(){ emailCheck();}, 600);
    }

    /* |-------------------| */
    /* |-- SUBJECT FIELD --| */
    /* |-------------------| */

    function subjectCheck(){
        var data = $('#contact_subject').serialize();

        $.post(
            'ajax/form_contact.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#contact_subject').css('border','1px #50A058 solid');
                    contactValidator.subject = 1;
                    $('#contact_subject-error').fadeTo(400,0, function(){$('#contact_subject-error').remove();});
                }else{
                    contactValidator.subject = 0;
                    $('#contact_subject').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#contact_subject').on('input keyup change',function(){
        subjectCheck();

        window.setTimeout(function(){
            contactValidCheck();
        },200)
    });


    /* |-------------------| */
    /* |-- MESSAGE FIELD --| */
    /* |-------------------| */

    function messageCheck(){
        var data = $('#contact_message').serialize();

        $.post(
            'ajax/form_contact.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#contact_message').css('border','1px #50A058 solid');
                    contactValidator.message = 1;
                    $('#contact_message-error').fadeTo(400,0, function(){$('#contact_message-error').remove();});
                }else{
                    contactValidator.message = 0;
                    $('#contact_message').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#contact_message').on('input keyup change',function(){
        messageCheck();

        window.setTimeout(function(){
            contactValidCheck();
        },200)
    });


    /* |-------------------| */
    /* |-- SUBMIT BUTTON --| */
    /* |-------------------| */

    var errorRemove;

    $('#submit').click(function(){

        if(contactValidCheck()== true){
            $('#form_contact').submit();
        }else{
            for(index in contactValidator){

                var selector = $('#contact_'+index);
                var msg = '';

                switch(index) {
                    case 'email':
                        msg = 'Votre email est incorrect.'; break;
                    case 'subject':
                        msg = 'Votre objet est incorrect.'; break;
                    case 'message':
                        msg = 'Votre message est trop court.'; break;
                }

                if(contactValidator[index] == 0){
                    selector.css("border","1px red solid");
                    $('#contact_'+index+'-error').remove();
                    window.clearTimeout(errorRemove);
                    selector.after('<div class="error" id="contact_'+index +'-error"></div>');
                    $('.error').fadeTo(400,1);
                    $('#contact_' +index +'-error').html('<div class="error-arrow"></div>' +msg);
                }else{
                    selector.css('border','1px #50A058 solid');
                }
            };

            errorRemove = window.setTimeout(function(){
                $('.error').each(function(){
                    $(this).fadeTo(600, 0, function(){
                        $(this).remove();
                    });
                });
            },5000);

        };

    });




    /* |---------------------------| */
    /* |-- MSG SENT CONFIRMATION --| */
    /* |---------------------------| */

    if(messageSent == true){
        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-contact"><p>Merci, votre message a été envoyé !<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-contact').fadeTo(500,1);
    }

    $('#close').click(function(){
        $('.dark-bg').fadeTo(400,0,function() {
            $('.dark-bg').remove();
        });
        $('#popin-contact').fadeTo(400,0,function() {
            $('#popin-contact').remove();
        });

    });

    $('.continuer').click(function(){
        $('.dark-bg').fadeTo(400,0,function(){
            $('.dark-bg').remove();
        });
        $('#popin-contact').fadeTo(400,0,function() {
            $('#popin-contact').remove();
        });
    });

});