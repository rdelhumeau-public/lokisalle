$(function(){

    /* |---------------------| */
    /* |-- INTRO ANIMATION --| */
    /* |---------------------| */


    if(introAnimation) {
        $('#left-block').css('left',470);
        $('#left-block').animate({
            left: 22
        }, 800);
    };


    /* |-------------------| */
    /* |-- PHOTO PREVIEW --| */
    /* |-------------------| */

    var animationTermine;

    $('#preview').hover(function(){
        $(this).prepend('<div class="photo-hover"><img src="/img/icons/search.png" alt=""></div>');
        $('.photo-hover').fadeTo(500,1, function(){
            animationTermine = true;
        });
    }, function(){
        $('.photo-hover').stop(true,true);

        if(animationTermine){
            $('.photo-hover').fadeTo(400, 0, function () {
                $('.photo-hover').remove();
            });
        }else{
            $('.photo-hover').remove();
        }
        animationTermine = false;
    });


    /* |------------------------| */
    /* |-- PHOTO PREVIEW EXIT --| */
    /* |------------------------| */

    $('#preview').click(function(){
        $('#zoom-preview').css('display', 'block');
        $('.dark-bg').fadeTo(600,1);
    });

    $('.dark-bg').click(function(){
        $('.dark-bg').fadeTo(600,0, function(){
            $('#zoom-preview').css('display','none');
        });
    });


    /* |----------------| */
    /* |-- PANIER ADD --| */
    /* |----------------| */

    var validator = false;
    var errorRemove;

    $('form input').change(function(){
        validator = true;
        $('.error').fadeTo(600, 0, function(){
            $('.error').remove();
        });
        window.clearTimeout(errorRemove);
        $('#submit').css('background-color', '#50A058');
    });

    $('#submit').click(function(){
        if(!validator){
            $('#offre-error').remove();
            window.clearTimeout(errorRemove);
            $('#submit').append('<div class="error" id="offre-error"></div>');
            $('#offre-error').fadeTo(400,1);
            $('#offre-error').html('<div class="error-arrow"></div>Veuillez choisir une offre.');
        }else{
            $('#add-form').submit();
        };

        errorRemove = window.setTimeout(function(){
            $('#offre-error').fadeTo(600, 0, function(){
                $('#offre-error').remove();
            });
        },5000);
    });


    /* |-----------| */
    /* |-- NOTES --| */
    /* |-----------| */

    $('#avis_note').change(function(){
        $('#current-note').html($(this).val());
    });




/* |---------------| */
/* |-- AVIS FORM --| */
/* |---------------| */



    /* |-----------------| */
    /* |-- AVIS FIELD ---| */
    /* |-----------------| */

    var avisValidator = false;

    function avisCheck(){
        var data = $('#avis_texte').serialize();

        $.post(
            '/ajax/form_avis.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#avis_texte').css('border','1px #50A058 solid');
                    avisValidator = true;
                    $('#avis_texte-error').fadeTo(400,0, function(){$('#avis_texte-error').remove();});
                }else{
                    avisValidator = false;
                    $('#avis_texte').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#avis_texte').on('keyup',function(){
        avisCheck();

        window.setTimeout(function(){
            if(avisValidator){
                $('#avis_submit').css('background-color','#50A058');
            }else{
                $('#avis_submit').css('background-color','#87523F');
            }
        },200);
    });


    /* |-------------------| */
    /* |-- SUBMIT BUTTON --| */
    /* |-------------------| */

    var errorRemoveAvis;

    $('#avis_submit').click(function(){

        if(avisValidator){
            $('#avis_form').submit();
        }else{
            var msg = 'Votre message est trop court ou contient des caractères interdits.';

            $('#avis_texte').css("border","1px red solid");
            $('#avis_texte-error').remove();
            window.clearTimeout(errorRemoveAvis);
            $('#avis_texte').after('<div class="error" id="avis_texte-error"></div>');
            $('#avis_texte-error').fadeTo(400,1);
            $('#avis_texte-error').html('<div class="error-arrow"></div>' +msg);
        };

        errorRemoveAvis = window.setTimeout(function(){
            $('#avis_texte-error').fadeTo(600, 0, function(){
                $('#avis_texte-error').remove();
                });
        },5000);

    });


});
