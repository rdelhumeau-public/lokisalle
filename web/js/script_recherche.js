$(function(){

/* |--------------------| */
/* |-- BLOC RECHERCHE --| */
/* |--------------------| */

    $('#filtre_'+filtreDefaut).css('display','block');
    $('#filtre_'+filtreDefaut).css('opacity',1);

    $('input[type=radio]').change(function(){
        var filtre = $(this).attr('id');

        $('.filtre-dynamique').each(function(){
            $(this).fadeOut();
        });
        $('#filtre_' +filtre).fadeIn();
    });


    $('#submit').click(function(){

        $('#search_form').submit();
    });

    /* |--------------------| */
    /* |-- ENTER KEYBOARD --| */
    /* |--------------------| */

    $('body').keyup(function(e) {
        if(e.keyCode == 13) {
            $('#submit').trigger('click');
        }
    });


/* |---------------| */
/* |-- RESULTATS --| */
/* |---------------| */

    //list pages

    $('#page1').css('display', 'block');
    $('#page-link1').css('font-weight','bold');

    var currentpage = 1;
    var pageid;
    $('.page-link').click(function(){
        pageid = $(this).attr('id').replace('page-link', '');

        if(pageid != currentpage) {
            $('#page' + currentpage).fadeOut(200, function () {
                $('#page' + pageid).fadeIn(200);
            });

            $('#page-link' + currentpage).css('font-weight', 'normal');
            $('#page-link' + pageid).css('font-weight', 'bold');
            currentpage = pageid;
        };
    });


    //rooms details previews

    var room, id;

    $('.room-list-selection').hover(function(){
        $('#back').stop(true,true);
        $('#back').fadeTo(2,0);

        id = $(this).attr('id').replace('list', '');
        $(this).prepend('<img src="/img/icons/panier_add_white_big.png" alt="" class="panier" id="panier'+ id +'">');

        room = '#room' + id;
        $(room).fadeTo(450, 1);
    }, function(){
        $('#panier'+id).remove();
        $(room).stop(true,true);
        $(room).fadeTo(350, 0);
        $('#back').fadeTo(700,1);
    });

});