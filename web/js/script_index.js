$(function(){

/* |--------------------| */
/* |-- HOME ANIMATION --| */
/* |--------------------| */

    if(first_visit == true) {
        $('#logo').css('opacity', 0).fadeTo(1550, 1);
        $('#legend').css('opacity', 0).fadeTo(100, 1);
        $('#legend').css('left', 75);
        $('#legend').animate({left: 150}, 900);
    }


/* |-----------------| */
/* |-- RAPH SLIDER --| */
/* |-----------------| */

    //init
    $('#slider1').fadeTo(900, 1);
    $('#text1').fadeTo(900, 1);
    $('#btn-slide1').fadeTo(900, 1);


    var boucle1;
    var boucle2;

    var slide = function(){
        this.n = 1;
    };

    function sliderIn(n){

        if(n==4){n=1;};
        slide.n = n;

        $('#slider' + n).fadeTo(2000, 1);
        $('#text' + n).fadeTo(2000, 1);
        $('#btn-slide' + n).fadeTo(2000, 1);
        boucle1 = window.setTimeout(function(){sliderOut(n)}, 7500);

    };

    function sliderOut(n){
        $('#slider'+n).fadeTo(2000,0);
        $('#text'+n).fadeTo(1500,0);
        $('#btn-slide' + n).fadeTo(1500, 0);
        n+=1;
        boucle2 = window.setTimeout(function(){sliderIn(n)},10);
    };

    sliderIn(1);

    $('#click-button1').click(function(){
        if(slide.n!=1) {
            window.clearTimeout(boucle1);
            window.clearTimeout(boucle2);

            for (var i = 1; i < 4; i++) {
                $('#slider' + i).css('display', 'none');
                $('#text' + i).css('display', 'none');
                $('#btn-slide' + i).css('display', 'none');
            }

            $('#slider1').fadeTo(10, 1);
            $('#text1').fadeTo(10, 1);
            $('#btn-slide1').fadeTo(10, 1);

            sliderIn(1);
        };
    });

    $('#click-button2').click(function(){
        if(slide.n!=2) {
            window.clearTimeout(boucle1);
            window.clearTimeout(boucle2);

            for (var i = 1; i < 4; i++) {
                $('#slider' + i).css('display', 'none');
                $('#text' + i).css('display', 'none');
                $('#btn-slide' + i).css('display', 'none');
            }

            $('#slider2').fadeTo(10, 1);
            $('#text2').fadeTo(10, 1);
            $('#btn-slide2').fadeTo(10, 1);

            sliderIn(2);
        };
    });

    $('#click-button3').click(function(){
        if(slide.n!=3) {
            window.clearTimeout(boucle1);
            window.clearTimeout(boucle2);

            for (var i = 1; i < 4; i++) {
                $('#slider' + i).css('display', 'none');
                $('#text' + i).css('display', 'none');
                $('#btn-slide' + i).css('display', 'none');
            }

            $('#slider3').fadeTo(10, 1);
            $('#text3').fadeTo(10, 1);
            $('#btn-slide3').fadeTo(10, 1);

            sliderIn(3);
        };
    });



/* |-----------------| */
/* |-- MDP CHANGED --| */
/* |-----------------| */


    if(mdp_changed == true){
        $('body').prepend('<div class="dark-bg"></div>');
        $('#content').prepend('<div id="popin-index"><p>Votre nouveau mot de passe vous a été envoyé par email !<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-index').fadeTo(500,1);
    }

    $('#close').click(function(){
        $('.dark-bg').fadeTo(400,0, function(){
            $('.dark-bg').remove();
        });
        $('#popin-index').fadeTo(400,0, function(){
            $('#popin-index').remove();
        });
    });

    $('.continuer').click(function(){
        $('.dark-bg').fadeTo(400,0, function(){
            $('.dark-bg').remove();
        });
        $('#popin-index').fadeTo(400,0, function(){
            $('#popin-index').remove();
        });
    });


    /* |----------------------------------| */
    /* |-- MDP CHANGE OPERATION EXPIRED --| */
    /* |----------------------------------| */


    if(mdp_session_expired == true){
        $('body').prepend('<div class="dark-bg"></div>');
        $('#content').prepend('<div id="popin-index"><p>La session nécessaire à cette opération a expiré. Veuillez renouveler votre demande.<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-index').fadeTo(500,1);
    }

    $('#close').click(function(){
        $('.dark-bg').fadeTo(400,0, function(){
            $('.dark-bg').remove();
        });
        $('#popin-index').fadeTo(400,0, function(){
            $('#popin-index').remove();
        });
    });

    $('.continuer').click(function(){
        $('.dark-bg').fadeTo(400,0, function(){
            $('.dark-bg').remove();
        });
        $('#popin-index').fadeTo(400,0, function(){
            $('#popin-index').remove();
        });
    });



    /* |----------------------------| */
    /* |-- NEWSLETTER INSCRIPTION --| */
    /* |----------------------------| */

    if(newsletter){
        $('body').prepend('<div class="dark-bg"></div>');
        $('#content').prepend('<div id="popin-global"><p>Merci pour votre inscription !<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-global').fadeTo(500,1);

        $('#close, .continuer').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('#popin-global').fadeTo(400,0,function() {
                $('#popin-global').remove();
            });

        });
    }


});