$(function(){

/* |-----------------------| */
/* |-- INSCRIPTION ENDED --| */
/* |-----------------------| */

    if(inscriptionValid == true){
        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-inscription"><p><img src="' +path +'img/icons/valid.png" alt="" class="icon2">Inscription réussie !<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(1,1);
        $('#popin-inscription').fadeTo(500,1);
    }

    $('#close').click(function(){
        $('.dark-bg').fadeTo(400,0, function(){
            $('.dark-bg').remove();
        });
        $('#popin-inscription').fadeTo(400,0, function(){
            $('#popin-inscription').remove();
        });
    });

    $('.continuer').click(function(){
        $('.dark-bg').fadeTo(400,0, function(){
            $('.dark-bg').remove();
        });
        $('#popin-inscription').fadeTo(400,0, function(){
            $('#popin-inscription').remove();
        });
    });



/* |-----------------| */
/* |-- EDIT BUTTON --| */
/* |-----------------| */

    $('#edit-profil').hover(function(){
        $('#edit-profil img').attr('src', path +'img/icons/small_edit_hover.png');
    }, function(){
        $('#edit-profil img').attr('src', path +'img/icons/small_edit.png');
    });

    $('#edit-profil').click(function(){
        $('body').prepend('<div class="dark-bg"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-profil').css('display', 'block');
        $('#popin-profil').fadeTo(500,1);

        $('#close').click(function(){
            $('#popin-profil').fadeTo(360,0, function(){
                $('#popin-profil').css('display','none');

                if(editValidator.ancien_mdp == 0) {
                    window.location = '/profil';
                };
            });
        });

    });

/* |--------------------| */
/* |-- OTHERS BUTTONS --| */
/* |--------------------| */

    $('#panier-link').hover(function(){
        $('#panier-link img').attr('src', path +'img/icons/small_panier_hover.png');
    }, function(){
        $('#panier-link img').attr('src', path +'img/icons/small_panier_black.png');
    });

    $('#deco-link').hover(function(){
        $('#deco-link img').attr('src', path +'img/icons/small_logout_hover.png');
    }, function(){
        $('#deco-link img').attr('src', path +'img/icons/small_logout.png');
    });





    //-- password auto completion remove
    window.setTimeout(function() {
        $('#edit_ancien_mdp').val('');
        $('#edit_nouveau_mdp').val('');
        $('#edit_nouveau_mdpConfirmation').val('');
    }, 650);



/* |--------------------| */
/* |-- EDIT FEATURES ---| */
/* |--------------------| */


    /* |-------------------| */
    /* |-- FORM VALIDATOR--| */
    /* |-------------------| */

    var editValidator = function(){
        this.ville = 0;
        this.cp = 0;
        this.adresse = 0;
        this.email = 0;
        this.ancien_mdp = 1;
        this.nouveau_mdp = 1;
        this.nouveau_mdpConfirmation = 1;
    };

    editValidator = new editValidator();


    /* |--------------------------| */
    /* |-- FORM VALIDATOR CHECK --| */
    /* |--------------------------| */

    function editValidCheck(){
        var validator = true;

        for(index in editValidator){
            if(editValidator[index] == 0){
                validator = false;
            };
        };

        if(validator == true){
            $('#submit').css('background-color','#50A058');
        }else{
            $('#submit').css('background-color','#9B3937');
        };

        return validator;
    };

    /* |-----------------| */
    /* |-- VILLE FIELD --| */
    /* |-----------------| */

    function villeCheck(){
        var data = $('#edit_ville').serialize();

        $.post(
            'ajax/form_profiledit.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#edit_ville').css('border','1px #50A058 solid');
                    editValidator.ville = 1;
                    $('#edit_ville-error').fadeTo(400,0, function(){$('#edit_ville-error').remove();});
                }else{
                    editValidator.ville = 0;
                    $('#edit_ville').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#edit_ville').on('input keyup change',function(){
        villeCheck();

        window.setTimeout(function(){
            editValidCheck();
        },200);
    });


    /* |-----------------------| */
    /* |-- CODE POSTAL FIELD --| */
    /* |-----------------------| */

    function cpCheck(){
        var data = $('#edit_cp').serialize();

        $.post(
            'ajax/form_profiledit.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#edit_cp').css('border','1px #50A058 solid');
                    editValidator.cp = 1;
                    $('#edit_cp-error').fadeTo(400,0, function(){$('#edit_cp-error').remove();});
                }else{
                    editValidator.cp = 0;
                    $('#edit_cp').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#edit_cp').on('input keyup change',function(){
        cpCheck();

        window.setTimeout(function(){
            editValidCheck();
        },200);
    });


    /* |-------------------| */
    /* |-- ADRESSE FIELD --| */
    /* |-------------------| */

    function adresseCheck(){
        var data = $('#edit_adresse').serialize();

        $.post(
            'ajax/form_profiledit.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#edit_adresse').css('border','1px #50A058 solid');
                    editValidator.adresse = 1;
                    $('#edit_adresse-error').fadeTo(400,0, function(){$('#edit_adresse-error').remove();});
                }else{
                    editValidator.adresse = 0;
                    $('#edit_adresse').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#edit_adresse').on('input keyup change',function(){
        adresseCheck();

        window.setTimeout(function(){
            editValidCheck();
        },200);
    });


    /* |-----------------| */
    /* |-- EMAIL FIELD --| */
    /* |-----------------| */

    function emailCheck(){
        var data = [
            $('#edit_email').val(),
            $('#membre_id').val()
            ];

        $.post(
            'ajax/form_profiledit.php',
            {email:data},
            function(value){

                if(value == 'success'){
                    $('#edit_email').css('border','1px #50A058 solid');
                    editValidator.email = 1;
                    $('#edit_email-error').fadeTo(400,0, function(){$('#edit_email-error').remove();});
                }else{
                    editValidator.email = 0;
                    $('#edit_email').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#edit_email').on('input keyup change',function(){
        emailCheck();

        window.setTimeout(function(){
            editValidCheck();
        },200);
    });



    /* |---------------| */
    /* |-- MDP BLOCK --| */
    /* |---------------| */

    $('#changeMdpLink').click(function(){
        $('#changeMdpLink').fadeTo(300,0, function(){
            $('#changeMdpLink').remove();
            $('#changeMdp').css('display','block');
            $('#changeMdp').fadeTo(300,1);
            editValidator.ancien_mdp = 0;
            editValidator.nouveau_mdp = 0;
            editValidator.nouveau_mdpConfirmation = 0;

            window.setTimeout(function(){
                editValidCheck();
            },200);
        });
    });


    /* |---------------| */
    /* |-- MDP FIELD --| */
    /* |---------------| */

    function ancienMdpCheck(){
        var data = [
            $('#edit_ancien_mdp').val(),
            $('#membre_id').val()
        ];

        $.post(
            'ajax/form_profiledit.php',
            {membre:data},
            function(value){

                if(value == 'success'){
                    $('#edit_ancien_mdp').css('border','1px #50A058 solid');
                    editValidator.ancien_mdp = 1;
                    $('#edit_ancien_mdp-error').fadeTo(400,0, function(){$('#edit_ancien_mdp-error').remove();});
                }else{
                    editValidator.ancien_mdp = 0;
                    $('#edit_ancien_mdp').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#edit_ancien_mdp').on('input keyup change',function(){
        ancienMdpCheck();

        window.setTimeout(function(){
            editValidCheck();
        },200);
    });


    /* |------------------------| */
    /* |--  NOUVEAU MDP FIELD --| */
    /* |------------------------| */

    function nouveauMdpCheck(){

        if($('#edit_nouveau_mdp').val().length >= 3){
            $('#edit_nouveau_mdp').css('border','1px #50A058 solid');
            editValidator.nouveau_mdp = 1;
            $('#edit_nouveau_mdp-error').fadeTo(400,0, function(){$('#edit_nouveau_mdp-error').remove();});
        }else{
            editValidator.nouveau_mdp = 0;
            $('#edit_nouveau_mdp').css('border','1px #aaaeaf solid');
        };

    };

    $('#edit_nouveau_mdp').on('input keyup change',function(){
        nouveauMdpCheck();
        nouveauMdpConfirmationCheck();

        window.setTimeout(function(){
            editValidCheck();
        },200);
    });


    /* |------------------------------------| */
    /* |-- NOUVEAU MDP CONFIRMATION FIELD --| */
    /* |------------------------------------| */

    function nouveauMdpConfirmationCheck(){

        if($('#edit_nouveau_mdpConfirmation').val() == $('#edit_nouveau_mdp').val()){
            $('#edit_nouveau_mdpConfirmation').css('border','1px #50A058 solid');
            editValidator.nouveau_mdpConfirmation = 1;
            $('#edit_nouveau_mdpConfirmation-error').fadeTo(400,0, function(){$('#edit_nouveau_mdpConfirmation-error').remove();});
        }else{
            editValidator.nouveau_mdpConfirmation = 0;
            $('#edit_nouveau_mdpConfirmation').css('border','1px #aaaeaf solid');
        };

    };

    $('#edit_nouveau_mdpConfirmation').on('input keyup change',function(){
        nouveauMdpConfirmationCheck();

        window.setTimeout(function(){
            editValidCheck();
        },200);
    });


    /* |----------------| */
    /* |-- AUTO CHECK --| */
    /* |----------------| */

    window.setTimeout(function(){
        villeCheck();
        cpCheck();
        adresseCheck();
        emailCheck();

        window.setTimeout(function(){
            editValidCheck();
        },200);
    }, 900);



    /* |-------------------| */
    /* |-- SUBMIT BUTTON --| */
    /* |-------------------| */

    var errorRemove;

    $('#submit').click(function(){

        if(editValidCheck()== true){
            $('#form_profiledit').submit();
        }else{
            for(index in editValidator){

                var selector = $('#edit_'+index);
                var msg = '';

                switch(index) {
                    case 'ville':
                        msg = 'Votre ville ne doit contenir que des lettres.'; break;
                    case 'cp':
                        msg = 'Votre code postal est incorrect.'; break;
                    case 'adresse':
                        msg = 'Votre adresse est trop courte.'; break;
                    case 'email':
                        msg = 'Votre email est incorrect ou a déjà été enregistré sur notre site.'; break;
                    case 'ancien_mdp':
                        msg = 'Mot de passe incorrect.'; break;
                    case 'nouveau_mdp':
                        msg = 'Votre mot de passe doit contenir au moins 3 caractères.'; break;
                    case 'nouveau_mdpConfirmation':
                        msg = 'Vos mots de passe ne correspondent pas.'; break;
                }

                if(editValidator[index] == 0){
                    selector.css("border","1px red solid");
                    $('#edit_'+index+'-error').remove();
                    window.clearTimeout(errorRemove);
                    selector.after('<div class="error" id="edit_'+index +'-error"></div>');
                    $('.error').fadeTo(400,1);
                    $('#edit_' +index +'-error').html('<div class="error-arrow"></div>' +msg);

                }else{
                    selector.css('border','1px #50A058 solid');
                }
            };

            errorRemove = window.setTimeout(function(){
                $('.error').each(function(){
                    $(this).fadeTo(600, 0, function(){
                        $(this).remove();
                    });
                });
            },5000);

        };

    });


    /* |--------------------| */
    /* |-- ENTER KEYBOARD --| */
    /* |--------------------| */

    $('#form_profiledit').keyup(function(e) {
        if(e.keyCode == 13) {
            $('#submit').trigger('click');
        }
    });

});