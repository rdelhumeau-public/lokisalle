
$(function() {


/* |----------------| */
/* |-- RIGHT MENU --| */
/* |----------------| */


    $('#subscribe').hover(function(){
        $('#subscribe img').attr('src', path +'img/icons/subscribe_hover.png');
    }, function(){
        $('#subscribe img').attr('src', path +'img/icons/subscribe.png');
    });

    $('#login').hover(function(){
        $('#login img').attr('src', path +'img/icons/login_hover.png');
    }, function(){
        $('#login img').attr('src', path +'img/icons/login.png');
    });

    $('#logout').hover(function(){
        $('#logout img').attr('src', path +'img/icons/logout_hover.png');
    }, function(){
        $('#logout img').attr('src', path +'img/icons/logout.png');
    });

    $('#profil').hover(function(){
        $('#profil img').attr('src', path +'img/icons/profile_hover.png');
    }, function(){
        $('#profil img').attr('src', path +'img/icons/profile.png');
    });



/* |-----------------| */
/* |-- SCROLL TO UP--| */
/* |-----------------| */

    $('.scrollTo').click( function() {
        var page = $(this).attr('href');
        $('html, body').animate( {
            scrollTop: $(page).offset().top
        }, 350 );
        return false;
    });


/* |----------------| */
/* |-- PRINT PAGE --| */
/* |----------------| */


    $('#print').click(function(){
        window.print();
    });


});
