$(function() {

    /* |------------| */
    /* |-- POPIN  --| */
    /* |------------| */

    $('body').prepend('<div class="dark-bg"></div>');
    $('.dark-bg').fadeTo(25, 1);
    $('#popin-oubli').fadeTo(500, 1);




    /* |-------------------| */
    /* |-- FORM VALIDATOR--| */
    /* |-------------------| */

    var oubliValidator = function(){
        this.email = 0;
    };

    oubliValidator = new oubliValidator();



    /* |--------------------------| */
    /* |-- FORM VALIDATOR CHECK --| */
    /* |--------------------------| */

    function oubliValidCheck(){

        validator = false;

        if(oubliValidator.email == 1){
            $('#submit').css('background-color','#50A058');
            validator = true;
        }else{
            $('#submit').css('background-color','#9B3937');
        };

        return validator;
    };


    /* |-----------------| */
    /* |-- EMAIL FIELD --| */
    /* |-----------------| */

    function emailCheck(){
        var data = $('#oubli_email').serialize();

        $.post(
            'ajax/form_oubli.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#oubli_email').css('border','1px #50A058 solid');
                    oubliValidator.email = 1;
                    $('#oubli_email-error').fadeTo(400,0, function(){$('#oubli_email-error').remove();});
                }else{
                    oubliValidator.email = 0;
                    $('#oubli_email').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#oubli_email').on('input keyup change',function(){
        emailCheck();

        window.setTimeout(function(){
            oubliValidCheck();
        },200);
    });




    /* |-------------------| */
    /* |-- SUBMIT BUTTON --| */
    /* |-------------------| */

    var errorRemove;

    $('#submit').click(function(){

        if(oubliValidCheck()== true){
            $('#form_oubli').submit();
        }else{

            var selector = $('#oubli_email');
            var msg = 'Votre email n\'est pas enregistré sur notre site';

            if(oubliValidator.email == 0){
                selector.css("border","1px red solid");
                $('#oubli_email-error').remove();
                window.clearTimeout(errorRemove);
                selector.after('<div class="error" id="oubli_email-error"></div>');
                $('.error').fadeTo(400,1);
                $('#oubli_email-error').html('<div class="error-arrow"></div>' +msg);

            }else{
                selector.css('border','1px #50A058 solid');
            }

        };

        errorRemove = window.setTimeout(function(){
            $('.error').each(function(){
                $(this).fadeTo(600, 0, function(){
                    $(this).remove();
                });
            });
        },5000);

    });


    /* |--------------------| */
    /* |-- ENTER KEYBOARD --| */
    /* |--------------------| */

    $('body').keyup(function(e) {
        if(e.keyCode == 13) {
            $('#submit').trigger('click');
        }
    });

    /* |----------------| */
    /* |-- AUTO FOCUS --| */
    /* |----------------| */

    $('#oubli_email').select();


});