$(function(){

/* |--------------------| */
/* |-- GESTION SALLES --| */
/* |--------------------| */

    $('.supprSalle').click(function(){
        salle = $(this).attr('id').replace('salle', '');

        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-global"><p>Êtes-vous sûr de vouloir<br> supprimer cette salle : <strong>'+salle +'</strong> ?<br><a href="' +path +'admin/gestion-salles/supprimer/'+salle +'"><div class="oui">Oui</div></a><div class="non">Non</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-global').fadeTo(500,1);

        $('#close, .non').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('#popin-global').fadeTo(400,0,function() {
                $('#popin-global').remove();
            });

        });

    });


    if(typeof editerSalle!= 'undefined' && editerSalle){
        $('body').prepend('<div class="dark-bg"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('.popin-salle').fadeTo(500,1);

        $('.popin-salle #close').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('.popin-salle').fadeTo(400,0,function() {
                $('.popin-salle').remove();
            });

        });
    }



    $('.popin-salle #submit').click(function(){
        $('#form-edit').submit();
    });

    $('.popin-ajouter #submit').click(function(){
        $('#form-add').submit();
    });


    if(typeof editerror != 'undefined' && editerror){
        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-global"><p>Une erreur est survenue.<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-global').fadeTo(500,1);

        $('#close, .continuer').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('#popin-global').fadeTo(400,0,function() {
                $('#popin-global').remove();
            });

        });
    }



    $('#ajouter').click(function(){
        $('body').prepend('<div class="dark-bg"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('.popin-ajouter').fadeTo(500,1);

        $('#close').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('.popin-ajouter').fadeTo(400,0,function() {
                $('.popin-ajouter').css('display', 'none');
            });

        });
    });



/* |----------------------| */
/* |-- GESTION PRODUITS --| */
/* |----------------------| */


    $('.supprProduit').click(function(){
        idproduit = $(this).attr('id').replace('produit', '');

        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-global"><p>Êtes-vous sûr de vouloir<br> supprimer ce produit ?<br><a href="' +path +'admin/gestion-produits/supprimer/'+idproduit +'"><div class="oui">Oui</div></a><div class="non">Non</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-global').fadeTo(500,1);

        $('#close, .non').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('#popin-global').fadeTo(400,0,function() {
                $('#popin-global').remove();
            });

        });

    });



    if(typeof editerProduit!= 'undefined' && editerProduit){
        $('body').prepend('<div class="dark-bg"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('.popin-produit').fadeTo(500,1);

        $('.popin-produit #close').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('.popin-produit').fadeTo(400,0,function() {
                $('.popin-produit').remove();
            });

        });
    }




    $('.popin-produit #submit').click(function(){
        $('#form-edit-produit').submit();
    });

    $('.popin-produit-ajouter #submit').click(function(){
        $('#form-add-produit').submit();
    });



/* |-----------------| */
/* |-- DATE PICKER --| */
/* |-----------------| */

    (function( factory ) {
        factory( jQuery.datepicker );

    }(function( datepicker ) {

        datepicker.regional['fr'] = {
            closeText: 'Fermer',
            prevText: 'Précédent',
            nextText: 'Suivant',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
                'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['janv.', 'févr.', 'mars', 'avr.', 'mai', 'juin',
                'juil.', 'août', 'sept.', 'oct.', 'nov.', 'déc.'],
            dayNames: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
            dayNamesShort: ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'],
            dayNamesMin: ['D','L','M','M','J','V','S'],
            weekHeader: 'Sem.',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''};
        datepicker.setDefaults(datepicker.regional['fr']);

        return datepicker.regional['fr'];

    }));

    $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );


    if(typeof annee != 'undefined') {

        var annees = annee + ':' + (annee + 1);

        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: annees,
            defaultDate: date
        });

    }




    $('#ajouterProduit').click(function(){
        $('body').prepend('<div class="dark-bg"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('.popin-produit-ajouter').fadeTo(500,1);

        $('#close').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('.popin-produit-ajouter').fadeTo(400,0,function() {
                $('.popin-produit-ajouter').css('display', 'none');
            });

        });
    });






    /* |---------------------| */
    /* |-- GESTION MEMBRES --| */
    /* |---------------------| */


    $('.supprMembre').click(function(){
        pseudo = $(this).attr('id').replace('membre', '');

        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-global"><p>Êtes-vous sûr de vouloir<br> supprimer ce membre : <strong>' +pseudo +'</strong> ?<br><a href="' +path +'admin/gestion-membres/supprimer/'+pseudo +'"><div class="oui">Oui</div></a><div class="non">Non</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-global').fadeTo(500,1);

        $('#close, .non').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('#popin-global').fadeTo(400,0,function() {
                $('#popin-global').remove();
            });

        });

    });



    $('#addAdmin').click(function(){
        $('body').prepend('<div class="dark-bg"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('.popin-membre-admin').fadeTo(500,1);

        $('#close').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('.popin-membre-admin').fadeTo(400,0,function() {
                $('.popin-membre-admin').css('display', 'none');
            });

        });
    });


    $('.popin-membre-admin #submit').click(function(){
        $('#form-addadmin').submit();
    });



    /* |------------------| */
    /* |-- GESTION AVIS --| */
    /* |------------------| */



    $('.supprAvis').click(function(){
        idavis = $(this).attr('id').replace('avis', '');

        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-global"><p>Êtes-vous sûr de vouloir<br> supprimer cet avis ?<br><a href="' +path +'admin/gestion-avis/supprimer/'+idavis +'"><div class="oui">Oui</div></a><div class="non">Non</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-global').fadeTo(500,1);

        $('#close, .non').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('#popin-global').fadeTo(400,0,function() {
                $('#popin-global').remove();
            });

        });

    });


    /* |----------------| */
    /* |-- NEWSLETTER --| */
    /* |----------------| */

    if(typeof envoi != 'undefined' && envoi){
        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-global"><p>Newsletter envoyée !<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-global').fadeTo(500,1);

        $('#close, .continuer').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('#popin-global').fadeTo(400,0,function() {
                $('#popin-global').remove();
            });

        });
    }


});