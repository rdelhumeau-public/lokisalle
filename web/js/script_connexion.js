$(function() {

    /* |------------| */
    /* |-- POPIN  --| */
    /* |------------| */

    $('body').prepend('<div class="dark-bg"></div>');
    $('.dark-bg').fadeTo(500, 1);
    $('#popin-connexion').fadeTo(500, 1);


    /* |-------------------| */
    /* |-- FORM VALIDATOR--| */
    /* |-------------------| */

    var connexionValidator = function(){
        this.pseudo = 0;
        this.mdp = 0;
    };

    connexionValidator = new connexionValidator();


    /* |--------------------------| */
    /* |-- FORM VALIDATOR CHECK --| */
    /* |--------------------------| */

    function connexionValidCheck(){
        var validator = true;

        for(index in connexionValidator){
            if(connexionValidator[index] == 0){
                validator = false;
            };
        };

        if(validator == true){
            $('#submit').css('background-color','#50A058');
        }else{
            $('#submit').css('background-color','#9B3937');
        };

        return validator;
    };


    /* |------------------| */
    /* |-- PSEUDO FIELD --| */
    /* |------------------| */

    function pseudoCheck(){
        var data = $('#connexion_pseudo').serialize();

        $.post(
            'ajax/form_connexion.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#connexion_pseudo').css('border','1px #50A058 solid');
                    connexionValidator.pseudo = 1;
                    $('#connexion_pseudo-error').fadeTo(400,0, function(){$('#connexion_pseudo-error').remove();});
                }else{
                    connexionValidator.pseudo = 0;
                    $('#connexion_pseudo').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#connexion_pseudo').on('input change keyup', function(){
        pseudoCheck();
        window.setTimeout(function(){
            connexionValidCheck();
        },200);
    });



    /* |---------------| */
    /* |-- MDP FIELD --| */
    /* |---------------| */

    function mdpCheck(){

        if($('#connexion_mdp').val().length >= 3){
            $('#connexion_mdp').css('border','1px #50A058 solid');
            connexionValidator.mdp = 1;
            $('#connexion_mdp-error').fadeTo(400,0, function(){$('#connexion_mdp-error').remove();});
        }else{
            connexionValidator.mdp = 0;
            $('#connexion_mdp').css('border','1px #aaaeaf solid');
        };

    };

    $('#connexion_mdp').on('input keyup change',function(){
        mdpCheck();
        window.setTimeout(function(){
            connexionValidCheck();
        },200);
    });


    /* |----------------| */
    /* |-- AUTO CHECK --| */
    /* |----------------| */

    window.setTimeout(function(){
        pseudoCheck();
        mdpCheck();

        window.setTimeout(function(){
            connexionValidCheck();
        },200);
    }, 800);



    /* |-------------------| */
    /* |-- SUBMIT BUTTON --| */
    /* |-------------------| */

    var errorRemove;

    $('#submit').click(function(){

        if(connexionValidCheck()== true){
            $('#form_connexion').submit();
        }else{
            for(index in connexionValidator){

                var selector = $('#connexion_'+index);
                var msg = '';

                switch(index) {
                    case 'pseudo':
                        msg = 'Veuillez saisir un pseudo correct.'; break;
                    case 'mdp':
                        msg = 'Votre mot de passe doit contenir au moins 3 caractères.'; break;
                }

                if(connexionValidator[index] == 0){
                    selector.css("border","1px red solid");
                    $('#connexion_'+index+'-error').remove();
                    window.clearTimeout(errorRemove);
                    selector.after('<div class="error" id="connexion_'+index +'-error"></div>');
                    $('.error').fadeTo(400,1);
                    $('#connexion_' +index +'-error').html('<div class="error-arrow"></div>' +msg);

                }else{
                    selector.css('border','1px #50A058 solid');
                }
            };

            errorRemove = window.setTimeout(function(){
                $('.error').each(function(){
                    $(this).fadeTo(600, 0, function(){
                        $(this).remove();
                    });
                });
            },5000);

        };

    });


    /* |--------------------| */
    /* |-- ENTER KEYBOARD --| */
    /* |--------------------| */

    $('body').keyup(function(e) {
        if(e.keyCode == 13) {
            $('#submit').trigger('click');
        }
    });



    /* |------------------| */
    /* |-- ERROR REPORT --| */
    /* |------------------| */

    if(pseudoValid != 'ok'){
        $('#connexion_pseudo').after('<div class="error" id="pseudo-error"></div>');
        $('.error').fadeTo(400,1);
        $('#pseudo-error').html('<div class="error-arrow"></div>Pseudo inexistant');

        window.setTimeout(function(){
            $('.error').fadeTo(400,0, function(){
                $('#pseudo-error').remove();
            });
        },4000);

    }

    if(mdpValid != 'ok'){
        $('#connexion_mdp').after('<div class="error" id="mdp-error"></div>');
        $('.error').fadeTo(400,1);
        $('#mdp-error').html('<div class="error-arrow"></div>Mot de passe incorrect');

        window.setTimeout(function(){
            $('.error').fadeTo(400,0, function(){
                $('#pseudo-error').remove();
            });
        },4000);
    }

});