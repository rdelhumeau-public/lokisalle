$(function(){


/* |---------------------------------| */
/* |-- PRODUIT DEJA DANS LE PANIER --| */
/* |---------------------------------| */

    if(alreadyPanier){
        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-global"><p>Cet article est déjà dans votre panier !<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-global').fadeTo(500,1);

        $('#close, .continuer').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('#popin-global').fadeTo(400,0,function() {
                $('#popin-global').remove();
            });

        });

    };


/* |------------------| */
/* |-- BOUTON PAYER --| */
/* |------------------| */

    var cgvValidator = function(){
        this.cgv = false;
    };

    $('#cgv').on('change', function(){
        if(!cgvValidator.cgv){
            cgvValidator.cgv = true;
        }else{
            cgvValidator.cgv = false;
        };

        window.setTimeout(function(){
            if(cgvValidator.cgv == 1 && panier){
                $('#submit').css('background-color','#50A058');
            }else{
                $('#submit').css('background-color','#C88F68');
            }
        },100);
    });



    $('#submit').click(function(){
        if(!panier){
            $('body').prepend('<div class="dark-bg"></div>');
            $('#page').prepend('<div id="popin-global"><p>Votre panier est vide !<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
            $('.dark-bg').fadeTo(500,1);
            $('#popin-global').fadeTo(500,1);

            $('#close, .continuer').click(function(){
                $('.dark-bg').fadeTo(400,0,function() {
                    $('.dark-bg').remove();
                });
                $('#popin-global').fadeTo(400,0,function() {
                    $('#popin-global').remove();
                });

            });
        }else{
            if(!cgvValidator.cgv){
                $('body').prepend('<div class="dark-bg"></div>');
                $('#page').prepend('<div id="popin-global"><p>Veuillez accepter les conditions générales de vente !<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
                $('.dark-bg').fadeTo(500,1);
                $('#popin-global').fadeTo(500,1);

                $('#close, .continuer').click(function(){
                    $('.dark-bg').fadeTo(400,0,function() {
                        $('.dark-bg').remove();
                    });
                    $('#popin-global').fadeTo(400,0,function() {
                        $('#popin-global').remove();
                    });

                });
            }else{
                if(cgvValidator.cgv){
                    window.location = path + 'panier/payer';
                }
            };
        };
    });



/* |---------------------------| */
/* |-- CONFIRMATION PAIEMENT --| */
/* |---------------------------| */

    if(paye){
        $('#popin-global').css('height','450px');
        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-global"><p id="first">Transaction en cours...<br><br><img src="' +path +'img/loader.gif" alt=""></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-global').fadeTo(500,1);

        window.setTimeout(function(){
            $('#first').fadeTo(500,0);
            window.setTimeout(function(){
                window.location = path + 'commande/facture/' +factureId;
            },500);
        },2500);

    };



    /* |---------------------------| */
    /* |-- ECHECS DE RESERVATION --| */
    /* |---------------------------| */

    if(echec){
        $('body').prepend('<div class="dark-bg"></div>');
        $('#page').prepend('<div id="popin-global"><p>Certains de vos articles ne sont plus disponibles.<br><div class="continuer">Continuer</div></p><img src="' +path +'img/icons/close.png" alt="" title="Fermer" id="close"></div>');
        $('.dark-bg').fadeTo(500,1);
        $('#popin-global').fadeTo(500,1);

        $('#close, .continuer').click(function(){
            $('.dark-bg').fadeTo(400,0,function() {
                $('.dark-bg').remove();
            });
            $('#popin-global').fadeTo(400,0,function() {
                $('#popin-global').remove();
            });

        });
    }


});
