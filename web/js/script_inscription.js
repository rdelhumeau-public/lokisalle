$(function(){

    //popin
    $('body').prepend('<div class="dark-bg"></div>');
    $('.dark-bg').fadeTo(500,1);
    $('#popin-inscription').fadeTo(500,1);




/* |----------------| */
/* |-- FORM CHECK --| */
/* |----------------| */


    //form validator
    var inscriptionValidator = function(){
        this.pseudo = 0;
        this.mdp = 0;
        this.mdpConfirmation = 0;
        this.nom = 0;
        this.prenom = 0;
        this.email = 0;
        this.sexe = 0;
        this.ville = 0;
        this.cp = 0;
        this.adresse = 0;
    };

    inscriptionValidator = new inscriptionValidator();


    //form validator check
    function inscriptionValidCheck(){
        var validator = true;

        for(index in inscriptionValidator){
            if(inscriptionValidator[index] == 0){
                validator = false;
            };
        };

        if(validator == true){
            $('#submit').css('background-color','#50A058');
        }else{
            $('#submit').css('background-color','#9B3937');
        };

        return validator;
    };



/* |------------------| */
/* |-- PSEUDO FIELD --| */
/* |------------------| */

    function pseudoCheck(){
        var data = $('#membre_pseudo').serialize();

        $.post(
            'ajax/form_inscription.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#membre_pseudo').css('border','1px #50A058 solid');
                    inscriptionValidator.pseudo = 1;
                    $('#membre_pseudo-error').fadeTo(400,0, function(){$('#membre_pseudo-error').remove();});
                }else{
                    inscriptionValidator.pseudo = 0;
                    $('#membre_pseudo').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#membre_pseudo').on('input keyup change',function(){
        pseudoCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });


/* |---------------| */
/* |-- MDP FIELD --| */
/* |---------------| */

    function mdpCheck(){

        if($('#membre_mdp').val().length >= 3){
            $('#membre_mdp').css('border','1px #50A058 solid');
            inscriptionValidator.mdp = 1;
            $('#membre_mdp-error').fadeTo(400,0, function(){$('#membre_mdp-error').remove();});
        }else{
            inscriptionValidator.mdp = 0;
            $('#membre_mdp').css('border','1px #aaaeaf solid');
        };


    };

    $('#membre_mdp').on('input keyup change',function(){
        mdpCheck();
        mdpConfirmationCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });


/* |--------------------| */
/* |-- MDP CONF FIELD --| */
/* |--------------------| */

    //mdp Confirmation field check
    function mdpConfirmationCheck(){
        if($('#membre_mdp').val() == $('#membre_mdpConfirmation').val()){
            $('#membre_mdpConfirmation').css('border','1px #50A058 solid');
            inscriptionValidator.mdpConfirmation = 1;
            $('#membre_mdpConfirmation-error').fadeTo(400,0, function(){$('#membre_mdpConfirmation-error').remove();});
        }else{
            inscriptionValidator.mdpConfirmation = 0;
            $('#membre_mdpConfirmation').css('border','1px #aaaeaf solid');
        };
    };

    $('#membre_mdpConfirmation').on('input keyup change',function(){
        mdpConfirmationCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });


/* |---------------| */
/* |-- NOM FIELD --| */
/* |---------------| */

    function nomCheck(){
        var data = $('#membre_nom').serialize();

        $.post(
            'ajax/form_inscription.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#membre_nom').css('border','1px #50A058 solid');
                    inscriptionValidator.nom = 1;
                    $('#membre_nom-error').fadeTo(400,0, function(){$('#membre_nom-error').remove();});
                }else{
                    inscriptionValidator.nom = 0;
                    $('#membre_nom').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#membre_nom').on('input keyup change',function(){
        nomCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });


/* |------------------| */
/* |-- PRENOM FIELD --| */
/* |------------------| */

    function prenomCheck(){
        var data = $('#membre_prenom').serialize();

        $.post(
            'ajax/form_inscription.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#membre_prenom').css('border','1px #50A058 solid');
                    inscriptionValidator.prenom = 1;
                    $('#membre_prenom-error').fadeTo(400,0, function(){$('#membre_prenom-error').remove();});
                }else{
                    inscriptionValidator.prenom = 0;
                    $('#membre_prenom').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#membre_prenom').on('input keyup change',function(){
        prenomCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });


/* |-----------------| */
/* |-- EMAIL FIELD --| */
/* |-----------------| */

    function emailCheck(){
        var data = $('#membre_email').serialize();

        $.post(
            'ajax/form_inscription.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#membre_email').css('border','1px #50A058 solid');
                    inscriptionValidator.email = 1;
                    $('#membre_email-error').fadeTo(400,0, function(){$('#membre_email-error').remove();});
                }else{
                    inscriptionValidator.email = 0;
                    $('#membre_email').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#membre_email').on('input keyup change',function(){
        emailCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });



/* |----------------| */
/* |-- SEXE FIELD --| */
/* |----------------| */

    function sexeCheck(){

        if($('#homme').val() == '' && $('#femme').val() == ''){
            inscriptionValidator.sexe = 0;
        }else{
            inscriptionValidator.sexe = 1;
            $('#membre_sexe-error').fadeTo(400,0, function(){$('#membre_sexe-error').remove();});
        };

    };

    $('#homme').change(function(){
        sexeCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });

    $('#femme').change(function(){
        sexeCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });



/* |-----------------| */
/* |-- VILLE FIELD --| */
/* |-----------------| */

    function villeCheck(){
        var data = $('#membre_ville').serialize();

        $.post(
            'ajax/form_inscription.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#membre_ville').css('border','1px #50A058 solid');
                    inscriptionValidator.ville = 1;
                    $('#membre_ville-error').fadeTo(400,0, function(){$('#membre_ville-error').remove();});
                }else{
                    inscriptionValidator.ville = 0;
                    $('#membre_ville').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#membre_ville').on('input keyup change',function(){
        villeCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });


/* |-----------------------| */
/* |-- CODE POSTAL FIELD --| */
/* |-----------------------| */

    function cpCheck(){
        var data = $('#membre_cp').serialize();

        $.post(
            'ajax/form_inscription.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#membre_cp').css('border','1px #50A058 solid');
                    inscriptionValidator.cp = 1;
                    $('#membre_cp-error').fadeTo(400,0, function(){$('#membre_cp-error').remove();});
                }else{
                    inscriptionValidator.cp = 0;
                    $('#membre_cp').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#membre_cp').on('input keyup change',function(){
        cpCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });



/* |-------------------| */
/* |-- ADRESSE FIELD --| */
/* |-------------------| */

    function adresseCheck(){
        var data = $('#membre_adresse').serialize();

        $.post(
            'ajax/form_inscription.php',
            data,
            function(value){

                if(value == 'success'){
                    $('#membre_adresse').css('border','1px #50A058 solid');
                    inscriptionValidator.adresse = 1;
                    $('#membre_adresse-error').fadeTo(400,0, function(){$('#membre_adresse-error').remove();});
                }else{
                    inscriptionValidator.adresse = 0;
                    $('#membre_adresse').css('border','1px #aaaeaf solid');
                };
            }
            ,
            'text'
        );

    };

    $('#membre_adresse').on('input keyup change',function(){
        adresseCheck();

        window.setTimeout(function(){
            inscriptionValidCheck();
        },200);
    });




    //first check
    window.setTimeout(function(){
        inscriptionValidCheck();
    }, 200);


/* |-------------------| */
/* |-- SUBMIT BUTTON --| */
/* |-------------------| */

    var errorRemove;

    $('#submit').click(function(){

        if(inscriptionValidCheck()== true){
            $('#form_inscription').submit();
        }else{
            for(index in inscriptionValidator){

                var selector = $('#membre_'+index);
                var msg = '';

                switch(index) {
                    case 'pseudo':
                        msg = 'Votre pseudo n\'est pas disponible ou est incorrect.'; break;
                    case 'mdp':
                        msg = 'Votre mot de passe est trop court.'; break;
                    case 'mdpConfirmation':
                        msg = 'Vos mots de passe ne correspondent pas.'; break;
                    case 'nom':
                        msg = 'Votre nom ne doit contenir que des lettres.'; break;
                    case 'prenom':
                        msg = 'Votre prénom ne doit contenir que des lettres.'; break;
                    case 'email':
                    msg = 'Votre email est incorrect ou a déjà été enregistré sur notre site.'; break;
                    case 'sexe':
                        msg = 'Veuillez cocher une de ces cases.'; break;
                    case 'ville':
                        msg = 'Votre ville ne doit contenir que des lettres.'; break;
                    case 'cp':
                        msg = 'Votre code postal est incorrect.'; break;
                    case 'adresse':
                        msg = 'Votre adresse est trop courte ou comporte des caractères interdits.'; break;
                };

                if(inscriptionValidator[index] == 0){
                    if(index != 'sexe') {
                        selector.css("border", "1px red solid");
                    };
                    $('#membre_'+index+'-error').remove();
                    window.clearTimeout(errorRemove);
                    selector.after('<div class="error" id="membre_'+index +'-error"></div>');
                    $('.error').fadeTo(400,1);
                    $('#membre_' +index +'-error').html('<div class="error-arrow"></div>' +msg);

                }else{
                    if(index != 'sexe') {
                        selector.css('border', '1px #50A058 solid');
                    };
                };
            };

            errorRemove = window.setTimeout(function(){
                $('.error').each(function(){
                    $(this).fadeTo(600, 0, function(){
                        $(this).remove();
                    });
                });
            },7000);

        };

    });


    //enter keyboard
    $('body').keyup(function(e) {
        if(e.keyCode == 13) {
            $('#submit').trigger('click');
        }
    });


    //-- auto completion remove
    window.setTimeout(function() {
        $('#membre_mdp').val('');
        $('#membre_pseudo').val('');

        window.setTimeout(function(){
            mdpCheck();
        },200);
    }, 100);



});