<?php

    // -------- AJAX CONTROL ---------- //



#===================
# PSEUDO
#===================


    if(isset($_POST['membre_pseudo'])){
        $pseudo = trim($_POST['membre_pseudo']);

        $regexp = '#^[a-z 0-9 ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-]{3,30}$#i';

        $pseudo_valid = true;

        if (!preg_match($regexp, $pseudo)) {
            $pseudo_valid = false;
        }


        //-- pseudo already registered check
        require_once 'ajaxManager.php';
        $pseudoCheck = entityGenerate('Membre');

        if($pseudoCheck->pseudoExists($pseudo)){
            $pseudo_valid = false;
        }


        if ($pseudo_valid) {
            echo "success";
        }
        else{
            echo "error";
        }
    }



#===================
# NOM
#===================


    if(isset($_POST['membre_nom'])){
        $nom = trim($_POST['membre_nom']);

        $regexp = '#^[a-z ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-]{2,30}$#i';
        if (preg_match($regexp, $nom)) {
            echo "success";
        }
        else{
            echo "error";
        }
    }



#===================
# PRENOM
#===================


    if(isset($_POST['membre_prenom'])){
        $prenom = trim($_POST['membre_prenom']);

        $regexp = '#^[a-z ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-]{2,30}$#i';
        if (preg_match($regexp, $prenom)) {
            echo "success";
        }
        else{
            echo "error";
        }
    }




#===================
# EMAIL
#===================


    if(isset($_POST['membre_email'])) {

        $email = $_POST['membre_email'];
        $email = trim(strtolower($email));

        $email_valid = true;

        $email_regexp = "/^[A-Za-z0-9]{1}+[\+\-\_\.A-Za-z0-9]{2,80}+@((([A-Za-z0-9]|[A-Za-z0-9][-A-Za-z0-9]*[A-Za-z0-9])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/";
        if (!preg_match($email_regexp, $email)) {
            $email_valid = false;
        }

        if (substr($email, 0, 4) == "www.") {
            $email_valid = false;
        }
        if (preg_match("/\.\@/", $email)
            || preg_match("/\.\./", $email)
            || preg_match("/\.-/", $email)
            || preg_match("/-\./", $email)
            || preg_match("/-\@/", $email)
            || preg_match("/\@\@/", $email)
        ) {
            $email_valid = false;
        }

        //email already registered check
        require_once 'ajaxManager.php';
        $emailCheck = entityGenerate('Membre');

        if($emailCheck->emailExists($email)){
            $email_valid = false;
        }

        if ($email_valid) {
            echo "success";
        } else {
            echo "error";
        }

    }



#===================
# VILLE
#===================


    if(isset($_POST['membre_ville'])){
        $ville = trim($_POST['membre_ville']);

        $regexp = '#^[a-z ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-\'.]{2,30}$#i';
        if (preg_match($regexp, $ville)) {
            echo "success";
        }
        else{
            echo "error";
        }
    }



#===================
# CODE POSTAL
#===================


    if(isset($_POST['membre_cp'])){
        $cp = trim($_POST['membre_cp']);

        $regexp = '/^[0-9]{5}$/';
        if (preg_match($regexp, $cp)) {
            echo "success";
        }
        else{
            echo "error";
        }
    }



#===================
# ADRESSE
#===================


    if(isset($_POST['membre_adresse'])){
        $adresse = trim($_POST['membre_adresse']);

        $regexp = '#^[a-z 0-9 ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-\^\)\:\,\;!?.\_\=\'\(\]\+\"\&\\r\\n]{7,60}$#i';

        if (preg_match($regexp, $adresse)) {
            echo "success";
        }
        else{
            echo "error";
        }
    }

?>