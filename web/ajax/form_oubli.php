<?php

    //-- Oubli Ajax Control --

#===================
# EMAIL
#===================


    if(isset($_POST['oubli_email'])) {

        $email = $_POST['oubli_email'];
        $email = trim(strtolower($email));

        $email_valid = true;

        //-- email already registered check
        require_once 'ajaxManager.php';
        $emailCheck = entityGenerate('Membre');

        if(!$emailCheck->emailExists($email)){
            $email_valid = false;
        }

        if ($email_valid) {
            echo "success";
        } else {
            echo "error";
        }

    }