<?php

    // -------- AJAX CONTROL ---------- //


#===================
# AVIS
#===================


    if(isset($_POST['avis_texte'])){
        $avis = trim($_POST['avis_texte']);

        $regexp = '#^[a-z 0-9 ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-\^\)\:\,\;!?.\_\=\'\(\]\+\"\&\\r\\n]{6,600}$#i';

        $avis_valid = true;

        if (!preg_match($regexp, $avis)) {
            $avis_valid = false;
        }

        if ($avis_valid) {
            echo "success";
        }
        else{
            echo "error";
        }
    }
