<?php

    // -------- AJAX CONTROL ---------- //

#===================
# EMAIL
#===================

    if(isset($_POST['email'][0])) {

        $email = $_POST['email'][0];
        $email = trim(strtolower($email));

        $email_valid = true;

        $email_regexp = "/^[A-Za-z0-9]{1}+[\+\-\_\.A-Za-z0-9]{2,80}+@((([A-Za-z0-9]|[A-Za-z0-9][-A-Za-z0-9]*[A-Za-z0-9])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/";
        if (!preg_match($email_regexp, $email)) {
            $email_valid = false;
        }

        if (substr($email, 0, 4) == "www.") {
            $email_valid = false;
        }
        if (preg_match("/\.\@/", $email)
            || preg_match("/\.\./", $email)
            || preg_match("/\.-/", $email)
            || preg_match("/-\./", $email)
            || preg_match("/-\@/", $email)
            || preg_match("/\@\@/", $email)
        ) {
            $email_valid = false;
        }

        //email already registered check
        require_once 'ajaxManager.php';
        $emailCheck = entityGenerate('Membre');

        $emailSearch = $emailCheck->emailExists($email);


        //check if email is user's email and if email is already registered

        $membre_id = $_POST['email'][1];
        if($emailSearch && $emailSearch != $membre_id){
            $email_valid = false;
        }

        if ($email_valid) {
            echo "success";
        } else {
            echo "error";
        }

    }



#===================
# VILLE
#===================


    if(isset($_POST['edit_ville'])){
        $ville = trim($_POST['edit_ville']);

        $regexp = '#^[a-z ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-\'.]{2,30}$#i';
        if (preg_match($regexp, $ville)) {
            echo "success";
        }
        else{
            echo "error";
        }
    }



#===================
# CODE POSTAL
#===================


    if(isset($_POST['edit_cp'])){
        $cp = trim($_POST['edit_cp']);

        $regexp = '/^[0-9]{5}$/';
        if (preg_match($regexp, $cp)) {
            echo "success";
        }
        else{
            echo "error";
        }
    }



#===================
# ADRESSE
#===================


    if(isset($_POST['edit_adresse'])){
        $adresse = trim($_POST['edit_adresse']);

        if (strlen($adresse) >= 7) {
            echo "success";
        }
        else{
            echo "error";
        }
    }




#===================
# ANCIEN MDP
#===================


    if(isset($_POST['membre'][0])){
        $mdp = trim($_POST['membre'][0]);

        //check if mdp is correct
        require_once 'ajaxManager.php';
        $membre = entityGenerate('Membre');
        $uid = $_POST['membre'][1];

        $membre_pseudo = $membre->getField('pseudo', $uid);

        $connexion = $membre->connexion($membre_pseudo, $mdp);


        if ($connexion && $connexion !='wrong_mdp') {
            echo "success";
        }
        else{
            echo "error";
        }
    }

?>