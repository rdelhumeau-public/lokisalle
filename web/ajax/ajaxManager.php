<?php

#===================
# AJAX MANAGER
#===================

    //-- Connection to database and entities from ajax

    define('DS', DIRECTORY_SEPARATOR);
    define('ROOT', str_replace('web' . DS .'ajax', '', __DIR__));
    require_once ROOT .'models' .DS .'Database.php';


    function entityGenerate($entity){
        require_once ROOT .'models' .DS .ucfirst($entity) .'Entity.php';

        $entity = 'Model\\' .ucfirst($entity) .'Entity';

        return new $entity();
    }
