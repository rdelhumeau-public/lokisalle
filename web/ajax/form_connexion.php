<?php

    // -------- CONNEXION AJAX ---------- //



#===================
# PSEUDO
#===================

    if(isset($_POST['connexion_pseudo'])){
        $pseudo = trim($_POST['connexion_pseudo']);

        $regexp = '#^[a-z 0-9 ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-]{2,60}$#i';
        if (preg_match($regexp, $pseudo)) {
            echo "success";
        }
        else{
            echo "error";
        }
    }
