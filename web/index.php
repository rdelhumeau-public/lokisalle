<?php
    define('ROOT', str_replace('web', '', __DIR__));
    define('DS', DIRECTORY_SEPARATOR);
    define('PATH', '/');

    if(!empty($_ENV)){
        define('ENV', 'prod');
        define('HOME', 'https://lokisalle.raphaeldelhumeau.com/');
    }
    else{
        define('ENV', 'dev');
        define('HOME', '/');
    }

    require_once(ROOT .'controllers'. DS .'autoload.php');
    require_once(ROOT .'config' .DS .'routes.php');
    require_once(ROOT .'controllers' .DS .'routeur.php');

?>