<?php

    $routes = array(
        'admin' => array('gestionsalles', 'gestionavis', 'gestionproduits', 'gestioncommandes', 'gestionmembres', 'envoinewsletter'),
        'avis' => array('ajouter'),
        'commande' => array('facture'),
        'connexion' => '',
        'contact' => '',
        'inscription' => '',
        'membre' => array('connexion','deconnexion','inscription', 'editer'),
        'newsletter' => array('inscription'),
        'oublimdp' => array('envoyer','reinitialiser'),
        'panier' => array('ajouter', 'vider', 'retirer', 'payer'),
        'profil' => '',
        'reservation' => array('salles'),
        'recherche' => array('filtres')
    );

