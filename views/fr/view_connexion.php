<div id="page">

    <div id="popin-connexion">
        <a href="<?php echo $param['back_url']; ?>" title="Fermer"><img src="<?php echo PATH;?>img/icons/close.png" alt="" id="close"></a>
        <div id="lokisalle"><img src="<?php echo PATH;?>img/lokisalle.png" alt=""></div>
        <h1>Connexion</h1>
        <div class="hr"></div>

        <form action="<?php echo HOME;?>membre/connexion" method="post" id="form_connexion">

            <div class="field-container">
                <img src="<?php echo PATH;?>img/icons/middle_pseudo_white.png" alt="" class="icon">
                <input type="text" name="connexion_pseudo" placeholder="Pseudo" id="connexion_pseudo" <?php if(isset($param['pseudo'])) echo 'value ="' .$param['pseudo'] .'"';?>>
            </div>
            <div class="field-container">
                <img src="<?php echo PATH;?>img/icons/middle_lock_white.png" alt="" class="icon">
                <input type="password" name="connexion_mdp" placeholder="Mot de passe" id="connexion_mdp">
            </div>
            <input type="checkbox" name="cookie" value="oui" id="cookie"><label for="cookie">Se souvenir de moi</label>

        </form>

        <div id="submit">Valider</div>

        <p class="connexion-footer">
            <a href="<?php echo HOME;?>inscription">Inscription</a> | <a href="<?php echo HOME;?>oubli-mdp">Mot de passe oublié</a>
        </p>
    </div>
</div>

<?php echo '<script>var pseudoValid = \'' .$param['pseudo-error'] .'\'; var mdpValid = \'' .$param['mdp-error'] .'\'</script>';?>