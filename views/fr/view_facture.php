<div id="page">

    <h1><img src="<?php echo PATH;?>img/icons/title_facture.png" alt="" class="icon2">Votre commande :</h1>
    <div class="hr"></div>

    <div id="facture">

        <p id="topleft">
            <img src="<?php echo PATH;?>img/lokisalle.png" alt="Lokisalle" style="width:170px;"><br>
            1 rue Boswellia<br>
            75001 Paris<br>
            http://lokisalle.raphaeldelhumeau.com<br>
            01 47 20 00 01
        </p>

        <div class="container">
            <div id="membre">
            <?php echo $_SESSION['membre_nom'];?><br>
            <?php echo $_SESSION['membre_prenom'];?><br>
            <?php echo $_SESSION['membre_adresse'];?><br>
            <?php echo $_SESSION['membre_cp'] .' ' .$_SESSION['membre_ville'];?>
                <br><br>
                <span style="font-size: 15px;">Le <?php echo $param['facture'][0]->commande_date .' - ' .$param['facture'][0]->commande_date_heure;?></span>
            </div>
        </div>

        <p><strong>Réf : </strong><?php echo 'C0'. $param['facture'][0]->ref;?></p>

        <div class="hr"></div>

        <table>
        <?php
            $total = 0;
            foreach($param['facture'] as $key => $commande){
                echo '<tr>';
                echo '<td><strong>'.$commande->salle_titre .'</strong><br><a href="'.HOME .'reservation/salles/'.strtolower($commande->salle_titre) .'"><img src="'.PATH .'img/rooms/' .strtolower($commande->salle_titre).'_preview.jpg"></a></td>';
                echo '<td>'.$commande->salle_ville  .'<br>' .$commande->salle_capacite .' <span class="small">pers.</span><br>' .ucfirst($commande->salle_categorie) .'</td>';
                echo '<td>Du '.$commande->produit_arrivee_date .'<br>à <span class="small">'.$commande->produit_arrivee_heure .'</span></td>';
                echo '<td>Au '.$commande->produit_depart_date .'<br>à <span class="small">'.$commande->produit_depart_heure .'</span></td>';
                echo '<td>'.number_format($commande->produit_prix, 2, ',', ' ') .' &euro;</td></tr>';
                $total += $commande->produit_prix;
        }
        ?>
        </table>
        <div class="container2">
            <div id="totaux">
                <p style="display:inline-block;margin-right:42px;">TOTAL :</p><?php echo number_format($total,2,',',' ') .' &euro;';?>
                <p style="display:inline-block;margin-right:58px;">TVA :</p><?php echo number_format(\Controller\Tools::tvaGenerate($total)['tva'],2,',',' ') .' &euro;';?>
            </div>
        </div>
        <div class="hr2"></div>
        <div class="container2">
            <div id="total">
                <p style="display:inline-block;margin-right:28px;">À PAYER :</p><?php echo number_format($param['facture'][0]->commande_montant,2,',',' ') .' &euro;';?>
            </div>
        </div>


    </div>

</div>

<script>
<?php
if(isset($param['paye'])) echo 'var paye = true;'; else echo 'var paye = false;';
?>
</script>