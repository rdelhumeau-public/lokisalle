<div id="page">

    <h1>
        <img src="<?php echo PATH;?>img/icons/title_key.png" alt="" class="icon">
        <a href="<?php echo HOME;?>reservation">Réservation</a></h1>
    <span class="title-link"> <img src="<?php echo PATH;?>img/icons/link_right_arrow.png" alt="" class="icon3"> Salle <?php echo $param['salle']->salle_titre . ' | ' .ucfirst($param['salle']->salle_ville);?></span>
    <div class="hr"></div>




    <div id="left-block">
        <p class="room-title"><?php echo $param['salle']->salle_titre; ?></p>

        <div id="preview">
            <img src="<?php echo PATH;?>img/rooms/<?php echo strtolower(urldecode($param['salle']->salle_titre)); ?>_preview.jpg" alt="<?php echo $param['salle']->salle_titre;?>" class="photo-preview">
        </div>

        <div class="details-left">
            <p><img src="<?php echo PATH;?>img/icons/middle_place.png" alt="Adresse" class="icon"> <strong><?php echo  $param['salle']->salle_cp .' - ' .strtoupper( $param['salle']->salle_ville); ?></strong></p>
            <p class="indent"><?php echo  $param['salle']->salle_adresse; ?></p>
            <p><img src="<?php echo PATH;?>img/icons/middle_capacite.png" alt="Capacité" class="icon"> <?php echo ucfirst( $param['salle']->salle_categorie) .' - ' . $param['salle']->salle_capacite; ?> personnes</p>
            <p style="margin-top:10px;"><img src="<?php echo PATH;?>img/icons/middle_key.png" alt="Disponibilité" class="icon"> <?php if($param['produits']) echo '<span style="color:green;">Offres disponibles</span>'; else echo '<span class="red">Aucune offre disponible</span>'; ?></p>
        </div>

        <?php if(isset($param['note'])):?>
        <div class="details-right">
            <p><?php echo $param['note'];?>/5</p>

            <p><?php for($i=1;$i<=5;$i++){
                    if($i<=$param['note']) {
                        echo '<img src="' . PATH . 'img/icons/small_star_full.png" alt="">';
                    }
                    else{
                        echo '<img src="' . PATH . 'img/icons/small_star_empty.png" alt="">';
                    }
                }?>
            </p>
            <p>( <?php echo $param['nbNotes']; ?> note<?php if($param['nbNotes']>1) echo 's'; ?>)</p>
        </div>
        <?php endif;?>
    </div>



    <div id="left-side">

        <div id="description-block-top">
            <img src="<?php echo PATH;?>img/icons/title_description.png" alt="" class="description-icon">
            <div class="description">
                <p><?php echo  $param['salle']->salle_description;

                        if($param['salle']->salle_titre == 'Ifocop'){
                            echo '<br><br><strong>Les + :</strong><br><br>
                                - Les imprimantes qui fonctionnent uniquement les jours de pleine lune.<br>
                                - Denis qui enregistre sur le serveur.<br>
                                - Les "blagues" de Christophe.<br>
                                - Les écrans tactiles.<br>
                                - L\'équipement téléphonique.<br>
                                - La prononciation franglaise de Ziad.<br>
                                - L\'élection des délégués de Grégoire.<br>
                                - Le réseau 3G-<br>
                                - Le mot de passe du Wifi.<br>
                                - Toute l\'équipe Ziad/Mathieu/Christophe et Sami pour m\'avoir transmis leur passion :)';
                        }
                    ?>
                </p>
            </div>
        </div>

    </div>



    <div id="right-side">

        <div id="offers-block">
            <img src="<?php echo PATH;?>img/icons/title_offer.png" alt="" class="description-icon">
            <div class="description">
                <p style="margin-bottom: 10px;">Nos offres :</p>

                <?php if($param['produits']): ?>
                    <form method="post" action="<?php echo HOME;?>panier/ajouter" id="add-form">
                        <div class="hr"></div>
                        <?php foreach($param['produits'] as $key => $value){?>

                            <input type="radio" name="produit_id" value="<?php echo $value->produit_id;?>" id="produit<?php echo $value->produit_id;?>">
                            <label for="produit<?php echo $value->produit_id;?>">Du <?php echo $value->produit_arrivee_date;?> <span style="font-size:12px">à <?php echo $value->produit_arrivee_heure;?></span>
                                <br>au <?php echo $value->produit_depart_date;?>  <span style="font-size:12px">à <?php echo $value->produit_depart_heure;?></span></label>
                            <div class="price"><?php echo number_format($value->produit_prix,2,',',' ');?> &euro; <span class="small">HT</span></div>
                            <div class="hr"></div>
                        <?php }
                        ?>
                    </form>
                <?php else: ?>
                    <p class="red">Cette salle n'est plus disponible pour le moment.</p>
                <?php endif;?>
            </div>

            <?php if($param['produits']): ?>
            <div id="submit"><img src="<?php echo PATH;?>img/icons/middle_panier_white.png" alt="" class="icon2">Ajouter au panier</div>
            <?php endif;?>

        </div>


        <div id="avis">
            <img src="<?php echo PATH;?>img/icons/title_avis.png" alt="" id="avis-icon">
            <p class="title"><strong>Avis</strong></p>
            <div class="hr"></div>
            <div id="avis-cadre">

                <?php if(isset($param['avis'])): ?>

                    <?php foreach($param['avis'] as $key => $value){?>
                        <div class="avis-line">
                            <div class="avis-date">le <?php echo $value->avis_date;?> à <?php echo $value->avis_heure;?></div>
                            <p><strong><?php echo $value->membre_pseudo;?></strong></p>
                            <div class="hr"></div>
                            <p><?php echo nl2br(htmlspecialchars(stripslashes($value->avis_texte)));?></p>
                            <p><?php for($i=1;$i<=$value->avis_note;$i++){
                                    echo '<img src="'.PATH .'img/icons/small_star_full.png" alt="">';
                                }?></p>
                        </div>

                    <?php }
                endif; ?>

            </div>
            <div class="hr"></div>

            <?php if(isset($_SESSION['membre_id'])):?>

                <?php if(isset($param['alreadyPosted'])):?>
                    <div id="commenter">
                        <p><img src="<?php echo PATH;?>img/icons/title_plume.png" alt="" class="icon">
                            Merci pour votre contribution.</p>
                    </div>
                <?php else:?>
            <div id="commenter">
                <p><img src="<?php echo PATH;?>img/icons/title_plume.png" alt="" class="icon">Donnez-nous votre avis :</p>

                <form method="post" id="avis_form" action="<?php echo HOME;?>avis/ajouter/<?php echo $param['salle']->salle_titre .'/' .$param['salle']->salle_id;?>">
                    <div class="field-container"><textarea name="avis_texte" id="avis_texte"></textarea></div>

                    <div id="note">
                        <p>Note :</p>
                        <select name="avis_note" id="avis_note">
                            <option>-</option>
                            <?php for($i=0;$i<=5;$i++) echo '<option value="'.$i .'">' .$i .'</option>';?>
                        </select>
                        <p><strong><span class="brown" id="current-note">-</span></strong>/5</p>
                    </div>

                    <div id="submit-container">
                        <div id="avis_submit" class="submit">Poster votre avis</div>
                    </div>
                </form>
            </div>
                <?php endif;?>

            <?php else:?>
                <div id="commenter">
                    <p><img src="<?php echo PATH;?>img/icons/title_plume.png" alt="" class="icon">
                        <a href="<?php echo HOME;?>connexion" style="text-decoration: underline;">Connectez-vous</a> pour partager votre avis.</p>
                </div>
            <?php endif;?>
        </div>
    </div>

    <?php if(isset($param['suggestions'])):?>

    <div id="suggestions">
        <h2><img src="<?php echo PATH;?>img/icons/middle_key.png" alt="" class="icon2">Offres similaires
            <?php if($param['filtre']=='ville') echo 'à '.$param['salle']->salle_ville; else echo 'dans la catégorie ' .ucfirst($param['salle']->salle_categorie);?> :</h2>
        <div class="hr"></div>

        <?php foreach($param['suggestions'] as $key => $value){?>
            <div class="suggestion-offre">
                <p><img src="<?php echo PATH;?>img/rooms/<?php echo strtolower($value->salle_titre);?>_preview.jpg" alt="" class="preview-suggestion"></p>
                <p style="font-size:18px;"><strong><?php echo $value->salle_titre;?></strong></p>
                <p><?php
                        if($value->produit_arrivee_date != $value->produit_depart_date){
                            echo 'Du ' .$value->produit_arrivee_date .'<span style="font-size:12px"> à ' .$value->produit_arrivee_heure .'</span>
                                  <br>au ' .$value->produit_depart_date .'<span style="font-size:12px"> à ' .$value->produit_depart_heure .'</span>';
                        }
                        else{
                            echo '<br>Le ' .$value->produit_arrivee_date .'<span style="font-size:14px"> de '.$value->produit_arrivee_heure  .' à ' .$value->produit_depart_heure .'</span>';
                        }
                    ?></p>
                <p><?php echo number_format($value->produit_prix,2,',',' ');?> &euro; HT</p>
                <a href="<?php echo HOME .'reservation/salles/' .$value->salle_titre;?>"><div class="suggestion-voir">Voir</div></a>
            </div>
        <?php
        }?>
    </div>
    <?php endif;?>

    <div id="zoom-preview">
        <div class="dark-bg">
            <img src="<?php echo $param['salle']->salle_photo ?>">
        </div>
    </div>

</div>

<script>
    <?php
    if(isset($_SESSION['previous_url']) && ($_SESSION['previous_url'] == PATH .'reservation' || preg_match('#recherche#',$_SESSION['previous_url']))) echo 'var introAnimation = true';
    else echo 'var introAnimation = false';
    ?>
</script>