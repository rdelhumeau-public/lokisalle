<div id="page">
    <h1><img src="<?php echo PATH;?>img/icons/title_conditions.png" alt="" class="icon2">Conditions Générales de Vente :</h1>
    <div class="hr"></div>

    <p><br><img src="<?php echo PATH;?>img/icons/warning.png" alt="" class="icon"><span class="red"><strong>Attention :</strong></span> La société Lokisalle ainsi que toutes les informations présentes sur ce site sont purement fictives. Ce site a été réalisé dans le cadre d'un projet d'étude de développeur web.</p>
    <p><br><strong>ARTICLE 1 – DEFINITION</strong></p>

    <p><br>
    1.1 On entend par « LOKISALLE », la société Lokisalle, exerçant l’activité commerciale de location de salles dans le cadre de réceptions privées ou d’événements de tout type et pour tout public.<br><br>
    1.2 On entend par « UTILISATEUR », l’Internaute à la recherche d'un espace et autres prestations événementielles pour l'organisation d'un événement de tout type ou d'une manifestation professionnelle.<br><br>
    1.3 On entend par « INTERNAUTE », le visiteur du site internet www.lokisalle.com.<br>
    </p>

    <p><br><strong>ARTICLE 2 – ACCES A L’ENREGISTREMENT</strong></p>

    <p><br>Préalablement à son inscription sur le site internet www.lokisalle.com, l’Utilisateur devra s’assurer qu’il remplit les conditions nécessaires définies ci-après :<br><br>
    Pour consulter et commander les prestations Lokisalle, l’Utilisateur doit auparavant s’inscrire sur le site internet www.lokisalle.com ce qui lui permet d'obtenir automatiquement un compte Utilisateur.<br>
    L’Utilisateur doit veiller à ce que son équipement informatique ait une configuration compatible à celle requise pour l’utilisation du site www.lokisalle.com. Il appartient à l’Utilisateur de s’assurer de cette compatibilité.<br>
    La demande d’inscription est validée lorsque l’Utilisateur a correctement rempli le formulaire d'inscription. Le cas échéant, l’Utilisateur reçoit immédiatement un courrier électronique à l’adresse renseignée par lui, confirmant son inscription.<br>
    Le mot de passe reconnu par Lokisalle est strictement personnel et confidentiel. L’Utilisateur en est seul responsable et s'engage à ne pas le divulguer. L'accès ultérieur aux services Lokisalle nécessite l'utilisation de l'identifiant (ou pseudo) et du mot de passe. L’Utilisateur peut modifier à tout moment son mot de passe.<br>
    Cependant l’Utilisateur s'engage à ne pas utiliser un pseudo à consonance injurieuse, déplacée, discriminatoire ou contraire au respect de la nature humaine.<br><br>
    A défaut, Lokisalle se réserve, à sa seule discrétion, le droit de suspendre l'accès à cet Utilisateur, le temps de l'informer d'une telle incompatibilité et de lui permettre de choisir un pseudo plus approprié, dans les conditions définies à l’« article 5 » des présentes.<br>
    Après validation de l’enregistrement du compte personnel de l’Utilisateur, celui-ci pourra renseigner les champs le concernant et choisir les différentes prestations proposées sur le site internet www.lokisalle.com pour l’organisation d’un événement ou d’une manifestation professionnelle.<br>
    </p>

    <p><br><strong>ARTICLE 3 – OBLIGATION DE L’UTILISATEUR ET DES INTERNAUTES</strong></p>

    <p><br>
    3.1. Fourniture par l’Utilisateur d’informations exactes et identification<br><br>
    L’Utilisateur s’engage à mettre à jour ses coordonnées au fur et à mesure des éventuels changements, de manière à ce que les informations du l’Utilisateur à la disposition de Lokisalle soient exactes pendant toute la durée des relations entre l’Utilisateur et Lokisalle.
    Lokisalle ne saurait être tenu responsable au cas où elle n’aurait pas été avisée d’un changement de situation de l’Utilisateur.<br><br>
    3.2. Obligations propres à l’utilisation des services de Lokisalle<br><br>
    La marque Lokisalle est déposée auprès de l’INPI, le site est la propriété de Lokisalle.
    L’intégralité du site internet, présentation et contenu, constitue une œuvre protégée par les lois en vigueur sur la propriété intellectuelle, dont Lokisalle est titulaire. La structure générale, images animées ou fixes, dessins, graphismes, textes, photos, savoir-faire, et tout autre élément composant le site sont la propriété exclusive de Lokisalle. L’ensemble de ces éléments est protégé par le Code de la propriété intellectuelle ainsi que toute norme internationale applicable, et constitue, sauf indications contraires, la propriété intellectuelle exclusive de Lokisalle.
    Plus généralement, les contenus publiés sur ce site sont protégés par des droits d'auteurs et sont la propriété exclusive de Lokisalle. Par « contenu », est entendu tout mode d'expression, information ou autre service rencontré sur www.lokisalle.com.
    Lokisalle ne concède aux Utilisateurs ni à aucun Internaute aucune licence ni aucun autre droit que celui de consulter le site aux fins susvisées. En conséquence, l’Utilisateur ainsi que tout Internaute s'engage, notamment :
    <br><br>- À ne pas diffuser ou divulguer tout ou partie du contenu du site par quelque moyen que ce soit ;
    <br>- À ne pas télécharger autrement que dans les cas autorisés par Lokisalle et, plus généralement, à ne pas fixer et reproduire sur tout support quel qu'il soit, par quelque procédé que ce soit, à la seule exception d'une impression sur support papier dans les limites ci-après;
    <br>- Plus généralement, à ne pas exploiter et/ou utiliser tout ou partie des éléments du site par quelque moyen que ce soit et sous quelque forme que ce soit à des fins autres que celles expressément autorisées.<br><br>
    En cas d'impression sur support papier, l’Utilisateur et tout Internaute garantissent que toutes les mentions figurant éventuellement sur le contenu ainsi imprimé relatives à la protection des droits seront reproduites sans modifications. Ces copies ne pourront être utilisées que par eux seuls à des fins personnelles pour leur seul usage privé à l'exclusion de tout autre et ne pourront en aucun cas être diffusées ou divulguées auprès des tiers.
    Toute utilisation du contenu du site internet www.lokisalle.com, ou d'un quelconque de ses éléments, contraire aux présentes engagera la responsabilité de l’Utilisateur ou de l’Internaute fautif, notamment à l'égard de Lokisalle, et sera susceptible de les exposer à des sanctions pénales.
    </p>
    <p><br><strong>ARTICLE 4 – OBLIGATION DE LOKISALLE</strong></p>

    <p><br>4.1. Description du prix et du produit<br><br>
    Lokisalle s'efforce de décrire au mieux les produits présentés dans ce site. Pour informer l'Utilisateur des prix des prestations de traiteur événementiel et d'animations événementielles, Lokisalle communique une tarification en euro faisant foi. En conséquence, seuls les prix indiqués en euros sur le site font foi.
    <br><br>4.2. Fourniture des Services<br><br>
    Lokisalle s’engage à fournir un accès au site internet www.lokisalle.com conforme aux normes en vigueur et spécifications contractuelles, 24 heures sur 24 et 7 jours sur 7.
    À titre exceptionnel, à des fins de maintenance ou de mise à jour, Lokisalle pourra suspendre l’accès à tout ou partie des Services pendant une période consécutive de 24 heures pour des raisons telles des opérations techniques indépendantes de sa volonté.
    En dehors des cas justifiés par l’urgence ou la force majeure, Lokisalle s’engage à prévenir l’Utilisateur sur son site internet de toute interruption des services Lokisalle.
    <br><br>4.3. Obligations de confidentialité – Protection des données à caractère personnel<br><br>
    Les informations nominatives déclarées par l’Utilisateur et tout élément d’identification le concernant sont destinés à Lokisalle, qui est autorisée par l’Utilisateur à les conserver en mémoire informatique et à les utiliser.
    Les informations détenues par Lokisalle ne sont pas utilisées à des fins commerciales autres que la commercialisation des services Lokisalle.
    En application de la législation en vigueur, les informations nominatives et tout élément d’identification concernant l’Utilisateur pourront être communiqués sur réquisition des autorités judiciaires, ou à une autre Société assistant Lokisalle dans la prévention ou la recherche de fraude.
    Conformément à la loi Informatique et Libertés n°78-17 du 6 janvier 1978 modifiée par la loi n°2004-801 du 6 août 2004, et sous réserve d’autres dispositions légales, l’Utilisateur dispose à tout moment d’un droit individuel d’opposition, d’accès, de modification, de rectification et de suppression des données qui le concernent en écrivant à Lokisalle, 1 rue Boswellia, 75001 Paris.
    <br><br>4.4. Formalité de déclaration<br><br>
    Conformément aux dispositions de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, le traitement automatisé des informations nominatives recueillies à partir du Site Internet de la société Lokisalle a fait l'objet d'une déclaration auprès de la Commission Nationale de l'Informatique et des Libertés (CNIL) qui a délivré récépissé sous le numéro : (en cours).
    Il est rappelé à l’internaute qu’il dispose au terme de l’article 34 de la loi du 6 janvier 1978, d’un droit d’accès de modification, de rectification et de suppression des données qui le concerne. Pour exercer ce droit ; l’internaute pourra s’adresser à la société Lokisalle : 1 rue Boswellia, 75001 Paris
    <br><br>4.5. Informatique et libertés<br><br>
    En vertu de la loi du 6 janvier 1978, l'utilisateur dispose à tout moment d'un droit de consultation, de modification et de suppression de ses informations personnelles détenues par la société lokisalle. La société Lokisalle se réserve le droit, aux bonnes fins d'utilisation de ses services, de placer des cookies sur votre ordinateur et d'y accéder. En particulier, Lokisalle utilise les cookies pour assurer la gestion des sessions que vous ouvrez sur les services personnalisés avec votre compte Lokisalle.
    </p>
    <p><br><strong>ARTICLE 5 : EXONERATION DE RESPONSABILITE</strong></p>

    <p><br>5.1.  L’accès au site internet www.lokisalle.com<br><br>
    Lokisalle s'efforce d'assurer au mieux de ses possibilités l'accès permanent aux Services ainsi que la mise à jour des informations figurant sur le site internet www.lokisalle.com dont elle se réserve le droit de corriger, à tout moment et sans préavis, le contenu.
    Toutes les démarchespour s’assurer de la fiabilité des informations publiées sur le site internet ayant été faites par Lokisalle, celle-ci ne peut encourir aucune responsabilité du fait d'erreurs, d'omissions, ou pour les résultats qui pourraient être obtenus par l'usage de ces informations.
    Lokisalle tient à signaler que :
    <br>- Les photographies et visuels publiés sur le site internet www.lokisalle.com ne sont pas contractuels et sont protégés par le copyright
    <br>- Certaines rédactions peuvent être transformées par les systèmes d'impression au point que leur signification devienne erronée.
    <br><br>L’Utilisateur s'interdit, en conséquence, de réclamer quelque indemnisation que ce soit à Lokisalle, pour quelque préjudice que ce soit qui trouverait sa cause dans un cas d’exonération de la responsabilité de Lokisalle précédemment exposé.
    <br><br>5.2. Les liens hypertextes<br><br>
    Les sites joints et utilisés dans le site internet www.lokisalle.com ne sont pas obligatoirement contrôlés par Lokisalle. Lokisalle n'est responsable ni du contenu de ces sites, ni des liens qu'ils contiennent, ni des changements ou mises à jour effectués. Les sites joints ne sont en aucun cas sous la responsabilité de Lokisalle.
    Toute création d'un lien hypertexte pointant vers le site www.lokisalle.com doit faire l'objet d'une autorisation préalable.
    L’accord de Lokisalle pour la création d'un lien dirigé sur www.lokisalle.com ne constitue en aucun cas une approbation du contenu du site sur lequel le lien est créé.
    Le site www.lokisalle.com comporte des liens hypertextes vers d'autres sites qui n'ont pas été développés par Lokisalle ou sous son contrôle. Ces liens ne sauraient en aucune manière être regardés comme une marque d'approbation, par Lokisalle, des contenus des sites élaborés sous la seule responsabilité de leur auteur.
    </p>
    <p><br><strong>ARTICLE 6 – SUSPENSION OU RESILIATION DU COMPTE DE L’UTILISATEUR A L’INITIATIVE DE LOKISALLE</strong></p>

    <p><br>Lokisalle pourra suspendre, de plein droit et à sa seule initiative, l’accès de l’Utilisateur à son compte Lokisalle en cas d’utilisation par un Utilisateur d’un identifiant à consonance injurieuse, déplacée ou discriminatoire ou portant atteinte au respect de la personne humaine. Cette suspension sera effective le temps de permettre à l’Utilisateur de choisir un identifiant plus approprié.
    Lokisalle pourra suspendre, de plein droit, l’accès de l’Utilisateur à son compte Lokisalle en cas de violation grave ou renouvelée par l’Utilisateur de ses obligations légales ou contractuelles.
    Une fois la suspension effective et huit jours après mise en demeure par voie électronique et/ou par lettre recommandée avec demande d’avis de réception restée sans effet, Lokisalle pourra procéder à la résiliation du compte de l’Utilisateur sans autres formalités.
    </p>
    <p><br><strong>ARTICLE 7 – DROIT APPLICABLE</strong></p>

    <p><br>Le présent site ainsi que ses conditions générales sont soumis au droit Français.</p>

    <p><br><strong>ARTICLE 8 – COORDONNEES</strong></p>

    <p><br>Toute notification et autre correspondance devra être adressée à l'adresse suivante :
    Société LOKISALLE, 1 rue Boswellia, 75001 Paris ou contact@lokisalle.com
    </p>

</div>