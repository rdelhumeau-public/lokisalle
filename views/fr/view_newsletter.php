<div id="page">
    <h1><img src="<?php echo PATH;?>img/icons/title_newsletter.png" alt="" class="icon2">Newsletter :</h1>
    <div class="hr"></div>

    <div id="newsletter">
        <img src="<?php echo PATH;?>img/main_meeting02.jpg" alt="" class="photo">
        <div id="back">
            <img src="<?php echo PATH;?>img/lokisalle.png" alt="Lokisalle" class="loki">
            <p>Inscrivez-vous à la newsletter et soyez les premiers informés de l'actualité de Lokisalle !</p>

            <?php
            if(isset($_SESSION['membre_id']) && !isset($param['already-registered'])) {
                echo '<a href="' . HOME . 'newsletter/inscription"><div id="submit">S\'inscrire</div></a>';
            }
            elseif(isset($_SESSION['membre_id'])){
                echo '<div id="submit">Vous êtes déjà inscrit</div>';
            }
            else{
                echo '<a href="' . HOME . 'connexion"><div id="submit">Connectez-vous</div></a>';
            }
            ?>
        </div>
    </div>

</div>