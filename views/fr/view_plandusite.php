<div id="page">
    <h1><img src="<?php echo PATH;?>img/icons/title_map.png" alt="" class="icon2">Plan du site :</h1>
    <div class="hr"></div>

    <img src="<?php echo PATH;?>img/lokisalle.png" alt="" class="lokisalle">
    <img src="<?php echo PATH;?>img/main_room01.jpg" alt="" class="photo">

    <div id="plandusite-container">
        <div class="plandusite">
            <p><a href="<?php echo HOME;?>"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> Accueil</a></p>
            <p><a href="<?php echo HOME;?>reservation"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> Réservation</a></p>
            <p><a href="<?php echo HOME;?>recherche"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> Recherche</a></p>
            <p><a href="<?php echo HOME;?>inscription"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> Inscription</a></p>
            <p><a href="<?php echo HOME;?>connexion"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> Connexion</a></p>
            <p><a href="<?php echo HOME;?>oubli-mdp"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> Mot de passe oublié</a></p>
        </div>

        <div class="plandusite">
            <p><a href="<?php echo HOME;?>mentions-legales"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> Mentions Légales</a></p>
            <p><a href="<?php echo HOME;?>conditions-generales"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> Conditions Générales</a></p>
            <p><a href="<?php echo HOME;?>plan-du-site"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> Plan du site</a></p>
            <p><a href="<?php echo HOME;?>newsletter"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> S'inscrire à la newsletter</a></p>
            <p><a href="<?php echo HOME;?>contact"><img src="<?php echo PATH;?>img/icons/right_arrow.png" alt=""> Nous contacter</a></p>
        </div>
    </div>

</div>