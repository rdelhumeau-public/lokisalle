<div id="page">

    <div id="popin-oubli">
        <a href="<?php echo $param['back_url']; ?>" title="Fermer"><img src="<?php echo PATH;?>img/icons/close.png" alt="" id="close"></a>
        <div id="lokisalle"><img src="<?php echo PATH;?>img/lokisalle.png" alt=""></div>
        <h1>Mot de passe oublié</h1>
        <div class="hr"></div>
        <p>Un email de confirmation vous sera envoyé afin de réinitialiser votre mot de passe :
        <br>(ne fermez pas votre navigateur pendant l'opération)</p>

        <form action="<?php echo HOME;?>oubli-mdp/envoyer" method="post" id="form_oubli">

            <div class="field-container">
                <input type="email" name="oubli_email" placeholder="Votre email" id="oubli_email">
            </div>

        </form>

        <div id="submit">Envoyer</div>

    </div>
</div>