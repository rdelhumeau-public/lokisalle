<div id="page">
    <h1><img src="<?php echo PATH;?>img/icons/title_legal.png" alt="" class="icon2">Mentions légales :</h1>
    <div class="hr"></div>

    <p><br><img src="<?php echo PATH;?>img/icons/warning.png" alt="" class="icon"><span class="red"><strong>Attention :</strong></span> La société Lokisalle ainsi que toutes les informations présentes sur ce site sont purement fictives. Ce site a été réalisé dans le cadre d'un projet d'étude de développeur web.</p>

    <p><img src="<?php echo PATH;?>img/main_room02.png" alt="" class="photo"></p>

    <p><br><strong>Lokisalle :</strong>
    <br>SARL au capital de 250 000 €<br>1 rue Boswellia<br>75001 PARIS<br>France<br>01 47 20 00 01<br>
    <br>LOKISALLE est une société imaginaire. Les informations la concernant sont factices.</p>

    <p><br><strong>Réalisation :</strong><br>Raphaël Delhumeau<br>
        <a class="brown" href="https://www.raphaeldelhumeau.com" target="_blank">https://www.raphaeldelhumeau.com</a>
        <br>IFOCOP, promotion DIWOO 08 - 2014/2015
        <br>PARIS XI</p>

    <p><br><strong>Directeur de la publication :</strong><br>Raphaël Delhumeau</p>

    <p><br><strong>Hébergement :</strong></p>
    <p>OVH<br>
    SAS au capital de 10 000 000 €<br>
    RCS Roubaix – Tourcoing 424 761 419 00045<br>
    Code APE 6202A - N° TVA : FR 22 424 761 419<br>
    Siège social : 2 rue Kellermann 59100 Roubaix - France.<br></p>

    <p><br><strong>Conditions générales de vente :</strong>
    <br><a href="<?php echo HOME;?>conditions-generales" style="color:#000000;">Voir les C.G.V</a></p>


</div>