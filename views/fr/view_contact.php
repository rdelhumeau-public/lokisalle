<div id="page">
    <h1><img src="<?php echo PATH;?>img/icons/title_contact.png" alt="" class="icon2">Nous contacter :</h1>
    <div class="hr"></div>

    <div class="side">
        <div id="contact-infos">
            <img src="<?php echo PATH;?>img/lokisalle.png" alt="" class="lokisalle">
            <p><img src="<?php echo PATH;?>img/icons/postal_black.png" alt="">1 rue Boswellia - 75001 Paris</p>

            <p><img src="<?php echo PATH;?>img/icons/phone_black.png" alt="">01 47 20 00 01</p>

            <p><img src="<?php echo PATH;?>img/icons/arobase_black.png" alt="">contact@lokisalle.com</p>
        </div>
    </div>

    <div class="side">
        <div id="contact">
            <p>Nos équipes sont à votre écoute, écrivez-nous :</p>
            <form action="<?php echo HOME;?>contact" method="post" id="form_contact">

                <div class="field-container">
                    <input <?php if(isset($_SESSION['membre_id'])) echo 'type="hidden"'; else echo 'type="text"';?> name="contact_email" <?php if($param['email'] != '') echo 'value="'.$param['email'].'"'?> placeholder="Votre adresse email" id="contact_email">
                </div>
                <div class="field-container">
                    <input type="text" name="contact_subject" placeholder="Le sujet de votre demande" id="contact_subject">
                </div>
                <div class="field-container">
                <textarea name="contact_message" placeholder="Votre message..." id="contact_message" required></textarea>
                </div>
            </form>
            <div id="submit">Envoyer</div>
        </div>
    </div>


    <img src="<?php echo PATH;?>img/main_meeting01.jpg" alt="" class="photo">

</div>

<?php
    if(isset($param['messageSent']) && $param['messageSent'] == 'true'){
        echo '<script>var messageSent = true; var membreEmail = \''.$param['email'].'\'</script>';
    }
    else{
        echo '<script>var messageSent = false; var membreEmail = \''.$param['email'] .'\'</script>';
    }
?>