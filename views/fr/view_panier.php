<div id="page">

    <h1><img src="<?php echo PATH;?>img/icons/title_panier.png" alt="" class="icon2">Panier :</h1>
    <div class="hr"></div>

    <div id="panier-container">
        <div id="panier-title"><img src="<?php echo PATH;?>img/icons/middle_key.png" alt="" class="icon2">Mes articles :</div>
        <div class="hr"></div>

        <a href="<?php echo HOME;?>panier/vider"><div id="vider"><img src="<?php echo PATH;?>img/icons/middle_vider.png" alt="" class="icon">Vider le panier</div></a>

        <table>
            <thead><tr>
                <th>Réf.</th><th>Salle</th><th>Photo</th><th>Ville</th><th>Capacité</th><th>Arrivée</th><th>Départ</th><th>Retirer</th><th>Prix</th>
            </tr></thead>
            <?php
                if(isset($param['produits'])){
                    foreach($param['produits'] as $key => $produit){
                        echo '<tr><td>LS00' .$produit->produit_id .'</td><td>' .$produit->salle_titre
                            .'</td><td><a href="'.HOME .'reservation/salles/' .$produit->salle_titre .'" title="Voir la salle"><img src="' .PATH .'img/rooms/' .strtolower($produit->salle_titre) .'_preview.jpg" alt="" class="panier-photo"></a></td><td>' .$produit->salle_ville
                            .'</td><td>' .$produit->salle_capacite .' <span class="small">pers.</span></td><td>' .$produit->produit_arrivee_date .'<br><span class="small">' .$produit->produit_arrivee_heure
                            .'</span></td><td>' .$produit->produit_depart_date .'<br><span class="small">' .$produit->produit_depart_heure .'</span></td><td><a href="'.HOME .'panier/retirer/' .$key .'" title="Retirer du panier"><img src="'.PATH .'img/icons/small_croix.png" alt=""></a></td><td>'
                            .number_format($produit->produit_prix, 2, ',', ' ').' &euro;</td></tr>';
                    }
                }
                else{
                    echo '<tr><td colspan="9">Votre panier est vide.</td></tr>';
                }
            ?>
        </table>


        <div id="total">
            <div id="tva">TVA <span class="small">(19,6%)</span><br>
            <?php if(isset($_SESSION['panier']['tva'])) echo number_format($_SESSION['panier']['tva'], 2, ',', ' ') .' &euro;'; else echo number_format(0,2, ',', ' ') .' &euro;';?> </div>
            <div id="total-left">TOTAL TTC :</div>
            <div id="prix-total">
                <?php if(isset($_SESSION['panier']['total'])) echo number_format($_SESSION['panier']['ttc'], 2, ',', ' ') .' &euro;'; else echo number_format(0,2, ',', ' ') .' &euro;';?>
            </div>
            <div id="conditions">
                <input type="checkbox" name="cgv" value="true" id="cgv"><label for="cgv">J'accepte les <a href="<?php echo HOME;?>conditions-generales" target="_blank">conditions générales de vente</a></label>
            </div>
            <div id="submit">Payer</div>
        </div>


        <div id="reglement">
            Règlement par chèque uniquement,<br>à l'adresse suivante :<br>
            <p><img src="<?php echo PATH;?>img/icons/postal_black.png" alt="" class="icon2">1 rue Boswellia - 75001 Paris</p>
        </div>

    </div>

</div>


<script>
    <?php
     if(isset($param['already-panier'])) echo 'var alreadyPanier = true;'; else echo 'var alreadyPanier = false;';
     if(isset($param['paye'])) echo 'var paye = true;'; else echo 'var paye = false;';
     if(isset($param['echec'])) echo 'var echec = true;'; else echo 'var echec = false;';
     if(isset($param['factureId'])) echo 'var factureId = ' .$param['factureId'] .';';
    ?>
</script>