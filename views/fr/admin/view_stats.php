
<div class="gestion-title"><img src="<?php echo PATH;?>img/icons/dash_stats.png" alt="" class="icon2">Statistiques</div>
<div class="count"><img src="<?php echo PATH;?>img/icons/middle_count.png" alt="" class="icon2">
    <?php
        if(isset($param['chiffreAffaire'])){
            echo '<strong>Chiffre d\'affaires : <span class="green">' .number_format($param['chiffreAffaire'],2, ',', ' ') .' &euro;</span></strong>';
        }
        if(isset($param['nbSalles'])){
            echo ' <span style="color:grey;">|</span> ' .$param['nbSalles'] .' salle';
            if($param['nbSalles'] > 1) echo 's';
        }
        if(isset($param['nbCommandes'])){
            echo ' <span style="color:grey;">|</span> ' .$param['nbCommandes'] .' commande';
            if($param['nbCommandes'] > 1) echo 's';
        }
        if(isset($param['nbMembres'])){
            echo ' <span style="color:grey;">|</span> ' .$param['nbMembres'] .' membre';
            if($param['nbMembres'] > 1) echo 's';
        }

    ?></div>

<div class="stats">
    <h1><img src="<?php echo PATH;?>img/icons/middle_chiffres.png" alt="" class="icon">Les chiffres</h1>
    <div class="hr"></div>
    <p><img src="<?php echo PATH;?>img/icons/small_salles.png" alt="" class="icon2"><strong><?php echo $param['nbSalles'];?> salles enregistrées</strong></p>
    <p><img src="<?php echo PATH;?>img/icons/small_produits.png" alt="" class="icon2"><strong><?php echo $param['nbProduits'];?> offres répertoriées</strong></p>
    <p style="text-indent:29px;"><span class="bordeaux">- <?php echo $param['nbProduitsReserves'];?> réservées</span></p>
    <p style="text-indent:29px;"><span class="green">- <?php echo ($param['nbProduits'] - $param['nbProduitsReserves']);?> disponibles</span></p>
    <p><img src="<?php echo PATH;?>img/icons/small_commandes.png" alt="" class="icon2"><strong><?php echo $param['nbCommandes'];?> commandes passées</strong></p>
    <p><img src="<?php echo PATH;?>img/icons/small_membres.png" alt="" class="icon2"><strong><?php echo $param['nbMembres'];?> membres enregistrés</strong></p>
    <p><img src="<?php echo PATH;?>img/icons/small_avis.png" alt="" class="icon2"><strong><?php echo $param['nbAvis'];?> avis postés</strong></p>
    <p><img src="<?php echo PATH;?>img/icons/postal_black.png" alt="" class="icon2"><strong><?php echo $param['nbNl'];?> abonnés à la newsletter</strong></p>
    <p><img src="<?php echo PATH;?>img/icons/small_affaires.png" alt="" class="icon2"><strong>Chiffre d'affaires : <span class="green"><?php echo number_format($param['chiffreAffaire'],2, ',', ' ')?> &euro;</span></strong></p>
</div>

<div class="stats">
    <h1><img src="<?php echo PATH;?>img/icons/middle_topventes.png" alt="" class="icon">Top des ventes</h1>
    <div class="hr"></div>

    <table>
        <thead><tr>
            <th>Salle</th><th>Photo</th><th>Ville</th><th>Ventes</th>
        </tr></thead>
        <?php
            if(isset($param['topventes'])){
                foreach($param['topventes'] as $key => $salle){
                    echo '<tr>';
                    echo '<td>' .ucfirst($salle->salle_titre) .'</td>';
                    echo '<td><a href="'.HOME .'reservation/salles/' .$salle->salle_titre .'" title="Voir la salle"><img src="' .PATH .'img/rooms/' .strtolower($salle->salle_titre) .'_preview.jpg" alt="" class="salle-preview"></a></td>';
                    echo '<td>' .$salle->salle_ville .'</td>';
                    echo '<td>' .$salle->nbVentes .'</td>';
                    echo '</tr>';
                }
            }
        ?>
    </table>
</div>

<div class="stats">
    <h1><img src="<?php echo PATH;?>img/icons/middle_topnotes.png" alt="" class="icon">Clients les plus rentables</h1>
    <div class="hr"></div>

    <table>
        <thead><tr>
            <th>Id</th><th>Pseudo</th><th>Ville</th><th>Montant</th>
        </tr></thead>
        <?php
            if(isset($param['topclientsrentables'])){
                foreach($param['topclientsrentables'] as $key => $membre){
                    echo '<tr>';
                    echo '<td>' .$membre->membre_id .'</td>';
                    echo '<td>' .$membre->membre_pseudo .'</td>';
                    echo '<td>' .$membre->membre_ville .'</td>';
                    echo '<td>' .number_format($membre->montantTotal,2,',',' ') .' &euro;</td>';
                    echo '</tr>';
                }
            }
        ?>
    </table>
</div>

<div class="stats">
    <h1><img src="<?php echo PATH;?>img/icons/middle_topclients.png" alt="" class="icon">Clients les plus fidèles</h1>
    <div class="hr"></div>

    <table>
        <thead><tr>
            <th>Id</th><th>Pseudo</th><th>Ville</th><th>Commandes</th>
        </tr></thead>
        <?php
            if(isset($param['topclients'])){
                foreach($param['topclients'] as $key => $membre){
                    echo '<tr>';
                    echo '<td>' .$membre->membre_id .'</td>';
                    echo '<td>' .$membre->membre_pseudo .'</td>';
                    echo '<td>' .$membre->membre_ville .'</td>';
                    echo '<td>' .$membre->nbCommandes .'</td>';
                    echo '</tr>';
                }
            }
        ?>
    </table>
</div>
