<div class="gestion-title"><img src="<?php echo PATH;?>img/icons/title_gestionsalles.png" alt="" class="icon2">Gestion des salles</div>
<div class="add" id="ajouter"><img src="<?php echo PATH;?>img/icons/small_add.png" alt="" class="icon2">Ajouter une salle</div>

<div class="count"><img src="<?php echo PATH;?>img/icons/middle_count.png" alt="" class="icon2"><?php if(isset($param['salles'])) echo count($param['salles']) .' salles enregistrées';?></div>

<table>
    <thead><tr>
        <th>Id</th><th>Salle</th><th>Photo</th><th>Ville</th><th>Adresse</th><th>Code Postal</th><th>Description</th><th>Capacité</th><th>Catégorie</th><th>Éditer</th><th>Suppr.</th>
    </tr></thead>
    <?php
        if(isset($param['salles'])){
            foreach($param['salles'] as $key => $salle){
                echo '<tr>';
                echo '<td>' .$salle->salle_id .'</td>';
                echo '<td>' .ucfirst($salle->salle_titre) .'</td>';
                echo '<td><a href="'.HOME .'reservation/salles/' .$salle->salle_titre .'" title="Voir la salle"><img src="' .PATH .'img/rooms/' .strtolower($salle->salle_titre) .'_preview.jpg" alt="" class="salle-preview"></a></td>';
                echo '<td>' .$salle->salle_ville .'</td>';
                echo '<td>' .$salle->salle_adresse .'</td>';
                echo '<td>' .$salle->salle_cp .'</td>';
                echo '<td>' .substr($salle->salle_description,0,40) .' <span style="color:grey">...</span></td>';
                echo '<td>' .$salle->salle_capacite .' <span class="small">pers.</span></td>';
                echo '<td>' .ucfirst($salle->salle_categorie) .'</td>';
                echo '<td><a href="'.HOME .'admin/gestion-salles/editer/'.$salle->salle_id .'"><img src="'.PATH .'img/icons/small_edit.png" alt="" class="edit"></a></td>';
                echo '<td><img src="'.PATH .'img/icons/small_croix.png" alt="" class="supprSalle" id="salle'.$salle->salle_titre .'"></td>';
                echo '</tr>';
            }
        }
    ?>
</table>

<div class="popin-ajouter">
    <img src="<?php echo PATH;?>img/icons/close.png" alt="" id="close">
    <h1><img src="<?php echo PATH;?>img/icons/title_add.png" alt="" class="icon2">Ajouter une salle</h1>
    <div class="hr"></div>

    <form action="<?php echo HOME;?>admin/gestion-salles/ajouter/" method="post" enctype="multipart/form-data" id="form-add">
        <div class="form-side">
            <div class="field-container"><label for="salle_titre">Nom : </label><input type="text" name="salle_titre" placeholder="Nom" id="salle_titre"></div>
            <div class="field-container"><label for="salle_photo">Photo : </label><input type="file" name="salle_photo" placeholder="Envoyez une photo" id="salle_photo"></div>
            <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
            <div class="field-container"><label for="salle_description">Description : </label><textarea name="salle_description" placeholder="Description" id="salle_description"></textarea></div>
        </div>

        <div class="form-side">
            <div class="field-container"><label for="salle_categorie">Catégorie : </label><input type="text" name="salle_categorie" placeholder="Catégorie" id="salle_categorie"></div>
            <div class="field-container"><label for="salle_capacite">Capacité : </label><input type="text" name="salle_capacite" placeholder="Capacité" id="salle_capacite"></div>
            <div class="field-container"><label for="salle_ville">Ville : </label><input type="text" name="salle_ville" placeholder="Ville" id="salle_ville"></div>
            <div class="field-container"><label for="salle_cp">Code Postal : </label><input type="text" name="salle_cp" placeholder="Code Postal" id="salle_cp"></div>
            <div class="field-container"><label for="salle_adresse">Adresse : </label><textarea name="salle_adresse" placeholder="Adresse" id="salle_adresse"></textarea></div>
        </div>
    </form>

    <div id="submit">Valider</div>
</div>






<?php if(isset($param['salle'])){?>

    <div class="popin-salle">
        <img src="<?php echo PATH;?>img/icons/close.png" alt="" id="close">
        <h1><img src="<?php echo PATH;?>img/icons/title_edit.png" alt="" class="icon2">Modifier une salle</h1>
        <div class="hr"></div>

        <form action="<?php echo HOME;?>admin/gestion-salles/editer/<?php echo $param['salle']->salle_id;?>" method="post" id="form-edit" enctype="multipart/form-data">
            <div class="form-side">
                <div class="field-container"><label for="salle_titre">Nom : </label><input type="text" name="salle_titre" placeholder="Nom" id="salle_titre"  value="<?php echo $param['salle']->salle_titre;?>"></div>
                <p><img src="<?php echo PATH;?>img/rooms/<?php echo strtolower($param['salle']->salle_titre);?>_preview.jpg" alt="" class="photo"></p>
                <div class="field-container"><label for="salle_photo">Photo : </label><input type="file" name="salle_photo" placeholder="Envoyez une photo" id="salle_photo"></div>
                <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
                <div class="field-container"><label for="salle_description">Description : </label><textarea name="salle_description" placeholder="Description" id="salle_description"><?php echo $param['salle']->salle_description;?></textarea></div>
            </div>

            <div class="form-side">
                <div class="field-container"><label for="salle_categorie">Catégorie : </label><input type="text" name="salle_categorie" placeholder="Catégorie" id="salle_categorie" value="<?php echo ucfirst($param['salle']->salle_categorie);?>"></div>
                <div class="field-container"><label for="salle_capacite">Capacité : </label><input type="text" name="salle_capacite" placeholder="Capacité" id="salle_capacite" value="<?php echo $param['salle']->salle_capacite;?>"></div>
                <div class="field-container"><label for="salle_ville">Ville : </label><input type="text" name="salle_ville" placeholder="Ville" id="salle_ville" value="<?php echo $param['salle']->salle_ville;?>"></div>
                <div class="field-container"><label for="salle_cp">Code Postal : </label><input type="text" name="salle_cp" placeholder="Code Postal" id="salle_cp" value="<?php echo $param['salle']->salle_cp;?>"></div>
                <div class="field-container"><label for="salle_adresse">Adresse : </label><textarea name="salle_adresse" placeholder="Adresse" id="salle_adresse"><?php echo $param['salle']->salle_adresse;?></textarea></div>
            </div>
        </form>

        <div id="submit">Valider</div>
    </div>

<?php
}
?>

<script>
    <?php
    if(isset($param['salle'])) echo 'var editerSalle = true;'; else echo 'var editerSalle = false;';
    if(isset($param['ajouter'])) echo 'var ajouterSalle = true;'; else echo 'var ajouterSalle = false;';
    if(isset($param['edit-error'])) echo 'var editerror = true;'; else echo 'var editerror = false;';
    ?>
</script>
