<div class="gestion-title"><img src="<?php echo PATH;?>img/icons/title_gestioncommandes.png" alt="" class="icon2">Gestion des commandes</div>
<div class="count"><img src="<?php echo PATH;?>img/icons/middle_count.png" alt="" class="icon2">
    <?php
        if(isset($param['commandes'])){
            echo $param['nbCommandes'] .' commandes | <strong>Chiffre d\'affaires : <span class="green">' .number_format($param['chiffreAffaire'],2, ',', ' ') .' &euro;</span></strong>';
        }
    ?></div>


<table>
    <thead><tr>
        <th>Id</th><th>Réf</th><th>Pseudo</th><th>Date</th><th>Montant</th><th>Facture</th>
    </tr></thead>
    <?php
        if(isset($param['commandes'])){
            foreach($param['commandes'] as $key => $commande){
                echo '<tr>';
                echo '<td>' .$commande->commande_id .'</td>';
                echo '<td>C0' .$commande->ref .'</td>';
                echo '<td>' .$commande->membre_pseudo .'</td>';
                echo '<td>' .$commande->commande_date .'</td>';
                echo '<td>' .number_format($commande->commande_montant,2,',',' ') .' &euro;</td>';
                echo '<td><a style="color:#000000;" href="' .HOME .'commande/facture/' .$commande->commande_id .'">Voir</a></td>';
                echo '</tr>';
            }
        }
    ?>
</table>
