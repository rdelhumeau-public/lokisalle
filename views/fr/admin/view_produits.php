<div class="gestion-title"><img src="<?php echo PATH;?>img/icons/title_gestionproduits.png" alt="" class="icon2">Gestion des produits</div>

<div class="add" id="ajouterProduit"><img src="<?php echo PATH;?>img/icons/small_add.png" alt="" class="icon2">Ajouter un produit</div>
<div class="count"><img src="<?php echo PATH;?>img/icons/middle_count.png" alt="" class="icon2"><?php if(isset($param['produits'])) echo $param['nbProduits'] .' produits | <span class="bordeaux">' .$param['nbReserved'] .' réservés</span> | <span class="green">' .($param['nbProduits'] - $param['nbReserved']) .' disponibles </span>';?></div>

<table>
    <thead><tr>
        <th>Id</th><th>Salle</th><th>Photo</th>
        <th>Arrivée <a href="<?php echo HOME;?>admin/gestion-produits/tri/produit_arrivee/decroissant"><img src="<?php echo PATH;?>img/icons/tri_decroissant.png" alt="" class="tri"></a>
            <a href="<?php echo HOME;?>admin/gestion-produits/tri/produit_arrivee/croissant"><img src="<?php echo PATH;?>img/icons/tri_croissant.png" alt="" class="tri"></a></th>

        <th>Départ <a href="<?php echo HOME;?>admin/gestion-produits/tri/produit_depart/decroissant"><img src="<?php echo PATH;?>img/icons/tri_decroissant.png" alt="" class="tri"></a>
            <a href="<?php echo HOME;?>admin/gestion-produits/tri/produit_depart/croissant"><img src="<?php echo PATH;?>img/icons/tri_croissant.png" alt="" class="tri"></a></th>

        <th>Prix <a href="<?php echo HOME;?>admin/gestion-produits/tri/produit_prix/decroissant"><img src="<?php echo PATH;?>img/icons/tri_decroissant.png" alt="" class="tri"></a>
            <a href="<?php echo HOME;?>admin/gestion-produits/tri/produit_prix/croissant"><img src="<?php echo PATH;?>img/icons/tri_croissant.png" alt="" class="tri"></th></a>

        <th>Dispo</th><th>Éditer</th><th>Suppr.</th>
    </tr></thead>
    <?php
        if(isset($param['produits'])){
            foreach($param['produits'] as $key => $produit){
                echo '<tr>';
                echo '<td>' .$produit->produit_id .'</td>';
                echo '<td>' .ucfirst($produit->salle_titre) .'</td>';
                echo '<td><a href="'.HOME .'reservation/salles/' .$produit->salle_titre .'" title="Voir la salle"><img src="' .PATH .'img/rooms/' .strtolower($produit->salle_titre) .'_preview.jpg" alt="" class="salle-preview"></a></td>';
                echo '<td>' .$produit->produit_arrivee_date .' - <span class="small">' .$produit->produit_arrivee_heure .'</span></td>';
                echo '<td>' .$produit->produit_depart_date .' - <span class="small">' .$produit->produit_depart_heure .'</span></td>';
                echo '<td>' .number_format($produit->produit_prix,2, ',', ' ') .' &euro;</td>';

                if($produit->produit_etat == 0){
                    echo '<td><span class="green">Disponible</span></td>';
                }
                else{
                    echo '<td><span class="bordeaux">Réservé</span></td>';
                }

                if($produit->produit_etat == 0) {
                    echo '<td><a href="' . HOME . 'admin/gestion-produits/editer/' . $produit->produit_id . '"><img src="' . PATH . 'img/icons/small_edit.png" alt="" class="edit"></a></td>';
                    echo '<td><img src="' . PATH . 'img/icons/small_croix.png" alt="" class="supprProduit" id="produit' . $produit->produit_id . '"></td>';
                }
                else{
                    echo '<td>-</td><td>-</td>';
                }
                echo '</tr>';
            }
        }
    ?>
</table>


<div class="popin-produit-ajouter">
    <img src="<?php echo PATH;?>img/icons/close.png" alt="" id="close">
    <h1><img src="<?php echo PATH;?>img/icons/title_add.png" alt="" class="icon2">Ajouter un produit</h1>
    <div class="hr"></div>

    <form action="<?php echo HOME;?>admin/gestion-produits/ajouter/" method="post" id="form-add-produit">
        <div class="form-side">
            <div class="field-container"><label for="produit_salle_id">Salle : </label>
                <select name="produit_salle_id" id="produit_salle_id">
                    <?php foreach($param['sallesListe'] as $key => $salle){
                            echo '<option value="' . $salle->salle_id . '">' . $salle->salle_id . ' - ' . $salle->salle_titre . ' - ' . $salle->salle_ville . ' - ' . $salle->salle_capacite . ' pers</option>';
                    }?>
                </select></div>
            <div class="field-container"><label for="produit_prix">Prix : </label><input type="text" name="produit_prix" id="produit_prix" style="text-align: right;padding-right: 15px;"> &euro;</div>
        </div>

        <div class="form-side">
            <div class="field-container"><label for="produit_arrivee">Date d'arrivée : </label><input type="text" name="produit_arrivee" id="produit_arrivee" class="datepicker"></div>
            <div class="field-container"><label for="produit_depart">Date de départ : </label><input type="text" name="produit_depart" id="produit_depart" class="datepicker"></div>
        </div>
    </form>

    <div id="submit">Valider</div>
</div>




<?php if(isset($param['produit'])){?>

    <div class="popin-produit">
        <img src="<?php echo PATH;?>img/icons/close.png" alt="" id="close">
        <h1><img src="<?php echo PATH;?>img/icons/title_edit.png" alt="" class="icon2">Modifier un produit</h1>
        <div class="hr"></div>

        <form action="<?php echo HOME;?>admin/gestion-produits/editer/<?php echo $param['produit']->produit_id;?>" method="post" id="form-edit-produit">
            <div class="form-side">
                <div class="field-container"><label for="produit_salle_id">Salle : </label>
                <select name="produit_salle_id" id="produit_salle_id">
                    <?php foreach($param['sallesListe'] as $key => $salle){
                        if($salle->salle_id == $param['produit']->produit_salle_id) {
                            echo '<option value="' . $salle->salle_id . '" selected>' . $salle->salle_id . ' - ' . $salle->salle_titre . ' - ' . $salle->salle_ville . ' - ' . $salle->salle_capacite . ' pers</option>';
                        }
                        else{
                            echo '<option value="' . $salle->salle_id . '">' . $salle->salle_id . ' - ' . $salle->salle_titre . ' - ' . $salle->salle_ville . ' - ' . $salle->salle_capacite . ' pers</option>';
                        }
                    }?>
                </select></div>
                <div class="field-container"><label for="produit_prix">Prix : </label><input type="text" name="produit_prix" id="produit_prix" value="<?php echo $param['produit']->produit_prix; ?>" style="text-align: right;padding-right: 15px;"> &euro;</div>
            </div>

            <div class="form-side">
                <div class="field-container"><label for="produit_arriveee">Date d'arrivée : </label><input type="text" name="produit_arrivee" id="produit_arriveee" class="datepicker" value="<?php echo $param['produit']->produit_arrivee_date; ?>"></div>
                <div class="field-container"><label for="produit_departt">Date de départ : </label><input type="text" name="produit_depart" id="produit_departt" class="datepicker"  value="<?php echo $param['produit']->produit_depart_date; ?>"></div>
            </div>
        </form>

        <div id="submit">Valider</div>
    </div>

<?php
}
?>

<script>
    <?php
    if(isset($param['produit'])) echo 'var editerProduit = true;'; else echo 'var editerProduit = false;';
    if(isset($param['ajouter'])) echo 'var ajouterProduit = true;'; else echo 'var ajouterProduit = false;';
    if(isset($param['edit-error'])) echo 'var editerror = true;'; else echo 'var editerror = false;';
    echo 'var annee =' .$param['annee'] .';';
    echo 'var date ="' .$param['date'] .'";';
    ?>
</script>
