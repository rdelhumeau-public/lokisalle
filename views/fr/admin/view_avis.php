<div class="gestion-title"><img src="<?php echo PATH;?>img/icons/title_gestionavis.png" alt="" class="icon2">Gestion des avis</div>
<div class="count"><img src="<?php echo PATH;?>img/icons/middle_count.png" alt="" class="icon2">
    <?php if(isset($param['avis'])) {
        if(count($param['avis']) > 1) {
            echo count($param['avis']) . ' avis enregistrés ';
        }
        else{
            echo count($param['avis']) . ' avi enregistré ';
        }
    }?>
</div>




<table>
    <thead><tr>
        <th>Id</th><th>Salle</th><th>Pseudo</th><th>Commentaire</th><th>Note</th><th>Date</th><th>Suppr.</th>
    </tr></thead>
    <?php
        if(isset($param['avis'])){
            foreach($param['avis'] as $key => $value){
                echo '<tr>';
                echo '<td>' .$value->avis_id .'</td>';
                echo '<td>' .$value->salle_titre .'</td>';
                echo '<td>' .$value->membre_pseudo .'</td>';
                echo '<td>' .$value->avis_texte .'</td>';
                echo '<td>' .$value->avis_note .'</td>';
                echo '<td>' .$value->avis_date .' - ' .$value->avis_heure .'</td>';
                echo '<td><img src="' . PATH . 'img/icons/small_croix.png" alt="" class="supprAvis" id="avis' . $value->avis_id . '"></td>';
                echo '</tr>';
            }
        }
    ?>
</table>