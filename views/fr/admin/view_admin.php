<div id="page">

    <div id="admin-dashboard">
        <a href="<?php echo HOME;?>admin"><img src="<?php echo PATH;?>img/icons/dash_admin.png" alt="" class="boss"></a>
        <p class="title">Administration</p>

        <div id="commandes">

        <a href="<?php echo HOME;?>admin/gestion-salles"><div class="boutons <?php if(isset($param['page-admin']) && $param['page-admin'] == 'salles') echo 'actif';?>"><img src="<?php echo PATH;?>img/icons/small_gestionsalles.png" class="icon2">Gestion des salles</div></a>
        <a href="<?php echo HOME;?>admin/gestion-produits"><div class="boutons <?php if(isset($param['page-admin']) && $param['page-admin'] == 'produits') echo 'actif';?>"><img src="<?php echo PATH;?>img/icons/small_gestionproduits.png" class="icon2">Gestion des produits</div></a>
        <a href="<?php echo HOME;?>admin/gestion-membres"><div class="boutons <?php if(isset($param['page-admin']) && $param['page-admin'] == 'membres') echo 'actif';?>"><img src="<?php echo PATH;?>img/icons/small_gestionmembres.png" class="icon2">Gestion des membres</div></a>
        <a href="<?php echo HOME;?>admin/gestion-avis"><div class="boutons <?php if(isset($param['page-admin']) && $param['page-admin'] == 'avis') echo 'actif';?>"><img src="<?php echo PATH;?>img/icons/small_gestionavis.png" class="icon2">Gestion des avis</div></a>
        <a href="<?php echo HOME;?>admin/gestion-commandes"><div class="boutons <?php if(isset($param['page-admin']) && $param['page-admin'] == 'commandes') echo 'actif';?>"><img src="<?php echo PATH;?>img/icons/small_gestioncommandes.png" class="icon2">Gestion des commandes</div></a>
        <a href="<?php echo HOME;?>admin/envoi-newsletter"><div class="boutons <?php if(isset($param['page-admin']) && $param['page-admin'] == 'newsletter') echo 'actif';?>"><img src="<?php echo PATH;?>img/icons/small_envoinewsletter.png" class="icon2">Envoi de la newsletter</div></a>

        </div>

    </div>

    <?php if(isset($param['page-admin'])) include_once(ROOT .'views' .DS .$lang .DS .'view_' .$param['page-admin']) .'.php';?>
</div>