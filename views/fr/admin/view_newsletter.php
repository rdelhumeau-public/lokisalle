<div class="gestion-title"><img src="<?php echo PATH;?>img/icons/title_envoinewsletter.png" alt="" class="icon2">Envoi de la newsletter</div>
<div class="count"><img src="<?php echo PATH;?>img/icons/middle_count.png" alt="" class="icon2">
    <?php
        if(isset($param['nbNl'])){
            echo $param['nbNl'] .' abonnés';
        }
    ?></div>


<img src="<?php echo PATH;?>img/mail_header.png" alt="" class="email-preview">

<form method="post" action="<?php echo HOME;?>admin/envoi-newsletter/envoyer" id="newsletter-form">

    <textarea name="newsletter_message"></textarea>
    <input type="submit" id="submit" value="Envoyer">
</form>

<img src="<?php echo PATH;?>img/mail_footer.png" alt="" class="email-preview">


<div style="margin:60px 0 15px 55px;">Membres abonnés : <?php

        if(isset($param['pseudos'])){
            $i=0;
            foreach($param['pseudos'] as $key => $pseudos){
                if($i == 0){
                    echo $pseudos->membre_pseudo;
                }else{
                    echo ', '.$pseudos->membre_pseudo;
                }
                $i++;
            }
        }

    ?></div>
<script>
    <?php
     if(isset($param['envoi'])) echo 'var envoi = true;'; else echo 'var envoi = false;';
    ?>
</script>