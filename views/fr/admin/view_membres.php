<div class="gestion-title"><img src="<?php echo PATH;?>img/icons/title_gestionmembres.png" alt="" class="icon2">Gestion des membres</div>
<div class="add" id="addAdmin"><img src="<?php echo PATH;?>img/icons/small_add.png" alt="" class="icon2">Ajouter un nouvel administrateur</div>
<div class="count"><img src="<?php echo PATH;?>img/icons/middle_count.png" alt="" class="icon2">
    <?php if(isset($param['membres'])) {
        if(count($param['membres']) > 1) {
            echo count($param['membres']) . ' membres enregistrés ';
        }
        else{
            echo count($param['membres']) . ' membre enregistré ';
        }
    }?>
</div>



<table>
    <thead><tr>
        <th>Id</th><th>Pseudo</th><th>Email</th><th>Nom</th><th>Prénom</th><th>Sexe</th><th>Ville</th>
        <th>Code Postal</th><th>Adresse</th><th>Statut</th><th>Suppr.</th>
    </tr></thead>
    <?php
        if(isset($param['membres'])){
            foreach($param['membres'] as $key => $membre){
                echo '<tr>';
                echo '<td>' .$membre->membre_id .'</td>';
                echo '<td>' .$membre->membre_pseudo .'</td>';
                echo '<td>' .$membre->membre_email .'</td>';
                echo '<td>' .$membre->membre_nom .'</td>';
                echo '<td>' .$membre->membre_prenom .'</td>';

                if($membre->membre_sexe == 'h'){
                    echo '<td>Homme</td>';
                }
                else{
                    echo '<td>Femme</td>';
                }

                echo '<td>' .$membre->membre_ville .'</td>';
                echo '<td>' .$membre->membre_cp .'</td>';
                echo '<td>' .htmlspecialchars($membre->membre_adresse) .'</td>';

                if($membre->membre_statut < 5) {
                    echo '<td class="green">Membre</td>';
                }
                elseif($membre->membre_statut == 10){
                    echo '<td class="red"><img src="'.PATH .'img/icons/small_crown.png" alt="" class="icon">Admin</td>';
                }
                else{
                    echo '<td class="red">Admin</td>';
                }
                echo '<td><img src="' . PATH . 'img/icons/small_croix.png" alt="" class="supprMembre" id="membre' . $membre->membre_pseudo . '"></td>';
                echo '</tr>';
            }
        }
    ?>
</table>

<?php if(isset($param['membres'])){?>


<div class="popin-membre-admin">
    <img src="<?php echo PATH;?>img/icons/close.png" alt="" id="close">
    <h1><img src="<?php echo PATH;?>img/icons/title_add.png" alt="" class="icon2">Ajouter un administrateur</h1>
    <div class="hr"></div>

    <form action="<?php echo HOME;?>admin/gestion-membres/ajouter-admin/" method="post" id="form-addadmin">
        <div class="form-side">
            <select name="membre_id">
            <?php
                foreach($param['membres'] as $key => $membre){
                    if($membre->membre_statut < 4){
                        echo '<option value="' .$membre->membre_id .'">' .$membre->membre_pseudo .'</option>';
                    }
                }
            ?>
            </select>
    </form>

    <div id="submit">Valider</div>
</div>

<?php
}
?>