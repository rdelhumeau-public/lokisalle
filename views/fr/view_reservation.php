<div id="page">

    <h1><img src="<?php echo PATH;?>img/icons/title_key.png" alt="" class="icon2">Réservation</h1>
    <div class="hr"></div>

    <div id="rooms-list">
        <?php
            $i = $p = 1;

            foreach($param['salles'] as $key => $value){
            if($i == 1) echo '<div class="page-list" id="page' .$p .'">';
            if($i%12 == 0) {
                $p++;
                echo '</div><div class="page-list" id="page' . $p . '">';
            }
                ?>
            <a href="<?php echo HOME .'reservation/salles/' .strtolower(urldecode($value->salle_titre));?>">
                <div class="room-list-selection" id="list<?php echo $value->salle_id;?>">
                <p>
                    <img src="<?php echo PATH;?>img/rooms/<?php echo strtolower(urldecode($value->salle_titre)); ?>_preview.jpg" class="mini-preview">
                    <span class="room-list-title"><?php echo $value->salle_titre; ?></span>
                    <span class="separator">|</span>
                    <?php echo $value->salle_ville; ?>
                    <span class="separator">|</span>
                    <?php echo $value->salle_capacite; ?> personnes
                </p>

                </div>
            </a>
        <?php
            $i++;
            }?>
        </div>

    <?php if($i>12){
        echo '<div id="page-nav">Pages : ';

        for($j=1;$j<=$p;$j++){
            echo '<div class="page-link" id="page-link' .$j .'">';
            if($j>1) echo ' - ';
            echo $j .'</div> ';
        }
        echo '</div>';
    }
    ?>
    </div>

    <div id="room-preview">

        <div id="back">
            <img src="<?php echo PATH;?>img/main_room03.jpg" alt="" class="room-back">
            <div id="back-legend">
                <img src="<?php echo PATH;?>img/lokisalle.png" alt="Lokisalle">
                <p>Réunions professionnelles, réceptions privées, mariages...
                    Dénichez la perle rare parmi nos <?php echo $param['nbSalles'];?> salles disponibles dans toute la France !</p>
            </div>
        </div>

        <?php foreach($param['salles'] as $key => $value){ ?>

            <div class="room-details" id="room<?php echo $value->salle_id;?>">
                <p class="room-title"><?php echo $value->salle_titre; ?></p>
                <img src="<?php echo PATH;?>img/rooms/<?php echo strtolower(urldecode($value->salle_titre)); ?>_preview.jpg" alt="" class="preview">

                <div class="room-details-left">
                    <p><img src="<?php echo PATH;?>img/icons/middle_place.png" alt="Adresse" class="icon"> <strong><?php echo $value->salle_cp .' - ' .strtoupper($value->salle_ville); ?></strong></p>
                    <p class="indent"><?php echo $value->salle_adresse; ?></p>
                    <p><img src="<?php echo PATH;?>img/icons/middle_capacite.png" alt="Capacité" class="icon"> <?php echo ucfirst($value->salle_categorie) .' - ' .$value->salle_capacite; ?> personnes</p>
                    <p style="margin-top:10px;"><img src="<?php echo PATH;?>img/icons/middle_key.png" alt="Disponibilité" class="icon"> <?php if($value->salle_dispo) echo '<span style="color:green;">Offres disponibles</span>'; else echo '<span class="red">Aucune offre disponible</span>'; ?></p>
                </div>
                <div class="room-details-right">
                    <p><img src="<?php echo PATH;?>img/icons/middle_description.png" alt="Description" class="icon">
                        <?php echo ucfirst($value->salle_description); ?>
                    </p>
                </div>

            </div>
        <?php }?>
    </div>

</div>
