
<div id="raphael-slider">

    <div id="slider-container">
        <div id="slider1"><img src="<?php echo PATH; ?>img/slider/slide1.jpg" alt=""></div>
        <div id="slider2"><img src="<?php echo PATH; ?>img/slider/slide2.jpg" alt=""></div>
        <div id="slider3"><img src="<?php echo PATH; ?>img/slider/slide3.jpg" alt=""></div>


        <div id="text1">Des salles à Paris, Lyon, Marseille et dans toute la France pour vos réunions d'entreprise.</div>
        <div id="text2">Un équipement moderne et des hôtesses à votre service pour le confort de vos invités.</div>
        <div id="text3">Avec Lokisalle, construisez votre avenir dans les meilleures conditions.</div>

        <div id="btn-slide1"><img src="<?php echo PATH; ?>img/slider/btn_slide1.png" alt=""></div>
        <div id="btn-slide2"><img src="<?php echo PATH; ?>img/slider/btn_slide2.png" alt=""></div>
        <div id="btn-slide3"><img src="<?php echo PATH; ?>img/slider/btn_slide3.png" alt=""></div>

        <div id="click-button1"><img src="<?php echo PATH; ?>img/slider/btn_slide_click.png" alt=""></div>
        <div id="click-button2"><img src="<?php echo PATH; ?>img/slider/btn_slide_click.png" alt=""></div>
        <div id="click-button3"><img src="<?php echo PATH; ?>img/slider/btn_slide_click.png" alt=""></div>

        <div id="button-back"><img src="<?php echo PATH; ?>img/slider/btn_slide_back.png" alt=""></div>
    </div>

</div>

<div class="left-side">
    <h1 class="title">Besoin d'un cadre de qualité pour organiser vos réunions ? Faites confiance à  <span class="brown"><strong>Lokisalle</strong></span> !</h1>
    <p><br>Leader incontesté de la location de salles en France pour les particuliers et les entreprises, <span class="brown"><strong>Lokisalle</strong></span> vous met à disposition des salles de réunion de haute qualité à Paris, Lyon, Marseille et dans toute la France.</p>
    <p><br>Soucieux du bien être de vos collaborateurs, vous cherchez une solution efficace à la hauteur de vos exigences et de l'image de votre entreprise.</p>
    <p>N'attendez plus ! Inscrivez-vous sur notre site et réservez en quelques clics la salle qui répond à vos besoins.</p>
    <img src="<?php echo PATH; ?>img/index1.jpg" alt="reunion">
    <p><br>Nos salles peuvent accueillir jusqu'à 75 personnes dans un cadre moderne et harmonieux. Vous disposerez de tout l'équipement nécessaire au bon déroulement de vos présentations.</p>
    <p><br>Du personnel qualifié est présent sur place pour vous accompagner dans l'organisation de vos événements et veiller au confort de vos invités.</p>
    <p><br>Pour plus d'informations, contactez-nous via la page <strong><a href="<?php echo HOME; ?>contact" class="brown">contact</a></strong> de notre site, par téléphone au 01 47 20 00 01 ou venez nous rencontrer au 1 rue Boswellia à Paris.</p>
    <p><br>Choisir la qualité <span class="brown"><strong>Lokisalle</strong></span>, c'est garantir le meilleur pour la réalisation de vos projets et renforcer la cohésion de vos équipes.</p>
</div>

<div class="right-side">
    <div class="title">Nos 3 dernières offres :</div>
    <?php foreach($param['lastOffers'] as $key => $value){?>
        <div class="last-offer">
            <p><a href="<?php echo HOME .'reservation/salles/' .$value->salle_titre;?>"><img src="<?php echo PATH;?>img/rooms/<?php echo strtolower($value->salle_titre);?>_preview.jpg" alt="" class="preview-offer"></a></p>
            <p style="font-size:18px;"><strong><?php echo $value->salle_titre;?></strong></p>
            <p><img src="<?php echo PATH;?>img/icons/small_place.png" alt="Adresse"> <?php echo $value->salle_ville . ' - ' .$value->salle_capacite .' personnes';?></p>
            <p><?php
                    if($value->produit_arrivee_date != $value->produit_depart_date){
                        echo 'Du ' .$value->produit_arrivee_date .' au ' .$value->produit_depart_date;
                    }
                    else{
                        echo 'Le ' .$value->produit_arrivee_date .' <span style="font-size:14px">de '.$value->produit_arrivee_heure  .' à ' .$value->produit_depart_heure .'</span>';
                    }
                ?></p>
            <p><?php echo number_format($value->produit_prix,2,',',' ');?> &euro; HT</p>
            <a href="<?php echo HOME .'reservation/salles/' .$value->salle_titre;?>"><div class="offer-voir">Voir</div></a>
            <a href="<?php echo HOME .'panier/ajouter/' .$value->produit_id;?>"><div class="offer-voir"><img src="<?php echo PATH;?>img/icons/middle_panier_white.png" alt="" class="icon4"></div></a>
        </div>
    <?php } ?>
</div>

<script>
<?php
    if($param['first_visit']) echo 'var first_visit = true;'; else echo 'var first_visit = false;';
    if(isset($param['mdp_changed']) && $param['mdp_changed']) echo 'var mdp_changed = true;'; else echo 'var mdp_changed = false;';
    if(isset($param['mdp_session_expired']) && $param['mdp_session_expired']) echo 'var mdp_session_expired = true;'; else echo 'var mdp_session_expired = false;';
    if(isset($param['newsletter'])) echo 'var newsletter = true;'; else echo 'var newsletter = false;';
?>

</script>