<div id="page">

    <div id="popin-inscription">
        <a href="<?php echo $param['back_url']; ?>" title="Fermer"><img src="<?php echo PATH;?>img/icons/close.png" alt="" id="close"></a>
        <div id="lokisalle"><img src="<?php echo PATH;?>img/lokisalle.png" alt=""></div>
        <h1>Inscription</h1>
        <div class="hr"></div>

        <form action="<?php echo HOME;?>membre/inscription" method="post" id="form_inscription">
            <div class="form-side">
            <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_pseudo_white.png" alt="" class="icon2"><input type="text" name="membre_pseudo" placeholder="Pseudo" id="membre_pseudo"></div>
            <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_lock_white.png" alt="" class="icon2"><input type="password" name="membre_mdp" placeholder="Mot de passe" id="membre_mdp"></div>
            <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_lock_white.png" alt="" class="icon2"><input type="password" name="membre_mdpConfirmation" placeholder="Confirmez votre mot de passe" id="membre_mdpConfirmation"></div>
            <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_id_white.png" alt="" class="icon2"><input type="text" name="membre_nom" placeholder="Nom" id="membre_nom"></div>
            <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_id_white.png" alt="" class="icon2"><input type="text" name="membre_prenom" placeholder="Prénom" id="membre_prenom"></div>
            <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_email_white.png" alt="" class="icon2"><input type="email" name="membre_email" placeholder="Adresse email" id="membre_email"></div>
            </div>

            <div class="form-side">
                <div class="field-container">
                    <div id="membre_sexe"><img src="<?php echo PATH;?>img/icons/middle_sex_white.png" alt="" class="icon2">
                <label for="homme">Homme</label><input type="radio" name="membre_sexe" value="h" id="homme" required>
                <label for="femme">Femme</label><input type="radio" name="membre_sexe" value="f" id="femme" required></div>
                </div>

            <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_city_white.png" alt="" class="icon2"><input type="text" name="membre_ville" placeholder="Ville" id="membre_ville"></div>
            <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_postal_white.png" alt="" class="icon2"><input type="text" name="membre_cp" placeholder="Code Postal" id="membre_cp"></div>
            <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_mailbox_white.png" alt="" class="icon2"><textarea name="membre_adresse" placeholder="Adresse" id="membre_adresse"></textarea></div>
            </div>
        </form>

        <div id="submit">Valider</div>
    </div>

</div>