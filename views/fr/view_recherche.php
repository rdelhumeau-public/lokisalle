<div id="page">

    <h1><img src="<?php echo PATH;?>img/icons/title_search.png" alt="" class="icon2"> Recherche</h1>
    <div class="hr"></div>

    <div id="recherche">
        <img src="<?php echo PATH;?>img/main_room04.jpg" alt="" class="photo">
        <div id="back-recherche">
            <img src="<?php echo PATH;?>img/lokisalle.png" alt="Lokisalle" class="loki">
            <p><strong>Rechercher par :</strong></p>
                <form method="post" action="<?php echo HOME;?>recherche" id="search_form">

                    <input type="radio" name="filtre" value="categorie" id="categorie" <?php if(!isset($param['filtre']['defaut']) || $param['filtre']['defaut'] == 'categorie') echo 'checked' ?>><label for="categorie">Catégorie</label>
                    <input type="radio" name="filtre" value="ville" id="ville" <?php if(isset($param['filtre']['defaut']) && $param['filtre']['defaut'] == 'ville') echo 'checked' ?>><label for="ville">Ville</label>
                    <input type="radio" name="filtre" value="capacite" id="capacite" <?php if(isset($param['filtre']['defaut']) && $param['filtre']['defaut'] == 'capacite') echo 'checked' ?>><label for="capacite">Capacité</label>
                    <input type="radio" name="filtre" value="prix" id="prix" <?php if(isset($param['filtre']['defaut']) && $param['filtre']['defaut'] == 'prix') echo 'checked' ?>><label for="prix">Prix</label>
                    <input type="radio" name="filtre" value="date" id="date" <?php if(isset($param['filtre']['defaut']) && $param['filtre']['defaut'] == 'date') echo 'checked' ?>><label for="date">Date</label>

                    <div class="filtre">
                        <div id="filtre_categorie" class="filtre-dynamique">
                            <label for="cat">Catégorie :</label>
                            <select name="filtre_categorie" id="cat">
                                <?php foreach($param['filtres']['categories'] as $key => $categorie){
                                    if((isset($param['filtre']['defaut']) && $categorie == $param['valeur']['defaut'])
                                        || (!isset($param['filtre']['defaut']) && $categorie == 'réunion')
                                        || (isset($param['filtre']['defaut']) && $param['filtre']['defaut'] != 'categorie' && $categorie == 'réunion')){
                                        echo '<option value="'.$categorie .'" selected>' .ucfirst($categorie) .'</option>';
                                    }
                                    else{
                                        echo '<option value="'.$categorie .'">' .ucfirst($categorie) .'</option>';
                                    }
                                }?>
                            </select>
                        </div>

                        <div id="filtre_ville" class="filtre-dynamique">
                            <label for="vil">Ville :</label>
                            <select name="filtre_ville" id="vil">
                                <?php foreach($param['filtres']['villes'] as $key => $ville){
                                    if((isset($param['filtre']['defaut']) && $ville == $param['valeur']['defaut'])
                                        || (!isset($param['filtre']['defaut']) && $ville == 'Paris')
                                        || (isset($param['filtre']['defaut']) && $param['filtre']['defaut'] != 'ville' && $ville == 'Paris')){
                                        echo '<option value="'.$ville .'" selected>' .ucfirst($ville) .'</option>';
                                    }
                                    else{
                                        echo '<option value="'.$ville .'">' .ucfirst($ville) .'</option>';
                                    }
                                }?>
                            </select>
                        </div>

                        <div id="filtre_capacite" class="filtre-dynamique">
                            <label for="cap">Capacité :</label>
                                <input type="text" name="filtre_capacite" id="cap" <?php if(isset($param['filtre']['defaut']) && $param['filtre']['defaut'] == 'capacite') echo 'value="'.$param['valeur']['defaut'] .'"'; ?> placeholder="inférieur ou égal à"> <span class="small">pers.</span>
                        </div>

                        <div id="filtre_prix" class="filtre-dynamique">
                            <label for="pr">Prix :</label>
                            <input type="text" name="filtre_prix" id="pr" <?php if(isset($param['filtre']['defaut']) && $param['filtre']['defaut'] == 'prix') echo 'value="'.$param['valeur']['defaut'] .'"'; ?>  placeholder="inférieur ou égal à"> &euro;
                        </div>

                        <div id="filtre_date" class="filtre-dynamique">
                            <label>Date :</label>
                            <select name="filtre_date_mois">
                                <?php foreach($param['mois'] as $key => $mois){
                                    if((isset($param['filtre']['defaut']) && $param['filtre']['defaut'] == 'date' && $mois == $param['valeur']['defaut'][0])
                                        || (!isset($param['filtre']['defaut']) && $mois == $param['filtres']['mois'])
                                        || (isset($param['filtre']['defaut']) && $param['filtre']['defaut'] != 'date' && $mois == $param['filtres']['mois'])){
                                        echo '<option value="'.$mois .'" selected>'.ucfirst($mois) .'</option>';
                                    }
                                    else{
                                        echo '<option value="'.$mois .'">'.ucfirst($mois) .'</option>';
                                    }
                                } ?>
                            </select>
                            <select name="filtre_date_annee">
                                <?php
                                    echo '<option value="'.$param['filtres']['annee'] .'">'.$param['filtres']['annee'] .'</option>';

                                    if(isset($param['filtre']['defaut']) && $param['filtre']['defaut'] == 'date' && $param['valeur']['defaut'][1] == ($param['filtres']['annee'] +1)){
                                        echo '<option value="'. ($param['filtres']['annee'] +1) .'" selected>'. ($param['filtres']['annee'] +1) .'</option>';
                                    }
                                    else{
                                        echo '<option value="'. ($param['filtres']['annee'] +1) .'">'. ($param['filtres']['annee'] +1) .'</option>';
                                    }
                                ?>
                            </select>
                        </div>

                    </div>

                </form>
                <div id="submit"><img src="<?php echo PATH;?>img/icons/search.png" class="icon2">Rechercher</div>
        </div>
    </div>


<?php if(isset($param['salles'])): ?>

    <p><strong><?php echo $param['nbResultats'];?></strong>
        <?php if($param['nbResultats'] == 0){
            echo 'résultat trouvé';
        }
        elseif($param['nbResultats'] == 1){
            echo 'résultat trouvé :';
        }
        else{
            echo 'résultats trouvés :';
        }
        ?></p>

    <?php if($param['nbResultats'] >= 1): ?>
    <div id="rooms-list">
        <?php
            $i = $p = 1;

            foreach($param['salles'] as $key => $value){
                if($i == 1) echo '<div class="page-list" id="page' .$p .'">';
                if($i%12 == 0) {
                    $p++;
                    echo '</div><div class="page-list" id="page' . $p . '">';
                }
                ?>
                <a href="<?php echo HOME .'reservation/salles/' .strtolower(urldecode($value->salle_titre));?>">
                    <div class="room-list-selection" id="list<?php echo $value->salle_id;?>">
                        <p>
                            <img src="<?php echo PATH;?>img/rooms/<?php echo strtolower(urldecode($value->salle_titre)); ?>_preview.jpg" class="mini-preview">
                            <span class="room-list-title"><?php echo $value->salle_titre; ?></span>
                            <span class="separator">|</span>
                            <?php echo $value->salle_ville; ?>
                            <span class="separator">|</span>
                            <?php echo $value->salle_capacite; ?> personnes
                        </p>

                    </div>
                </a>
                <?php
                $i++;
            }?>
    </div>

    <?php if($i>12){
        echo '<div id="page-nav">Pages : ';

        for($j=1;$j<=$p;$j++){
            echo '<div class="page-link" id="page-link' .$j .'">';
            if($j>1) echo ' - ';
            echo $j .'</div> ';
        }
        echo '</div>';
    }
    ?>
</div>

<div id="room-preview">

    <div id="back">
        <img src="<?php echo PATH;?>img/main_room03.jpg" alt="" class="room-back">
        <div id="back-legend">
            <img src="<?php echo PATH;?>img/lokisalle.png" alt="Lokisalle">
            <p>Réunions professionnelles, réceptions privées, mariages...
                Dénichez la perle rare parmi nos <?php echo $param['nbSalles'];?> salles disponibles dans toute la France !</p>
        </div>
    </div>

    <?php foreach($param['salles'] as $key => $value){ ?>

        <div class="room-details" id="room<?php echo $value->salle_id;?>">
            <p class="room-title"><?php echo $value->salle_titre; ?></p>
            <img src="<?php echo PATH;?>img/rooms/<?php echo strtolower(urldecode($value->salle_titre)); ?>_preview.jpg" alt="" class="preview">

            <div class="room-details-left">
                <p><img src="<?php echo PATH;?>img/icons/middle_place.png" alt="Adresse" class="icon"> <strong><?php echo $value->salle_cp .' - ' .strtoupper($value->salle_ville); ?></strong></p>
                <p class="indent"><?php echo $value->salle_adresse; ?></p>
                <p><img src="<?php echo PATH;?>img/icons/middle_capacite.png" alt="Capacité" class="icon"> <?php echo ucfirst($value->salle_categorie) .' - ' .$value->salle_capacite; ?> personnes</p>
                <p style="margin-top:10px;"><img src="<?php echo PATH;?>img/icons/middle_key.png" alt="Disponibilité" class="icon"> <?php if($value->salle_dispo) echo '<span style="color:green;">Offres disponibles</span>'; else echo '<span class="red">Aucune offre disponible</span>'; ?></p>
            </div>
            <div class="room-details-right">
                <p><img src="<?php echo PATH;?>img/icons/middle_description.png" alt="Description" class="icon">
                    <?php echo ucfirst($value->salle_description); ?>
                </p>
            </div>

        </div>
    <?php }?>

    <?php endif;?>

<?php endif;?>


</div>

<script>
    <?php
        if(isset($param['filtre']['defaut'])) echo 'var filtreDefaut ="'.$param['filtre']['defaut'] .'";'; else echo 'var filtreDefaut = "categorie"';
 ?>
</script>