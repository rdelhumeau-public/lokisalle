<div id="page">

    <h1><img src="<?php echo PATH;?>img/icons/title_profile.png" alt="" class="icon2">Profil :</h1>
    <div class="hr"></div>

    <div class="side border">

        <div id="profil-infos">
            <h2>
                <div id="avatar"><img src="<?php echo PATH;?>img/icons/<?php if($_SESSION['membre_sexe'] == 'h') echo 'man'; else echo 'woman';?>.png" alt=""></div>
                <?php echo $_SESSION['membre_pseudo'];?>
            </h2>
            <div class="hr"></div>
            <?php if($_SESSION['membre_statut'] > 4):?>
            <p class="red"><img src="<?php echo PATH;?>img/icons/middle_crown.png" alt="" class="icon2"><strong>Administrateur</strong></p>
            <?php endif;?>

            <p><img src="<?php echo PATH;?>img/icons/small_id.png" alt="" class="icon2"><?php echo $_SESSION['membre_nom'] .' ' .$_SESSION['membre_prenom'];?></p>
            <p><img src="<?php echo PATH;?>img/icons/arobase_black.png" alt="" class="icon2"><?php echo $_SESSION['membre_email'];?></p>
            <p><img src="<?php echo PATH;?>img/icons/postal_black.png" alt="" class="icon2"><?php echo htmlspecialchars($_SESSION['membre_adresse']);?></p>
            <p><img src="<?php echo PATH;?>img/icons/small_city.png" alt="" class="icon2"><?php echo $_SESSION['membre_cp'] .' ' .$_SESSION['membre_ville'];?></p>

            <div id="dashboard">
                <div id="edit-profil"><img src="<?php echo PATH;?>img/icons/small_edit.png" alt="" class="icon2">Modifier mes informations</div>
                <div id="panier-link"><a href="<?php echo HOME;?>panier"><img src="<?php echo PATH;?>img/icons/small_panier_black.png" alt="" class="icon2">Voir mon panier</a></div>
                <div id="deco-link"><a href="<?php echo HOME;?>membre/deconnexion"><img src="<?php echo PATH;?>img/icons/small_logout.png" alt="" class="icon2">Me déconnecter</a></div>
            </div>
        </div>

    </div>

    <div class="side">
        <div id="stats">
            <h2><img src="<?php echo PATH;?>img/icons/middle_stats.png" alt="" class="icon2">Statistiques :</h2>
            <div class="hr"></div>
            <p>Inscrit depuis le : <strong><?php echo $param['date'];?></strong></p>
            <p><br>Commandes effectuées : <strong><?php if(!empty($param['nbCommandes'])) echo $param['nbCommandes']; else echo '0';?></strong></p>
            <p>Salles notées : <strong><?php if(!empty($param['notes'])) echo $param['notes']; else echo '0';?></strong></p>
            <p>Avis postés : <strong><?php if(!empty($param['avis'])) echo $param['avis']; else echo '0';?></strong></p>
            <p>Nombre de visites : <strong><?php echo $param['connexions'];?></strong></p>
        </div>

    </div>


    <div id="profil-commandes">
        <h2><img src="<?php echo PATH;?>img/icons/middle_panier.png" alt="" class="icon2">Historique de mes commandes :</h2>
        <div class="hr"></div>

        <table>
            <thead><tr>
                <th>Réf.</th><th>Date</th><th>Détails de la commande</th><th>Montant</th>
            </tr></thead>
            <?php
                if(isset($param['commandes']) && !empty($param['commandes'])){
                    foreach($param['commandes'] as $key => $commande){
                        echo '<tr><td>C0'.$commande->ref .'</td><td>' .$commande->commande_date
                            .'<span class="small"> (' .$commande->commande_date_heure.')</span></td><td>'
                            .'<a href="'.HOME.'commande/facture/'.$commande->commande_id .'">Voir la facture</a></td><td>'
                            .number_format($commande->commande_montant, 2, ',', ' ').' &euro;</td></tr>';
                    }
                }
                else{
                    echo '<tr><td colspan="4">Vous n\'avez passé aucune commande.</td></tr>';
                }
            ?>
        </table>

    </div>

    <img src="<?php echo PATH;?>img/main_room02.png" alt="" class="photo">



    <div id="popin-profil">
       <img src="<?php echo PATH;?>img/icons/close.png" alt="" title="Fermer" id="close">
        <div id="lokisalle"><img src="<?php echo PATH;?>img/lokisalle.png" alt=""></div>
        <h1>Modifier mon profil</h1>
        <div class="hr"></div>

        <form action="<?php echo HOME;?>membre/editer" method="post" id="form_profiledit">
            <div class="form-side">
                <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_city_white.png" alt="" class="icon2"><input type="text" name="edit_ville" value="<?php echo $_SESSION['membre_ville'];?>" placeholder="Ville" id="edit_ville"></div>
                <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_postal_white.png" alt="" class="icon2"><input type="text" name="edit_cp" placeholder="Code Postal" value="<?php echo $_SESSION['membre_cp'];?>" id="edit_cp"></div>
                <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_mailbox_white.png" alt="" class="icon2"><textarea name="edit_adresse" placeholder="Adresse" id="edit_adresse"><?php echo htmlspecialchars($_SESSION['membre_adresse']);?></textarea></div>

                <input type="hidden" name="membre_id" value="<?php echo $_SESSION['membre_id'];?>" id="membre_id">
            </div>
            <div class="form-side">
                <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_email_white.png" alt="" class="icon2"><input type="mail" name="edit_email" placeholder="Adresse email" value="<?php echo $_SESSION['membre_email'];?>" id="edit_email"></div>

                <div id="changeMdp">
                    <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_lock_white.png" alt="" class="icon2"><input type="password" name="edit_ancien_mdp" placeholder="Votre mot de passe actuel" id="edit_ancien_mdp"></div>
                    <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_lock_white.png" alt="" class="icon2"><input type="password" name="edit_nouveau_mdp" placeholder="Votre nouveau mot de passe" id="edit_nouveau_mdp"></div>
                    <div class="field-container"><img src="<?php echo PATH;?>img/icons/middle_lock_white.png" alt="" class="icon2"><input type="password" name="edit_nouveau_mdpConfirmation" placeholder="Confirmez votre mot de passe" id="edit_nouveau_mdpConfirmation"></div>
                </div>

                <div id="changeMdpLink"><img src="<?php echo PATH;?>img/icons/middle_lock_white.png" alt="" class="icon2">Modifier mon mot de passe</div>

            </div>
        </form>

        <div id="submit">Valider</div>
    </div>


</div>

<?php

    if(isset($param['inscription']) && $param['inscription']){
        echo '<script>var inscriptionValid = true;</script>';
    }
    else{
        echo '<script>var inscriptionValid = false;</script>';
    }
?>