<header>

    <div class="main-container">

       <a href="<?php echo HOME;?>"><img src="<?php echo PATH ?>img/lokisalle.png" id="logo" alt="Lokisalle"></a>
        <div id="legend">Réservez les clés de la réussite !</div>

        <div id="right-header">
            <?php if(isset($_SESSION['membre_id'])):?>
                <a href="<?php echo HOME;?>profil"><div id="profil"><img src="<?php echo PATH;?>img/icons/profile.png" alt="">Profil</div></a>
                <a href="<?php echo HOME;?>membre/deconnexion"><div id="logout"><img src="<?php echo PATH;?>img/icons/logout.png" alt="">Déconnexion</div></a>
            <?php else:?>
            <a href="<?php echo HOME;?>inscription"><div id="subscribe"><img src="<?php echo PATH;?>img/icons/subscribe.png" alt="">Inscription</div></a>
            <a href="<?php echo HOME;?>connexion"><div id="login"><img src="<?php echo PATH;?>img/icons/login.png" alt="">Connexion</div></a>
            <?php endif;?>
        </div>

    </div>
</header>

<nav>
    <div class="main-container">

    <div class="menu-container">
        <?php if($view == 'index'): ?>
            <div class="menu-actif"><img src="<?php echo PATH ?>img/icons/home.png" alt="">Accueil</div>
        <?php else:?>
            <a href="<?php echo HOME;?>"><div class="menu"><img src="<?php echo PATH ?>img/icons/home.png" alt="">Accueil</div></a>
        <?php endif; ?>

    </div><div class="menu-container">

        <?php if($view == 'reservation' || (preg_match('#reservation#', $_SESSION['current_page']) && isset($_SESSION['previous_page']) && !preg_match('#recherche#', $_SESSION['previous_page']))): ?>
            <a href="<?php echo HOME;?>reservation"><div class="menu-actif clicker"><img src="<?php echo PATH ?>img/icons/key.png" alt="">Réservation</div></a>
        <?php else:?>
            <a href="<?php echo HOME;?>reservation"><div class="menu"><img src="<?php echo PATH ?>img/icons/key.png" alt="">Réservation</div></a>
        <?php endif; ?>

    </div><div class="menu-container">

        <?php if($view == 'recherche' || ($view !="reservation" && preg_match('#reservation#', $_SESSION['current_page']) && isset($_SESSION['previous_page']) && preg_match('#recherche#', $_SESSION['previous_page']))): ?>
            <a href="<?php echo HOME;?>recherche"><div class="menu-actif clicker"><img src="<?php echo PATH ?>img/icons/search.png" alt="">Recherche</div></a>
        <?php else:?>
            <a href="<?php echo HOME;?>recherche"><div class="menu"><img src="<?php echo PATH ?>img/icons/search.png" alt="">Recherche</div></a>
        <?php endif; ?>

    </div><div class="menu-container">

        <?php if($view == 'contact'): ?>
            <div class="menu-actif"><img src="<?php echo PATH ?>img/icons/contact.png" alt="">Contact</div>
        <?php else:?>
            <a href="<?php echo HOME;?>contact"><div class="menu"><img src="<?php echo PATH ?>img/icons/contact.png" alt="">Contact</div></a>
        <?php endif; ?>
    </div><?php if(isset($_SESSION['membre_id']) && $_SESSION['membre_statut'] > 4):?><div class="menu-container">

            <?php if($view == 'admin' || preg_match('#admin#', $_SESSION['current_page'])): ?>
                <a href="<?php echo HOME;?>admin"><div class="menu-actif clicker"><img src="<?php echo PATH ?>img/icons/title_admin.png" alt="">Admin</div></a>
            <?php else:?>
                <a href="<?php echo HOME;?>admin"><div class="menu"><img src="<?php echo PATH ?>img/icons/title_admin.png" alt="">Admin</div></a>
            <?php endif; ?>
        </div>
        <?php endif;?>

    </div>

    <a href="<?php echo HOME;?>panier" title="Voir mon panier">
    <div id="panier">
        <?php if(isset($_SESSION['panier']) && isset($_SESSION['membre_id'])) echo number_format($_SESSION['panier']['ttc'],2,',',' ') .' &euro;';?>
        <img src="<?php echo PATH;?>img/icons/menu_panier.png" alt="Panier">
        <?php if(isset($_SESSION['panier']['articles']) && isset($_SESSION['membre_id'])) echo '<div id="nbarticles">'.count($_SESSION['panier']['articles']) .'</div>'; elseif(isset($_SESSION['membre_id'])) echo '<div id="paniervide">0</div>';?>
    </div>
    </a>
</nav>