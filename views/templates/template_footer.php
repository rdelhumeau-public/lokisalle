<footer>
    <div class="main-container">
        <img src="<?php echo PATH;?>img/logo.png" alt="" id="logo-footer">

        <div class="footer-coord">
            <p style="font-size:18px;font-weight:bold;">Loki<span style="color:#a96c47">salle</span></p>
            <p>1 rue Boswellia</p>
            <p>75001 Paris</p>
            <p>01 47 20 00 01</p>
        </div>

        <div class="footer-links">
            <a href="<?php echo HOME;?>mentions-legales"><p>Mentions légales</p></a>
            <a href="<?php echo HOME;?>conditions-generales"><p>Conditions générales</p></a>
        </div>
        <div class="footer-links">
            <a href="<?php echo HOME;?>plan-du-site"><p>Plan du site</p></a>
            <p id="print">Imprimer cette page</p>
        </div>
        <div class="footer-links last">
            <a href="<?php echo HOME;?>newsletter"><p>S'inscrire à la newsletter</p></a>
            <a href="<?php echo HOME;?>contact"><p>Nous contacter</p></a>
        </div>

        <div id="up-container"><a href="#top" class="scrollTo"><div id="up"><img src="<?php echo PATH;?>img/icons/up.png" alt=""></div></a></div>

        <div class="copyright">©Lokisalle - Raphaël Delhumeau - Tous droits réservés - 2015</div>
    </div>
</footer>

<div class="hidden">
    <img src="<?php echo PATH;?>img/icons/subscribe_hover.png" alt="">
    <img src="<?php echo PATH;?>img/icons/login_hover.png" alt="">
    <img src="<?php echo PATH;?>img/icons/logout_hover.png" alt="">
    <img src="<?php echo PATH;?>img/icons/profile_hover.png" alt="">
</div>