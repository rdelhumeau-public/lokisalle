<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title><?php if(isset($param['title']) && $param['title'] != '') echo $param['title'] .' | '; ?>Lokisalle</title>
    <meta name="keywords" content="lokisalle, location, salle, location de salle, paris, france">
    <meta name="description" content="Lokisalle, location de salles dans toute la France.">

    <meta property="og:title" content="Lokisalle" />
    <meta property="og:description" content="Lokisalle, location de salles dans toute la France" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://lokisalle.raphaeldelhumeau.com" />

    <link rel="shortcut icon" href="<?php echo PATH ?>favicon.ico">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH ?>vendor/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH ?>vendor/jquery-ui/jquery-ui.structure.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH ?>vendor/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo PATH ?>css/main.css">

    <?php if(file_exists('css/'.$view .'.css')):?>
        <link rel="stylesheet" type="text/css" href="<?php echo PATH ?>css/<?php echo $view;?>.css">
    <?php endif;?>

    <script src="<?php echo PATH ?>vendor/jquery-2.1.3.js"></script>
    <script src="<?php echo PATH ?>vendor/jquery-ui/jquery-ui.min.js"></script>
</head>
<body id="top">

<?php require 'template_header.php'; ?>

<div id="page-container">
    <div id="content">
    <?php echo $content; ?>
    </div>
</div>

<?php require 'template_footer.php'; ?>

<script>var path = '<?php echo HOME ?>';
<?php if(isset($_SESSION['panier']) && isset($_SESSION['membre_id'])) echo 'var panier = true;'; else echo 'var panier = false;';?>
<?php if(isset($_SESSION['membre_id'])) echo 'var connected = true;'; else echo 'var connected = false;';?>
</script>
<script src="<?php echo PATH ?>js/script_main.js"></script>
<?php if(file_exists('js/script_'.$view .'.js')):?>
    <script src="<?php echo PATH ?>js/script_<?php echo $view;?>.js"></script>
<?php endif;?>
</body>
</html>