DROP TABLE IF EXISTS `lokisalle_membre`;
CREATE TABLE `lokisalle_membre` (
  `membre_id` int(11) NOT NULL AUTO_INCREMENT,
  `membre_pseudo` varchar(30) DEFAULT NULL,
  `membre_mdp` varchar(120) DEFAULT NULL,
  `membre_nom` varchar(30) DEFAULT NULL,
  `membre_prenom` varchar(30) DEFAULT NULL,
  `membre_email` varchar(60) DEFAULT NULL,
  `membre_sexe` enum ('h','f') DEFAULT NULL,
  `membre_ville` varchar(30) DEFAULT NULL,
  `membre_cp` int(5) DEFAULT NULL,
  `membre_adresse` varchar(80) DEFAULT NULL,
  `membre_statut` int(2) DEFAULT NULL,
  `membre_dateInscription` datetime DEFAULT NULL,
  `membre_ip` varchar(30) DEFAULT NULL,
  `membre_connexions` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`membre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

