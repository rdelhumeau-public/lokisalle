DROP TABLE IF EXISTS `lokisalle_newsletter`;
CREATE TABLE `lokisalle_newsletter` (
  `newsletter_id` INT(11) NOT NULL AUTO_INCREMENT,
  `newsletter_membre_id` INT(11) DEFAULT NULL,
  PRIMARY KEY (`newsletter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

