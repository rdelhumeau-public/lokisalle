DROP TABLE IF EXISTS `lokisalle_commande`;
CREATE TABLE `lokisalle_commande` (
  `commande_id` INT(11) NOT NULL AUTO_INCREMENT,
  `commande_montant` FLOAT DEFAULT NULL,
  `commande_membre_id` INT(11) DEFAULT NULL,
  `commande_date` DATETIME DEFAULT NULL,
  PRIMARY KEY (`commande_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

