DROP TABLE IF EXISTS `lokisalle_produit`;
CREATE TABLE `lokisalle_produit` (
  `produit_id` INT(11) NOT NULL AUTO_INCREMENT,
  `produit_arrivee` DATETIME DEFAULT NULL,
  `produit_depart` DATETIME DEFAULT NULL,
  `produit_salle_id` INT(11) DEFAULT NULL,
  `produit_promo_id` INT(11) DEFAULT NULL,
  `produit_prix` INT(11) DEFAULT NULL,
  `produit_etat` INT(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`produit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

