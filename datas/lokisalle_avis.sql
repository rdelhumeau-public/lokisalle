DROP TABLE IF EXISTS `lokisalle_avis`;
CREATE TABLE `lokisalle_avis` (
  `avis_id` INT(11) NOT NULL AUTO_INCREMENT,
  `avis_membre_id` INT(11) DEFAULT NULL,
  `avis_salle_id` INT(11) DEFAULT NULL,
  `avis_texte` TEXT DEFAULT NULL,
  `avis_note` INT(5) DEFAULT NULL,
  `avis_date` DATETIME DEFAULT NULL,
  PRIMARY KEY (`avis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

