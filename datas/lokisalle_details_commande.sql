DROP TABLE IF EXISTS `lokisalle_details_commande`;
CREATE TABLE `lokisalle_details_commande` (
  `details_commande_id` INT(11) NOT NULL AUTO_INCREMENT,
  `details_commande_commande_id` INT(11) DEFAULT NULL,
  `details_commande_produit_id` INT(11) DEFAULT NULL,
  PRIMARY KEY (`details_commande_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

