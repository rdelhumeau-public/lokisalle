DROP TABLE IF EXISTS `lokisalle_salle`;
CREATE TABLE `lokisalle_salle` (
  `salle_id` int(11) NOT NULL AUTO_INCREMENT,
  `salle_pays` varchar(30) DEFAULT NULL,
  `salle_ville` text DEFAULT NULL,
  `salle_adresse` varchar(30) DEFAULT NULL,
  `salle_cp` varchar(5) DEFAULT NULL,
  `salle_titre` varchar(200) DEFAULT NULL,
  `salle_description` text DEFAULT NULL,
  `salle_photo` varchar(250) DEFAULT NULL,
  `salle_capacite` int(3) DEFAULT NULL,
  `salle_categorie` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`salle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

