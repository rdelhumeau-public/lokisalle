<?php

namespace Model;

class CommandeEntity extends Database{


    public function getField($field, $cid){

        $field = 'commande_' .$field;

        $database = $this->dbConnection();
        $request = $database->query("SELECT $field FROM lokisalle_commande WHERE commande_id = $cid ");

        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result[0]->$field;
    }


    public function setField($field, $value, $cid){

        $field = 'commande_' .$field;

        $database = $this->dbConnection();
        $request = $database->prepare("UPDATE lokisalle_commande SET $field = :val WHERE commande_id = :cid ");
        $request->execute(array(
            'val' => $value,
            'cid' => $cid
        ));
    }


    public function enregistrer(){

        $montant = $_SESSION['panier']['ttc'];
        $membre = $_SESSION['membre_id'];

        $database = $this->dbConnection();
        $request = $database->prepare("INSERT INTO lokisalle_commande (commande_montant, commande_membre_id, commande_date)
                                            VALUES (:montant, :mid, NOW()) ");
        $request->execute(array(
            'montant' => $montant,
            'mid' => $membre
        ));

        $cid = $this->getId();

        foreach($_SESSION['panier']['articles'] as $key => $pid) {
            $detailsRequest = $database->prepare("INSERT INTO lokisalle_details_commande (details_commande_commande_id, details_commande_produit_id)
                                            VALUES (:cid,:pid) ");
            $detailsRequest->execute(array(
                'cid' => $cid,
                'pid' => $pid
            ));
        }

    }

    public function getId(){
        $database = $this->dbConnection();
        $idRequest = $database->query("SELECT commande_id FROM lokisalle_commande ORDER BY commande_id DESC LIMIT 1");
        return $idRequest->fetchAll(\PDO::FETCH_CLASS)[0]->commande_id;
    }


    public function getCommandes($mid = ''){

        if($mid == ''){
            $mid = $_SESSION['membre_id'];
        }

        $database = $this->dbConnection();
        $request = $database->query("SELECT commande_id, commande_montant, DATE_FORMAT(commande_date, '%d/%m/%Y') AS commande_date,
                                        DATE_FORMAT(commande_date, '%Hh%i') AS commande_date_heure
                                        FROM lokisalle_commande WHERE commande_membre_id = $mid");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result;
    }


    public function getFullCommande($cid){

        $database = $this->dbConnection();
        $request = $database->query("SELECT commande_id, commande_membre_id, commande_montant, DATE_FORMAT(commande_date, '%d/%m/%Y') AS commande_date,
                                        DATE_FORMAT(commande_date, '%Hh%i') AS commande_date_heure,
                                        produit_id, DATE_FORMAT(produit_arrivee, '%d/%m/%Y') AS produit_arrivee_date,
                                        DATE_FORMAT(produit_arrivee, '%Hh%i') AS produit_arrivee_heure,
                                        DATE_FORMAT(produit_depart, '%d/%m/%Y') AS produit_depart_date,
                                        DATE_FORMAT(produit_depart, '%Hh%i') AS produit_depart_heure,
                                        produit_prix, produit_promo_id, produit_salle_id, salle_titre, salle_capacite, salle_adresse,
                                        salle_ville, salle_cp, salle_categorie
                                        FROM lokisalle_commande INNER JOIN lokisalle_details_commande
                                        ON commande_id = details_commande_commande_id AND commande_id = $cid
                                        INNER JOIN lokisalle_produit ON details_commande_produit_id = produit_id
                                        INNER JOIN lokisalle_salle ON produit_salle_id = salle_id");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result;
    }




    public function getAllCommandes(){

        $database = $this->dbConnection();
        $request = $database->query("SELECT membre_pseudo, commande_id, commande_montant, DATE_FORMAT(commande_date, '%d/%m/%Y') AS commande_date,
                                        DATE_FORMAT(commande_date, '%Hh%i') AS commande_date_heure
                                        FROM lokisalle_commande INNER JOIN lokisalle_membre ON commande_membre_id = membre_id ORDER BY commande_id DESC");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result;
    }

}