<?php

    namespace Model;

    class AvisEntity extends Database{


        public function getField($field, $aid){

            $field = 'avis_' .$field;

            $database = $this->dbConnection();
            $request = $database->query("SELECT $field FROM lokisalle_avis WHERE avis_id = $aid ");

            $result = $request->fetchAll(\PDO::FETCH_CLASS);
            return $result[0]->$field;
        }


        public function setField($field, $value, $aid){

            $field = 'avis_' .$field;

            $database = $this->dbConnection();
            $request = $database->prepare("UPDATE lokisalle_avis SET $field = :val WHERE avis_id = :aid ");
            $request->execute(array(
                'val' => $value,
                'aid' => $aid
            ));
        }


        public function enregistrer($sid){
            $avis = trim($_POST['avis_texte']);
            $note = null;
            $mid = $_SESSION['membre_id'];

            if($_POST['avis_note'] != '-'){
                $note = $_POST['avis_note'];
            }

            $database = $this->dbConnection();
            $request = $database->prepare("INSERT INTO lokisalle_avis (avis_membre_id, avis_salle_id, avis_texte, avis_note, avis_date)
                                            VALUES (:mid, :sid, :avis, :note, NOW()) ");
            $request->execute(array(
                'mid' => $mid,
                'sid' => $sid,
                'avis' => $avis,
                'note' => $note
            ));

        }


        public function getAvisBySalleId($sid){

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT membre_pseudo, avis_texte, avis_note, DATE_FORMAT(avis_date, '%d/%m/%Y') AS avis_date,
                                            DATE_FORMAT(avis_date, '%Hh%i') AS avis_heure FROM lokisalle_avis
                                            INNER JOIN lokisalle_membre ON avis_membre_id = membre_id AND avis_salle_id = :sid
                                            ORDER BY avis_id DESC");
            $request->execute(array(
                'sid' => $sid
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result;
        }


        public function getNbAvisByUid($uid = ''){

            if($uid == ''){
                $uid = $_SESSION['membre_id'];
            }

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT COUNT(avis_id) AS nb_avis FROM lokisalle_avis
                                            INNER JOIN lokisalle_membre ON avis_membre_id = membre_id AND avis_membre_id = :uid");
            $request->execute(array(
                'uid' => $uid
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result[0]->nb_avis;
        }


        public function getNbNotesByUid($uid = ''){

            if($uid == ''){
                $uid = $_SESSION['membre_id'];
            }

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT COUNT(avis_id) AS nb_notes FROM lokisalle_avis
                                            INNER JOIN lokisalle_membre ON avis_membre_id = membre_id AND avis_note IS NOT NULL
                                            AND avis_membre_id = :uid");
            $request->execute(array(
                'uid' => $uid
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result[0]->nb_notes;
        }


        public function hasAlreadyPosted($sid, $uid = ''){

            if($uid == ''){
                $uid = $_SESSION['membre_id'];
            }

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT avis_id FROM lokisalle_avis WHERE avis_salle_id = :sid AND avis_membre_id = :uid");
            $request->execute(array(
                'sid' => $sid,
                'uid' => $uid
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result;

        }



        public function getAvis(){
            $database = $this->dbConnection();
            $request = $database->query("SELECT avis_id, avis_texte, avis_note, DATE_FORMAT(avis_date, '%d/%m/%Y') AS avis_date,
                                            DATE_FORMAT(avis_date, '%Hh%i') AS avis_heure, membre_pseudo, salle_titre FROM lokisalle_avis
                                            INNER JOIN lokisalle_membre ON avis_membre_id = membre_id
                                            INNER JOIN lokisalle_salle ON avis_salle_id = salle_id ORDER BY avis_id DESC");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);
            return $result;
        }

        public function deleteById($aid){
            $database = $this->dbConnection();
            $database->query("DELETE FROM lokisalle_avis WHERE avis_id = $aid");
        }
        

    }