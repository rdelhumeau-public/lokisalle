<?php

    namespace Model;

    use \Controller\Tools as Tools;

    class ProduitEntity extends Database{


        public function getField($field, $pid){

            $field = 'produit_' .$field;

            $database = $this->dbConnection();
            $request = $database->query("SELECT $field FROM lokisalle_produit WHERE produit_id = $pid ");

            $result = $request->fetchAll(\PDO::FETCH_CLASS);
            return $result[0]->$field;
        }


        public function setField($field, $value, $pid){

            $field = 'produit_' .$field;

            $database = $this->dbConnection();
            $request = $database->prepare("UPDATE lokisalle_produit SET $field = :val WHERE produit_id = :pid ");
            $request->execute(array(
                'val' => $value,
                'pid' => $pid
            ));
        }


        public function getProduitById($pid){

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT produit_id, DATE_FORMAT(produit_arrivee, '%d/%m/%Y') AS produit_arrivee_date,
                                            DATE_FORMAT(produit_arrivee, '%Hh%i') AS produit_arrivee_heure,
                                            DATE_FORMAT(produit_depart, '%d/%m/%Y') AS produit_depart_date,
                                            DATE_FORMAT(produit_depart, '%Hh%i') AS produit_depart_heure,
                                            produit_prix, produit_promo_id, produit_salle_id, salle_ville, salle_adresse,
                                            salle_cp, salle_titre, salle_photo, salle_capacite, salle_categorie FROM lokisalle_produit
                                            INNER JOIN lokisalle_salle ON produit_salle_id = salle_id AND produit_id = :pid");
            $request->execute(array(
                'pid' => $pid,
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result[0];
        }


        public function getProduitsBySalleId($sid){

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT produit_id, DATE_FORMAT(produit_arrivee, '%d/%m/%Y') AS produit_arrivee_date,
                                            DATE_FORMAT(produit_arrivee, '%Hh%i') AS produit_arrivee_heure,
                                            DATE_FORMAT(produit_depart, '%d/%m/%Y') AS produit_depart_date,
                                            DATE_FORMAT(produit_depart, '%Hh%i') AS produit_depart_heure,
                                            produit_prix, produit_promo_id, produit_salle_id
                                            FROM lokisalle_produit
                                            INNER JOIN lokisalle_salle ON produit_salle_id = salle_id AND salle_id = :sid
                                            AND produit_etat = 0 AND produit_arrivee > NOW() ORDER BY produit_arrivee");
            $request->execute(array(
                'sid' => $sid,
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result;
        }


        public function searchProduitsByFilter($category, $value, $limit = '', $sid = ''){
            $category = 'salle_' .$category;

            if($sid !=''){
                $sid = 'AND salle_id != '.$sid;
            }

            if($limit != ''){
                $limit = 'LIMIT ' .$limit;
            }

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT DATE_FORMAT(produit_arrivee, '%d/%m/%Y') AS produit_arrivee_date,
                                            DATE_FORMAT(produit_arrivee, '%Hh%i') AS produit_arrivee_heure,
                                            DATE_FORMAT(produit_depart, '%d/%m/%Y') AS produit_depart_date,
                                            DATE_FORMAT(produit_depart, '%Hh%i') AS produit_depart_heure,
                                            produit_prix, produit_promo_id, produit_salle_id, salle_titre,
                                            salle_ville, salle_capacite, salle_categorie FROM lokisalle_produit
                                            INNER JOIN lokisalle_salle ON produit_salle_id = salle_id AND $category = :value
                                            AND produit_etat = 0 AND produit_arrivee > NOW() $sid GROUP BY produit_salle_id
                                            ORDER BY produit_arrivee_date $limit");
            $request->execute(array(
                'value' => $value,
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result;
        }


        public function getLastProduits($limit = ''){

            if($limit != ''){
                $limit = 'LIMIT ' .$limit;
            }
            $database = $this->dbConnection();
            $request = $database->query("SELECT DATE_FORMAT(produit_arrivee, '%d/%m/%Y') AS produit_arrivee_date,
                                            DATE_FORMAT(produit_arrivee, '%Hh%i') AS produit_arrivee_heure,
                                            DATE_FORMAT(produit_depart, '%d/%m/%Y') AS produit_depart_date,
                                            DATE_FORMAT(produit_depart, '%Hh%i') AS produit_depart_heure,
                                            produit_id, produit_prix, produit_salle_id, salle_titre,
                                            salle_ville, salle_capacite, salle_categorie FROM lokisalle_produit
                                            INNER JOIN lokisalle_salle ON produit_salle_id = salle_id
                                            AND produit_etat = 0 AND produit_arrivee > NOW() GROUP BY produit_salle_id
                                            ORDER BY produit_id DESC $limit");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result;
        }


        public function isReserved($pid){

            $database = $this->dbConnection();
            $request = $database->query("SELECT produit_id FROM lokisalle_produit WHERE produit_id = $pid AND produit_etat = 1 ");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);
            return $result;
        }

        public function setReserved($pid){

            $database = $this->dbConnection();
            $database->query("UPDATE lokisalle_produit SET produit_etat = 1 WHERE produit_id = $pid ");
        }


        public function searchSalles($category, $value){

            if($category == 'prix'){
                $category = 'produit_' .$category;
            }
            elseif($category == 'date'){
                $category = 'produit_arrivee';
            }
            else{
                $category = 'salle_' .$category;
            }

            switch($category){
                case 'salle_categorie':
                    $condition = $category .' = "' .$value .'"';
                    $desc = '';
                    break;
                case 'salle_ville':
                    $condition = $category .' = "' .$value .'"';
                    $desc = '';
                    break;
                case 'salle_capacite':
                    $condition = $category .' <= ' .$value;
                    $desc = 'DESC';
                    break;
                case 'produit_prix':
                    $condition = $category .' <= ' .$value;
                    $desc = 'DESC';
                    break;
                case 'produit_arrivee':
                    $condition = 'LEFT('.$category .',7) = "' .$value .'"';
                    $desc = '';
                    break;
            }

            $database = $this->dbConnection();
            $request = $database->query("SELECT DATE_FORMAT(produit_arrivee, '%d/%m/%Y') AS produit_arrivee_date,
                                            DATE_FORMAT(produit_arrivee, '%Hh%i') AS produit_arrivee_heure,
                                            DATE_FORMAT(produit_depart, '%d/%m/%Y') AS produit_depart_date,
                                            DATE_FORMAT(produit_depart, '%Hh%i') AS produit_depart_heure,
                                            produit_prix, produit_promo_id, produit_salle_id, salle_titre,
                                            salle_ville, salle_capacite, salle_categorie, salle_adresse, salle_description,
                                            salle_cp, salle_id FROM lokisalle_produit
                                            INNER JOIN lokisalle_salle ON produit_salle_id = salle_id AND $condition
                                            AND produit_arrivee > NOW() GROUP BY produit_salle_id
                                            ORDER BY $category $desc");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result;
        }



        public function getProduits($orderby = '', $desc = true){

            if($orderby == ''){
                $orderby = 'produit_id';
            }
            if($desc){
                $desc = 'DESC';
            }
            else{
                $desc = '';
            }
            $database = $this->dbConnection();
            $request = $database->query("SELECT DATE_FORMAT(produit_arrivee, '%d/%m/%Y') AS produit_arrivee_date,
                                            DATE_FORMAT(produit_arrivee, '%Hh%i') AS produit_arrivee_heure,
                                            DATE_FORMAT(produit_depart, '%d/%m/%Y') AS produit_depart_date,
                                            DATE_FORMAT(produit_depart, '%Hh%i') AS produit_depart_heure,
                                            produit_id, produit_prix, produit_salle_id, produit_etat, salle_titre,
                                            salle_ville, salle_capacite, salle_categorie FROM lokisalle_produit
                                            INNER JOIN lokisalle_salle ON produit_salle_id = salle_id
                                            AND produit_arrivee > NOW()
                                            ORDER BY $orderby $desc");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result;
        }


        public function deleteById($pid){

            $database = $this->dbConnection();
            $database->query("DELETE FROM lokisalle_produit WHERE produit_id = $pid ");
        }


        public function updateById($pid){

            $sid = trim($_POST['produit_salle_id']);
            $prix = trim($_POST['produit_prix']);
            $arrivee = Tools::dateConvert(trim($_POST['produit_arrivee']), 'en');
            $depart =  Tools::dateConvert(trim($_POST['produit_depart']), 'en');

            $arrivee = $arrivee .' 08:00:00';
            $depart = $depart .' 18:00:00';


            $database = $this->dbConnection();
            $request = $database->prepare("UPDATE lokisalle_produit SET produit_salle_id = :sid, produit_prix = :prix, produit_arrivee = :arrivee, produit_depart = :depart WHERE produit_id = :pid ");
            $request->execute(array(
                'sid' => $sid,
                'prix' => $prix,
                'arrivee' => $arrivee,
                'depart' => $depart,
                'pid' => $pid
            ));
        }


        public function addProduit(){

            $sid = trim($_POST['produit_salle_id']);
            $prix = trim($_POST['produit_prix']);
            $arrivee = Tools::dateConvert(trim($_POST['produit_arrivee']), 'en');
            $depart =  Tools::dateConvert(trim($_POST['produit_depart']), 'en');

            $arrivee = $arrivee .' 08:00:00';
            $depart = $depart .' 18:00:00';


            $database = $this->dbConnection();
            $request = $database->prepare("INSERT INTO lokisalle_produit (produit_salle_id, produit_prix, produit_arrivee, produit_depart, produit_etat)
                                            VALUES (:sid, :prix, :arrivee, :depart, '0') ");
            $request->execute(array(
                'sid' => $sid,
                'prix' => $prix,
                'arrivee' => $arrivee,
                'depart' => $depart,
            ));
        }


        public function dateIsReserved($arrivee, $depart, $sid, $pid = ''){

            $arrivee = $arrivee .' 08:00:00';
            $depart = $depart .' 18:00:00';

            if($pid !=''){
                $pid = 'AND produit_id != ' .$pid;
            }

            $database = $this->dbConnection();
            $request = $database->query("SELECT produit_id FROM lokisalle_produit WHERE (produit_arrivee BETWEEN '$arrivee' AND '$depart'
                                          OR produit_depart BETWEEN '$arrivee' AND '$depart') AND produit_salle_id = $sid $pid");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);
            return $result;
        }

    }