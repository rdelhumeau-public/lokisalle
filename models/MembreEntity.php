<?php

    namespace Model;

    class MembreEntity extends Database{


        public function getField($field, $uid = ''){

            if($uid == ''){
                $uid = $_SESSION['membre_id'];
            }

            $field = 'membre_' .$field;

            $database = $this->dbConnection();
            $request = $database->query("SELECT $field FROM lokisalle_membre WHERE membre_id = $uid ");

            $result = $request->fetchAll(\PDO::FETCH_CLASS);
            return $result[0]->$field;
        }


        public function setField($field, $value, $uid = ''){

            if($uid == ''){
                $uid = $_SESSION['membre_id'];
            }

            $field = 'membre_' .$field;

            $database = $this->dbConnection();
            $request = $database->prepare("UPDATE lokisalle_membre SET $field = :val WHERE membre_id = :uid ");
            $request->execute(array(
                'val' => $value,
                'uid' => $uid
            ));
        }


        public function getMembreById($uid){

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT * FROM lokisalle_membre WHERE membre_id = :uid");
            $request->execute(array(
                'uid' => $uid,
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result;
        }


        public function connexion($pseudo = '', $mdp = ''){

            if(isset($_POST['membre_pseudo']) && isset($_POST['membre_mdp'])){
                $pseudo = trim($_POST['membre_pseudo']);
                $mdp = trim($_POST['membre_mdp']);
            }

            if(isset($_POST['connexion_pseudo']) && isset($_POST['connexion_mdp'])){
                $pseudo = trim($_POST['connexion_pseudo']);
                $mdp = trim($_POST['connexion_mdp']);
            }

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT * FROM lokisalle_membre WHERE membre_pseudo = :pseudo");
            $request->execute(array(
                'pseudo' => $pseudo,
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            if($result && !password_verify($mdp, $result[0]->membre_mdp)){
                return 'wrong_mdp';
            }
            else {
                return $result;
            }
        }


        public function setSession($donneesMembre){
            $_SESSION['membre_id'] = $donneesMembre[0]->membre_id;
            $_SESSION['membre_pseudo'] = $donneesMembre[0]->membre_pseudo;
            $_SESSION['membre_nom'] = $donneesMembre[0]->membre_nom;
            $_SESSION['membre_prenom'] = $donneesMembre[0]->membre_prenom;
            $_SESSION['membre_email'] = $donneesMembre[0]->membre_email;
            $_SESSION['membre_sexe'] = $donneesMembre[0]->membre_sexe;
            $_SESSION['membre_ville'] = $donneesMembre[0]->membre_ville;
            $_SESSION['membre_cp'] = $donneesMembre[0]->membre_cp;
            $_SESSION['membre_adresse'] = $donneesMembre[0]->membre_adresse;
            $_SESSION['membre_statut'] = $donneesMembre[0]->membre_statut;
        }


        public function emailExists($email){

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT membre_id FROM lokisalle_membre WHERE membre_email = :email");
            $request->execute(array('email' => $email));

            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            if($result){
                return $result[0]->membre_id;
            }
            else{
                return false;
            }
        }

        public function pseudoExists($pseudo){

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT membre_id FROM lokisalle_membre WHERE membre_pseudo = :pseudo");
            $request->execute(array('pseudo' => $pseudo));

            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            if($result){
                return $result[0]->membre_id;
            }
            else{
                return false;
            }
        }

        public function getDate($uid = ''){

            if($uid == ''){
                $uid = $_SESSION['membre_id'];
            }

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT DATE_FORMAT(membre_dateInscription, '%d/%m/%Y') AS membre_dateInscription
                                            FROM lokisalle_membre WHERE membre_id = :uid");
            $request->execute(array('uid' => $uid));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result[0]->membre_dateInscription;
        }


        public function enregistrement(){

            $pseudo = trim($_POST['membre_pseudo']);
            $mdp = password_hash(trim($_POST['membre_mdp']), PASSWORD_BCRYPT);
            $nom = trim($_POST['membre_nom']);
            $prenom = trim($_POST['membre_prenom']);
            $email = trim($_POST['membre_email']);
            $sexe = trim($_POST['membre_sexe']);
            $ville= trim($_POST['membre_ville']);
            $cp = trim($_POST['membre_cp']);
            $adresse = trim($_POST['membre_adresse']);
            $ip = $_SERVER['REMOTE_ADDR'];

            $database = $this->dbConnection();
            $request = $database->prepare("INSERT INTO lokisalle_membre (membre_pseudo, membre_mdp, membre_nom, membre_prenom,
                                            membre_email, membre_sexe, membre_ville, membre_cp, membre_adresse, membre_statut,
                                            membre_dateInscription, membre_ip)
                                            VALUES (:pseudo, :mdp, :nom, :prenom, :email, :sexe, :ville, :cp, :adresse,
                                                    :statut, NOW(), :ip) ");
            $request->execute(array(
                'pseudo' => $pseudo,
                'mdp' => $mdp,
                'nom' => $nom,
                'prenom' => $prenom,
                'email' => $email,
                'sexe' => $sexe,
                'ville' => $ville,
                'cp' => $cp,
                'adresse' => $adresse,
                'statut' => 0,
                'ip' => $ip
            ));

        }


        public function connectionCount(){

            $uid = $_SESSION['membre_id'];
            $database = $this->dbConnection();
            $database->query("UPDATE lokisalle_membre SET membre_connexions = membre_connexions + 1 WHERE membre_id = $uid ");
        }


        public function modifier(){

            if(!empty($_POST) && isset($_POST['edit_ville'])){

                $ville= trim($_POST['edit_ville']);
                $cp = trim($_POST['edit_cp']);
                $adresse = trim($_POST['edit_adresse']);
                $email = trim($_POST['edit_email']);

                $uid = $_SESSION['membre_id'];

                $database = $this->dbConnection();
                $request = $database->prepare("UPDATE lokisalle_membre SET membre_ville = :ville, membre_cp = :cp,
                                                membre_adresse = :adresse, membre_email = :email WHERE membre_id = :uid ");
                $request->execute(array(
                    'ville' => $ville,
                    'cp' => $cp,
                    'adresse' => $adresse,
                    'email' => $email,
                    'uid' => $uid
                 ));

                if($_POST['edit_ancien_mdp'] != '' && $_POST['edit_nouveau_mdp'] != '') {
                    $mdp = password_hash(trim($_POST['edit_nouveau_mdp']), PASSWORD_BCRYPT);

                    $mdpRequest = $database->prepare("UPDATE lokisalle_membre SET membre_mdp = :mdp WHERE membre_id = :uid ");
                    $mdpRequest->execute(array(
                        'mdp' => $mdp,
                        'uid' => $uid
                    ));
                }

                $_SESSION['membre_ville'] = $ville;
                $_SESSION['membre_cp'] = $cp;
                $_SESSION['membre_adresse'] = $adresse;
                $_SESSION['membre_email'] = $email;

            }
        }


        public function getAdminEmail(){
            $database = $this->dbConnection();
            $request = $database->query("SELECT membre_email FROM lokisalle_membre WHERE membre_statut = 10");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result[0]->membre_email;
        }

        public function getAdmin(){
            $database = $this->dbConnection();
            $request = $database->query("SELECT membre_id, membre_pseudo, membre_email FROM lokisalle_membre WHERE membre_statut = 10");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result[0];
        }



        public function getMembres(){
            $database = $this->dbConnection();
            $request = $database->query("SELECT membre_id, membre_email, membre_pseudo, membre_nom, membre_prenom,
                                          membre_statut, membre_sexe, membre_ville, membre_cp, membre_adresse
                                          FROM lokisalle_membre ORDER BY membre_id DESC");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);
            return $result;
        }


        public function deleteByPseudo($pseudo){

            $database = $this->dbConnection();
            $database->query("DELETE FROM lokisalle_membre WHERE membre_pseudo = '$pseudo'");
        }


    }