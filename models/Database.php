<?php

    namespace Model;

    class Database{

        public function dbConnection(){

            include ROOT .'config/database.php';

            try {
                $database = new \PDO("mysql:dbname=$db[dbname];host=$db[host]", $db['user'], $db['psw']);
                $database->query('SET NAMES UTF8');
                return $database;
            }
            catch (\Exception $e){
                die('Erreur : ' . $e->getMessage());

            }
        }
    }
?>