<?php

    namespace Model;

    class SalleEntity extends Database{


        public function getField($field, $sid){

            $field = 'salle_' .$field;

            $database = $this->dbConnection();
            $request = $database->query("SELECT $field FROM lokisalle_salle WHERE salle_id = $sid ");

            $result = $request->fetchAll(\PDO::FETCH_CLASS);
            return $result[0]->$field;
        }


        public function setField($field, $value, $sid){

            $field = 'salle_' .$field;

            $database = $this->dbConnection();
            $request = $database->prepare("UPDATE lokisalle_salle SET $field = :val WHERE salle_id = :sid ");
            $request->execute(array(
                'val' => $value,
                'sid' => $sid
            ));
        }


        public function getSalleById($sid){

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT * FROM lokisalle_salle WHERE salle_id = :sid");
            $request->execute(array(
                'sid' => $sid,
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result[0];
        }


        public function getSalleByName($name){

            $name = ucfirst(urldecode($name));

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT * FROM lokisalle_salle WHERE salle_titre = :name");
            $request->execute(array(
                'name' => $name,
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result[0];
        }


        public function getSalles($orderby = '', $limit = ''){

            if($limit != ''){
                $limit = 'LIMIT ' .$limit;
            }

            if($orderby == ''){
                $orderby = 'salle_id';
            }

            $database = $this->dbConnection();
            $request = $database->query("SELECT * FROM lokisalle_salle ORDER BY $orderby DESC $limit");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result;
        }


        public function searchSallesByFilter($category, $value, $limit = '', $sid = ''){
            $category = 'salle_' .$category;

            if($sid !=''){
                $sid = 'AND salle_id != '.$sid;
            }

            if($limit != ''){
                $limit = 'LIMIT ' .$limit;
            }

            $database = $this->dbConnection();
            $request = $database->prepare("SELECT * FROM lokisalle_salle WHERE $category = :value $sid $limit");
            $request->execute(array(
                'value' => $value,
            ));
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result;
        }


        public function getList($field){

            $field = 'salle_' .$field;

            $database = $this->dbConnection();
            $request = $database->query("SELECT DISTINCT $field FROM lokisalle_salle GROUP BY $field");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            $list = array();
            foreach($result as $key => $value){
                $list[] = $value->$field;
            }

            return $list;
        }


        public function getIdByName($name){

            $database = $this->dbConnection();
            $request = $database->query("SELECT salle_id FROM lokisalle_salle WHERE salle_titre = '$name'");
            $result = $request->fetchAll(\PDO::FETCH_CLASS);

            return $result[0]->salle_id;
        }

        public function deleteById($sid){

            $database = $this->dbConnection();
            $database->query("DELETE FROM lokisalle_salle WHERE salle_id = $sid ");
        }


        public function updateById($sid){

            $titre = trim($_POST['salle_titre']);
            $description = trim($_POST['salle_description']);
            $categorie = trim($_POST['salle_categorie']);
            $capacite = trim($_POST['salle_capacite']);
            $ville = trim($_POST['salle_ville']);
            $cp = trim($_POST['salle_cp']);
            $adresse = trim($_POST['salle_adresse']);

            $database = $this->dbConnection();
            $request = $database->prepare("UPDATE lokisalle_salle SET salle_titre = :titre, salle_description = :description,
                                         salle_categorie = :categorie, salle_capacite = :capacite, salle_ville = :ville,
                                         salle_cp = :cp, salle_adresse = :adresse WHERE salle_id = :sid ");
            $request->execute(array(
                'sid' => $sid,
                'titre' => $titre,
                'description' => $description,
                'categorie' => $categorie,
                'capacite' => $capacite,
                'ville' => $ville,
                'cp' => $cp,
                'adresse' => $adresse
            ));
        }


        public function addSalle(){

            $titre = trim($_POST['salle_titre']);
            $description = trim($_POST['salle_description']);
            $categorie = trim($_POST['salle_categorie']);
            $capacite = trim($_POST['salle_capacite']);
            $ville = trim($_POST['salle_ville']);
            $cp = trim($_POST['salle_cp']);
            $adresse = trim($_POST['salle_adresse']);
            $photo = '';

            if(isset($_FILES['salle_photo'])){
                $photo = PATH .'img/rooms/' .strtolower($titre) .'_preview.jpg';
            }

            $database = $this->dbConnection();
            $request = $database->prepare("INSERT INTO lokisalle_salle (salle_pays,salle_ville,salle_adresse,salle_cp,salle_titre, salle_photo, salle_description,salle_capacite,salle_categorie)
                                            VALUES ('France', :ville, :adresse, :cp, :titre, :photo, :description, :capacite, :categorie)");
            $request->execute(array(
                'ville' => $ville,
                'adresse' => $adresse,
                'cp' => $cp,
                'titre' => $titre,
                'photo' => $photo,
                'description' => $description,
                'capacite' => $capacite,
                'categorie' => $categorie
            ));
        }

    }