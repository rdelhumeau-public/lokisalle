<?php

namespace Model;


class StatsEntity extends Database{


    public function produitsReservedCount(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT COUNT(produit_id) as nbProduits FROM lokisalle_produit WHERE produit_etat = 1");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result[0]->nbProduits;
    }

    public function chiffreAffaire(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT SUM(commande_montant) as chiffreaffaire FROM lokisalle_commande");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result[0]->chiffreaffaire;
    }

    public function sallesCount(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT COUNT(salle_id) as nbSalles FROM lokisalle_salle");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result[0]->nbSalles;
    }

    public function commandesCount(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT COUNT(commande_id) as nbCommandes FROM lokisalle_commande");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result[0]->nbCommandes;
    }

    public function membresCount(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT COUNT(membre_id) as nbMembres FROM lokisalle_membre");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result[0]->nbMembres;
    }

    public function produitsCount(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT COUNT(produit_id) as nbProduits FROM lokisalle_produit");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result[0]->nbProduits;
    }

    public function produitsReservesCount(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT COUNT(produit_id) as nbProduits FROM lokisalle_produit WHERE produit_etat = 1");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result[0]->nbProduits;
    }

    public function avisCount(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT COUNT(avis_id) as nbAvis FROM lokisalle_avis");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result[0]->nbAvis;
    }

    public function nlCount(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT COUNT(newsletter_membre_id) as nbNl FROM lokisalle_newsletter");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result[0]->nbNl;
    }

    public function topVentes(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT salle_titre, salle_ville, COUNT(produit_etat) as nbVentes FROM lokisalle_salle
                                      INNER JOIN lokisalle_produit ON salle_id = produit_salle_id AND produit_etat = 1
                                      GROUP BY produit_salle_id ORDER BY COUNT(produit_salle_id) DESC LIMIT 5");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result;
    }

    public function topClients(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT membre_id, membre_pseudo, membre_ville, COUNT(commande_id) as nbCommandes FROM lokisalle_membre
                                      INNER JOIN lokisalle_commande ON membre_id = commande_membre_id
                                      GROUP BY commande_membre_id ORDER BY COUNT(commande_membre_id) DESC LIMIT 5");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result;
    }

    public function topClientsRentables(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT membre_id, membre_pseudo, membre_ville, SUM(commande_montant) as montantTotal FROM lokisalle_membre
                                      INNER JOIN lokisalle_commande ON membre_id = commande_membre_id
                                      GROUP BY commande_membre_id ORDER BY SUM(commande_montant) DESC LIMIT 5");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result;
    }

}