<?php

namespace Model;


class NewsletterEntity extends Database {


    public function insert($mid = ''){

        if($mid == ''){
            $mid = $_SESSION['membre_id'];
        }

        $database = $this->dbConnection();
        $request = $database->prepare("INSERT INTO lokisalle_newsletter (newsletter_membre_id)
                                            VALUES (:mid) ");
        $request->execute(array(
            'mid' => $mid,
        ));
    }

    public function isRegistered($mid = ''){

        if($mid == ''){
            $mid = $_SESSION['membre_id'];
        }

        $database = $this->dbConnection();
        $request = $database->query("SELECT newsletter_membre_id FROM lokisalle_newsletter WHERE newsletter_membre_id = $mid ");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result;
    }


    public function getEmails(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT membre_email FROM lokisalle_newsletter INNER JOIN lokisalle_membre
                                      ON newsletter_membre_id = membre_id");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result;
    }

    public function getPseudos(){
        $database = $this->dbConnection();
        $request = $database->query("SELECT membre_pseudo FROM lokisalle_newsletter INNER JOIN lokisalle_membre
                                      ON newsletter_membre_id = membre_id");
        $result = $request->fetchAll(\PDO::FETCH_CLASS);
        return $result;
    }


}