<?php

    namespace Controller;

    use \Model\MembreEntity as MembreEntity;
    use \Model\AvisEntity as AvisEntity;
    use \Model\CommandeEntity as CommandeEntity;

    class Profil extends Controller{


        public function view(){
            global $lang;

            if(isset($_SESSION['membre_id'])) {

                $param = array('title' => 'Mon profil');

                if (isset($_SESSION['inscription']) && $_SESSION['inscription']) {
                    $param['inscription'] = true;
                    unset($_SESSION['inscription']);
                }

                $membre = new MembreEntity();
                $param['connexions'] = $membre->getField('connexions');
                $param['date'] = $membre->getDate();

                $avis = new AvisEntity();
                $param['avis'] = $avis->getNbAvisByUid();
                $param['notes'] = $avis->getNbNotesByUid();

                $commandes = new CommandeEntity();
                $param['commandes'] = $commandes->getCommandes();
                //génération de la référence
                foreach($param['commandes'] as $key => $value){
                    $param['commandes'][$key]->ref = Tools::keyGenerate(($value->commande_id));
                }

                $param['nbCommandes'] = count($param['commandes']);

                $this->render('profil', $lang, $param);
            }
            else{
                $this->redirect('');
            }
        }

    }