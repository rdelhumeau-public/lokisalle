<?php

    namespace Controller;

    class Inscription extends Controller{

        public function view(){
            global $lang;

            $param = array('title'=>'Inscription', 'back_url' => HOME);
            $badpage = PATH .'connexion';

            if(isset($_SESSION['previous_page']) && $_SESSION['previous_page'] != $badpage){
                $param['back_url'] = $_SESSION['previous_page'];
            }

            if(isset($_SESSION['membre_id'])){
                $this->redirect('');
            }
            else {
                $this->render('inscription', $lang, $param);
            }
        }

    }