<?php

    namespace Controller;

    use \Model\SalleEntity as SalleEntity;
    use \Model\ProduitEntity as ProduitEntity;
    use \Model\AvisEntity as AvisEntity;

    class Reservation extends Controller{


        public function view(){
            global $lang;

            $sallesRepository = new SalleEntity();
            $salles = $sallesRepository->getSalles();

            //vérification si offres dispos pour chaque salle
            $produitEntity = new ProduitEntity();
            foreach($salles as $key => $salle){
                $produits = $produitEntity->getProduitsBySalleId($salle->salle_id);
                $available = false;
                foreach($produits as $pkey => $produit){
                    if(!$produitEntity->isReserved($produit->produit_id)) {
                        $available = true;
                    }
                }
                if($available){
                    $salles[$key]->salle_dispo = true;
                }
                else{
                    $salles[$key]->salle_dispo = false;
                }
            }

            $param = array('title' => 'Réservation', 'salles' => $salles);
            $param['nbSalles'] = count($salles);

            $this->render('reservation', $lang, $param);
        }


        //details room page

        public function salles(){
            global $lang;
            global $url_request;

            if(!isset($url_request[3])) $this->redirect('');

            //-- salle import
            $salleName = $url_request[3];
            $salleEntity = new SalleEntity();
            $salle = $salleEntity->getSalleByName(urldecode($salleName));

            if(!empty($salle)){

                //-- produits import
                $produitEntity = new ProduitEntity();
                $produits = $produitEntity->getProduitsBySalleId($salle->salle_id);
                $param = array('title' => $salle->salle_titre, 'salle' => $salle, 'produits' => $produits);

                //-- avis import
                $avisEntity = new AvisEntity();
                $avis = $avisEntity->getAvisBySalleId($salle->salle_id);
                if(!empty($avis)){
                    $param['avis'] = $avis;

                    //-- calculs notes
                    $notesCount = 0;
                    $noteTotal = 0;
                    foreach($avis as $key => $value){
                        if($value->avis_note !== null){
                            $notesCount +=1;
                            $noteTotal += $value->avis_note;
                        }
                    }
                    if($notesCount != 0){
                        $param['nbNotes'] = $notesCount;
                        $noteMoyenne = $noteTotal / $notesCount;
                        $param['note'] = (int)$noteMoyenne;
                    }

                    //-- check if user has already posted
                    if(isset($_SESSION['membre_id']) && !empty($avisEntity->hasAlreadyPosted($salle->salle_id))){
                        $param['alreadyPosted'] = true;
                    }
                }

                //-- suggestions import
                $villeSearch = $produitEntity->searchProduitsByFilter('ville', $salle->salle_ville, 3, $salle->salle_id);

                if(!empty($villeSearch)){
                    $param['suggestions'] = $villeSearch;
                    $param['filtre'] = 'ville';
                }else{
                    $categorySearch = $produitEntity->searchProduitsByFilter('categorie', $salle->salle_categorie, 3, $salle->salle_id);
                    if(!empty($categorySearch)){
                        $param['suggestions'] = $categorySearch;
                        $param['filtre'] = 'categorie';
                    }
                }

                $this->render('salles', $lang, $param);
            }
            else{
                $this->redirect('');
            }
        }

    }