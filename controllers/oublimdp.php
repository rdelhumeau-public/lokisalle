<?php

    namespace Controller;

    use \Model\MembreEntity as MembreEntity;

    class Oublimdp extends Controller{


        public function view(){
            global $lang;

            if(isset($_SESSION['membre_id'])){
                $this->redirect('');
            }

            $param = array('title'=>'Mot de passe oublié', 'back_url' => HOME);

            $this->render('oubli_mdp', $lang, $param);

        }


        public function envoyer(){

            if(!empty($_POST) && isset($_POST['oubli_email'])){
                $email = $_POST['oubli_email'];
                $membre = new MembreEntity();
                $donneesMembre = $membre->emailExists($email);

                if($donneesMembre){
                    $membre_id = $donneesMembre;

                    $securityKey = Tools::keyGenerate($membre_id);

                    $mailer = new Mailer();
                    $mailer->sendOubliMdpMail($email, $membre_id, $securityKey);

                    unset($_SESSION['mdp_session_expired']);
                    $_SESSION['mdp_id'] = $membre_id;
                }
            }
            $this->redirect('');
        }


        public function reinitialiser(){
            global $url_request;
            $uid = $url_request[3];
            $securityKey = $url_request[4];

            if(isset($_SESSION['mdp_id']) && $_SESSION['mdp_id'] == $uid && $securityKey == Tools::keyGenerate($uid)){
                $nouveau_mdp = Tools::codeGenerate();
                $membre = new MembreEntity();
                $membre->setField('mdp', password_hash($nouveau_mdp, PASSWORD_BCRYPT), $uid);
                $email = $membre->getField('email', $uid);

                $mailer = new Mailer();
                $mailer->sendNouveauMdpMail($email, $nouveau_mdp);

                unset($_SESSION['mdp_id']);
                $_SESSION['mdp_changed'] = true;
            }
            else{
                unset($_SESSION['mdp_id']);
                $_SESSION['mdp_session_expired'] = true;
            }

            $this->redirect('');
        }

    }