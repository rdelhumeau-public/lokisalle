<?php

    namespace Controller;

    use \Model\MembreEntity as MembreEntity;

    class Membre extends Controller{


        public function connexion(){
            $membre = new MembreEntity();

            if(!empty($_POST)){
                $donneesMembre = $membre->connexion();

                if(!empty($donneesMembre) && $donneesMembre !='wrong_mdp') {
                    $membre->setSession($donneesMembre);
                    $membre->connectionCount();

                    if(isset($_POST['cookie']) && $_POST['cookie'] == 'oui'){
                        setcookie('membre_id', $donneesMembre[0]->membre_id, time()+60*60*24*30*2, '/', null, false, true);
                    }

                    //si on cherche à se connecter depuis la page d'une salle
                    if(isset($_SESSION['previous_previous_page']) && preg_match('#salles#',$_SESSION['previous_previous_page'])){
                        $nextUrl = substr($_SESSION['previous_previous_page'],1);
                        $this->redirect($nextUrl);
                    }
                    elseif(isset($_SESSION['panier'])){
                        $this->redirect('panier');
                    }
                    elseif(isset($_SESSION['panier-request'])){
                        unset($_SESSION['panier-request']);
                        $this->redirect('panier');
                    }
                    else{
                        $this->redirect('profil');
                    }
                }
                elseif($donneesMembre == 'wrong_mdp'){
                    $_SESSION['mdp-error'] = true;
                    $_SESSION['pseudo'] = $_POST['connexion_pseudo'];
                    $this->redirect('connexion');
                }
                else {
                    $_SESSION['pseudo-error'] = $_POST['connexion_pseudo'];
                    $this->redirect('connexion');
                }
            }
            $this->redirect('');
        }


        public function deconnexion(){
            $previousPage = substr($_SESSION['previous_url'], 1);
            setcookie('membre_id', '', time() -3600, '/', null, false, true);
            session_destroy();
            if($previousPage != 'panier') {
                $this->redirect($previousPage);
            }
            else{
                $this->redirect('');
            }
        }


        public function inscription(){

            if(!empty($_POST)){
                $membre = new MembreEntity();

                $validator = false;
                require 'form' .DS .'form_inscription.php';

                if($validator){
                    $membre->enregistrement();
                    $donneesMembre = $membre->connexion();
                    $membre->setSession($donneesMembre);
                    $membre->connectionCount();

                    $mailer = new Mailer();
                    $mailer->sendInscriptionMail($donneesMembre);

                    $_SESSION['inscription'] = true;
                    $this->redirect('profil');

                }else {
                    $this->redirect('');
                }
            }
            else {
                $this->redirect('');
            }
        }


        public function editer(){

            if(!empty($_POST)) {
                $membre = new MembreEntity();

                $validator = false;
                require 'form' . DS . 'form_profiledit.php';

                if ($validator) {
                    $membre->modifier();
                }
                $this->redirect('profil');
            }
            else{
                $this->redirect('');
            }
        }

    }