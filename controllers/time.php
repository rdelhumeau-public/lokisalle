<?php

namespace Controller;

class Time {

    private $year;
    private $month;
    private $monthName;
    private $utf8MonthName;
    private $utf8MonthArray;
    private $day;
    private $weekDay;
    private $hour;
    private $minute;

    public function __construct(){
        $time = localtime(time(), true);

        $this->year = $time['tm_year'] +1900;
        $this->month = $time['tm_mon'] +1;
        $this->day = $time['tm_mday'];

        $months = array('janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet',
                         'aout', 'septembre', 'octobre', 'novembre', 'decembre');
        $this->monthName = $months[$time['tm_mon']];

        $utf8months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet',
            'août', 'septembre', 'octobre', 'novembre', 'décembre');
        $this->utf8MonthArray = $utf8months;
        $this->utf8MonthName = $utf8months[$time['tm_mon']];

        $days = array('dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi');
        $this->weekDay = $days[$time['tm_wday']];

        $this->hour = $time['tm_hour'];
        $this->minute = $time['tm_min'];
    }

    public function timeShow(){
        echo ucfirst($this->weekDay) .' ' .$this->day .' ' .$this->monthName .' ' .$this->year .' : ' .$this->hour .'h' .$this->minute;
    }

    public function isWeekEnd(){
        return in_array($this->weekDay, array('samedi','dimanche'));
    }

    public function getYear(){
        return $this->year;
    }

    public function getMonth(){
        return $this->month;
    }

    public function getMonthName(){
        return $this->monthName;
    }

    public function getUtf8MonthName(){
        return $this->utf8MonthName;
    }

    public function getUtf8MonthsArray(){
        return $this->utf8MonthArray;
    }

    public function getDay(){
        return $this->day;
    }

    public function getWeekDay(){
        return $this->weekDay;
    }

    public function getHour(){
        return $this->hour;
    }

    public function getMinute(){
        return $this->minute;
    }

}