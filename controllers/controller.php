<?php

    namespace Controller;

    class Controller{

        public function render($view, $lang = 'fr', $param = []){
            ob_start();
            include(ROOT .'views'.DS . $lang .DS .'view_' .$view .'.php');
            $content = ob_get_contents();
            ob_end_clean();

            include(ROOT .'views' .DS .'templates' .DS .'template_base.php');
            exit;
        }


        public function redirect($url){
            $target = HOME .$url;
            header("Location: $target ");
            exit;
        }


        public function view(){
            $this->redirect('');
        }


    }
