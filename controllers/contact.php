<?php

    namespace Controller;

    class Contact extends Controller{

        public function view(){
            global $lang;

            $membreEmail = '';

            if(isset($_SESSION['membre_email'])){
                $membreEmail = $_SESSION['membre_email'];
            }

            if(!empty($_POST) && isset($_POST['contact_message'])){
                $validator = false;
                include ROOT. 'controllers' .DS .'form' .DS .'form_contact.php';

                if($validator) {
                    $mailer = new Mailer();

                    $mailer->sendContactMail($email, $subject, $message);

                    $this->render('contact', $lang, array('title' => 'Nous contacter', 'messageSent' => 'true', 'email' => $membreEmail));
                }
                else{
                    $this->render('contact', $lang, array('title' => 'Nous contacter', 'email' => $membreEmail));
                }

            }
            else {
                $this->render('contact', $lang, array('title' => 'Nous contacter', 'email' => $membreEmail));
            }
        }

    }