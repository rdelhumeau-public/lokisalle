<?php

namespace Controller;

use \Model\CommandeEntity as CommandeEntity;


class Commande extends Controller{


    public function facture(){
        global $url_request;
        global $lang;

        if(!isset($_SESSION['membre_id'])){
            $this->redirect('');
        }

        if(isset($url_request[3])){
            $param = array('title' => 'Ma facture');

            $cid = $url_request[3];
            $commande = new CommandeEntity();
            $facture = $commande->getFullCommande($cid);

            if($facture[0]->commande_membre_id != $_SESSION['membre_id'] && $_SESSION['membre_id'] < 5 || empty($facture)){
                $this->redirect('');
            }

            $param['facture'] = $facture;
            //génération de la référence
            $param['facture'][0]->ref = Tools::keyGenerate(($param['facture'][0]->commande_id));


            if(isset($_SESSION['paye'])){
                $param['paye'] = true;
                unset($_SESSION['paye']);
            }

            $this->render('facture', $lang, $param);
        }
        else{
            $this->redirect('');
        }
    }

}