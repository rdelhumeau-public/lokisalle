<?php

    namespace Controller;

    class Avis extends Controller{

        public function Ajouter(){
            global $url_request;
            $salle = urldecode($url_request[3]);
            $sid = $url_request[4];


            if(isset($_POST['avis_texte'])){
                $validator = false;
                require 'form' .DS .'form_avis.php';

                if($validator) {
                    $avisEntity = new \Model\AvisEntity();
                    $avisEntity->enregistrer($sid);

                    $this->redirect('reservation/salles/' .$salle .'#avis');
                }
            }
            else{
                $this->redirect('');
            }

        }
    }