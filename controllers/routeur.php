<?php

#==================
# SESSION RESTORE
#==================

    session_start();

    if(!isset($_SESSION['membre_id']) && isset($_COOKIE['membre_id'])){
        $membreEntity = new Model\MembreEntity();
        $membre = $membreEntity->getMembreById($_COOKIE['membre_id']);
        $membreEntity->setSession($membre);
        $membreEntity->connectionCount();
    }

#==================
# URL MANAGER
#==================

    $url_request = explode("/",$_SERVER['REQUEST_URI']);
    $lang = 'fr';
    $url_index = 1;

    //-- url tracking
    if(isset($_SESSION['current_page'])){
        if(isset($_SESSION['previous_page'])){
            $_SESSION['previous_previous_page'] = $_SESSION['previous_page'];
        }
        if($_SESSION['current_page'] != $_SERVER['REQUEST_URI']) {
            $_SESSION['previous_page'] = $_SESSION['current_page'];
        }
        $_SESSION['previous_url'] = $_SESSION['current_page'];
    }
    $_SESSION['current_page'] = $_SERVER['REQUEST_URI'];


    //-- extra characters remove
    for($i=0;$i<3;$i++){
        if (isset($url_request[$i]) && preg_match('#-#', $url_request[$i])) {
            $url_request[$i] = preg_replace('#-#', '', $url_request[$i]);
        }
    }


#==================
# ROUTEUR
#==================

    switch($url_request[$url_index]){

        case '':
            //homepage

            $p = new Controller\Home();
            $p->view();
            break;

        default:

            $page = $url_request[$url_index];

            //-- static pages render
            $staticPages = array('mentionslegales' => 'Mentions Légales', 'conditionsgenerales' => 'Conditions Générales',
                                'plandusite' => 'Plan du site');

            if (isset($staticPages[$page])) {
                $p = new Controller\Controller();
                $p->render($page, $lang, array( 'title' => $staticPages[$page]));
                break;
            }


            //-- check action
            if(isset($url_request[$url_index+1]) && $url_request[$url_index+1] != '') {
                $action = $url_request[$url_index + 1];
            }

            $found = false;

            foreach ($routes as $route => $route_action) {
                if ($route == $page) {

                    if(isset($action) && $action != '') {
                        foreach ($route_action as $indice) {
                            if ($indice == $action) {
                                $page = 'Controller\\' .$page;
                                $p = new $page();
                                $p->$indice();

                                $found = true;
                                break;
                            }
                        }
                    }
                    else{
                        //only view page
                        $page = 'Controller\\' .$page;
                        $p = new $page();
                        $p->view();

                        $found = true;
                        break;
                    }
                }
            }
            if(!$found){
                //no page found
                $p = new Controller\Controller();
                $p->redirect('');
                break;
            }

    }
