<?php

    namespace Controller;

    class Connexion extends Controller{

        public function view(){
            global $lang;

            $param = array('title'=>'Connexion', 'back_url' => HOME, 'pseudo-error' => 'ok', 'mdp-error' => 'ok');
            $badpage = array(PATH .'inscription' => '', PATH .'oubli-mdp' => '', PATH .'panier' => '');

            if(isset($_SESSION['previous_page']) && !isset($badpage[$_SESSION['previous_page']])
            && !preg_match('#ajouter#', $_SESSION['previous_page'])){
                $param['back_url'] = $_SESSION['previous_page'];
            }

            if(isset($_SESSION['pseudo-error'])){
                $param['pseudo-error'] = 'error';
                $param['pseudo'] = $_SESSION['pseudo-error'];
                unset($_SESSION['pseudo-error']);
            }

            if(isset($_SESSION['mdp-error'])){
                $param['mdp-error'] = 'error';
                $param['pseudo'] = $_SESSION['pseudo'];
                unset($_SESSION['mdp-error']);
                unset($_SESSION['pseudo']);
            }

            if(isset($_SESSION['membre_id'])){
                $this->redirect('');
            }
            else {
                $this->render('connexion', $lang, $param);
            }
        }

    }