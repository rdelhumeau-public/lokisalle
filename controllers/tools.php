<?php

    namespace Controller;

    class Tools {

        public static function codeGenerate(){

            $size=12;
            $code="";
            $characters = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f", "g", "h",
                "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");

            for($i=1;$i<=$size;$i++){
                $code .= $characters[array_rand($characters)];
            }
            return $code;
        }


        public static function keyGenerate($n){
            $n = (((($n*8)+15)*6)-6)*27;
            return $n;
        }


        public static function dateConvert($date, $lang = 'fr'){

            if($lang == 'en'){
                $dateConverted = explode("/", $date);
                $dateConverted = $dateConverted[2] . "-" . $dateConverted[1] . "-" . $dateConverted[0];
            }

            if($lang == 'fr') {
                $dateConverted = explode('-', $date);
                $dateConverted = $dateConverted[2] . "/" . $dateConverted[1] . "/" . $dateConverted[0];
            }

            return $dateConverted;
        }


        public static function dateTimeConvert($datetime){
            $datetime = explode(' ', $datetime);
            $dateFr = self::dateConvert($datetime[0]);

            $hoursEn = explode(':', $datetime[1]);
            $hoursFr = $hoursEn[0] .'h' .$hoursEn[1];

            return array($dateFr, $hoursFr);
        }


        public static function tvaGenerate($n){
            $tva = 19.6/100;
            $tvaProduit = $tva * $n;
            $produitTTC = $n + $tvaProduit;

            return array('tva' => $tvaProduit, 'ttc' => $produitTTC);
        }


        public static function idSearch($array, $index){
            $i = 0;
            foreach ($array as $key => $value) {
                if ($key == $index) return $i + 1;
                else $i++;
            }
            return false;
        }


        public static function specialCharsToHtml($string) {
            $string = str_replace(array('à', 'á', 'â', 'ã', 'ä', 'å'), array('&agrave;', '&acute;', '&acirc;', '&atilde;', '&auml;', '&aring;'), $string);
            $string = str_replace(array('æ'), '&aelig;', $string);
            $string = str_replace(array('ç'), '&ccedil;', $string);
            $string = str_replace(array('è', 'é', 'ê', 'ë'), array('&egrave;', '&eacute;', '&ecirc;', '&euml;'), $string);
            $string = str_replace(array('ƒ'), 'f', $string);
            $string = str_replace(array('ì', 'í', 'î', 'ï'), array('&igrave;', '&iacute;', '&icirc;', '&iuml;'), $string);
            $string = str_replace(array('ñ'), array('&ntilde;'), $string);
            $string = str_replace(array('ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ð'), array('&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&oslash;', '&eth;'), $string);
            $string = str_replace(array('œ'), '&oelig;', $string);
            $string = str_replace(array('þ'), 'p', $string);
            $string = str_replace(array('ù', 'ú', 'û', 'ü'), array('&ugrave;', '&uacute;', '&ucirc;', '&uuml;'), $string);
            $string = str_replace(array('ý', 'ÿ'), array('&yacute;', '&yuml;'), $string);
            $string = str_replace(array('š'), 's', $string);

            return $string;
        }

    }