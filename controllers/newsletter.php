<?php

namespace Controller;

use \Model\NewsletterEntity as NewsletterEntity;

class Newsletter extends Controller{


    public function view(){
        global $lang;
        $param = array('title' => 'Newsletter');

        $newsletter = new NewsletterEntity();
        if(isset($_SESSION['membre_id']) && $newsletter->isRegistered()){
            $param['already-registered'] = true;
        }

        $this->render('newsletter', $lang, $param);
    }


    public function inscription(){

        $newsletter = new NewsletterEntity();

        if(!$newsletter->isRegistered()){
            $newsletter->insert();
            $_SESSION['newsletter'] = true;
        }

        $this->redirect('');
    }

}