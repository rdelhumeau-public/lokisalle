<?php

    namespace Controller;

    use \Model\ProduitEntity as ProduitEntity;
    use \Model\SalleEntity as SalleEntity;

    class Recherche extends Controller{


        public function view(){
            global $lang;
            $param = array('title' => 'Recherche');

            $sallesRepository = new SalleEntity();
            $salles = $sallesRepository->getSalles();
            $param['nbSalles'] = count($salles);
            $param['filtres']['villes'] = $sallesRepository->getList('ville');
            $param['filtres']['categories'] = $sallesRepository->getList('categorie');

            $time = new Time();
            $param['filtres']['mois'] = $time->getUtf8MonthName();
            $param['filtres']['annee'] = $time->getYear();
            $param['mois'] = $time->getUtf8MonthsArray();

            if(!empty($_POST)){

                if($_POST['filtre'] == 'categorie' && !empty($_POST['filtre_categorie'])){
                    $filtre = 'categorie';
                    $recherche = $_POST['filtre_categorie'];
                }
                if($_POST['filtre'] == 'ville' && !empty($_POST['filtre_ville'])){
                    $filtre = 'ville';
                    $recherche = $_POST['filtre_ville'];
                }
                if($_POST['filtre'] == 'capacite' && !empty($_POST['filtre_capacite'])){
                    $filtre = 'capacite';
                    $recherche = $_POST['filtre_capacite'];
                }
                if($_POST['filtre'] == 'prix' && !empty($_POST['filtre_prix'])){
                    $filtre = 'prix';
                    $recherche = $_POST['filtre_prix'];
                }
                if($_POST['filtre'] == 'date' && !empty($_POST['filtre_date_mois']) && !empty($_POST['filtre_date_annee'])){
                    $filtre = 'date';
                    $recherche = $_POST['filtre_date_mois'] .'-' .$_POST['filtre_date_annee'];
                }

                $this->redirect('recherche/filtres/' .$filtre .'/' .$recherche);
            }

            $this->render('recherche', $lang, $param);
        }


        public function filtres(){
            global $lang;
            global $url_request;

            if(empty($url_request[3]) || empty($url_request[4])){
                $this->redirect('recherche');
            }

            $param = array('title' => 'Recherche');

            $sallesRepository = new SalleEntity();
            $salles = $sallesRepository->getSalles();
            $param['nbSalles'] = count($salles);
            $param['filtres']['villes'] = $sallesRepository->getList('ville');
            $param['filtres']['categories'] = $sallesRepository->getList('categorie');

            $time = new Time();
            $param['filtres']['mois'] = $time->getUtf8MonthName();
            $param['filtres']['annee'] = $time->getYear();
            $param['mois'] = $time->getUtf8MonthsArray();

            $filtre = urldecode($url_request[3]);
            $recherche = urldecode($url_request[4]);

            if($filtre == 'date'){
                $recherche = explode('-', $recherche);
                $mois = (array_keys($param['mois'], strtolower($recherche[0])));
                $mois = $mois[0] +1;
                if($mois < 10) $mois = '0'.$mois;
                $recherche = $recherche[1] .'-' .$mois;
            }

            $produitEntity = new ProduitEntity();
            $searchSalles = $produitEntity->searchSalles($filtre, $recherche);

            //vérification si offres dispos pour chaque salle
            foreach($searchSalles as $key => $salle){
                $produits = $produitEntity->getProduitsBySalleId($salle->salle_id);
                $available = false;
                foreach($produits as $pkey => $produit){
                    if(!$produitEntity->isReserved($produit->produit_id)) {
                        $available = true;
                    }
                }
                if($available){
                    $searchSalles[$key]->salle_dispo = true;
                }
                else{
                    $searchSalles[$key]->salle_dispo = false;
                }
            }

            if($filtre == 'date'){
                $recherche = urldecode($url_request[4]);
                $recherche = explode('-', $recherche);
            }

            $param['salles'] = $searchSalles;
            $param['nbResultats'] = count($param['salles']);
            $param['filtre']['defaut'] = $filtre;
            $param['valeur']['defaut'] = $recherche;

            $this->render('recherche', $lang, $param);

        }

    }