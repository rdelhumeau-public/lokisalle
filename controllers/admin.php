<?php

namespace Controller;

use \Model\SalleEntity as SalleEntity;
use \Model\ProduitEntity as ProduitEntity;
use \Model\AvisEntity as AvisEntity;
use \Model\CommandeEntity as CommandeEntity;
use \Model\MembreEntity as MembreEntity;
use \Model\NewsletterEntity as NewsletterEntity;
use \Model\StatsEntity as Stats;


class Admin extends Controller{


#===================
# PAGE PRINCIPALE
#===================


    public function view(){
        global $lang;
        $lang .= DS .'admin';
        if(!isset($_SESSION['membre_id']) || $_SESSION['membre_statut'] < 5) $this->redirect('');
        $param = array('title' => 'Administration', 'page-admin' => 'stats');

        $stats = new Stats();
        $param['chiffreAffaire'] = $stats->chiffreAffaire();
        $param['nbSalles'] = $stats->sallesCount();
        $param['nbCommandes'] = $stats->commandesCount();
        $param['nbMembres'] = $stats->membresCount();
        $param['nbProduits'] = $stats->produitsCount();
        $param['nbProduitsReserves'] = $stats->produitsReservesCount();
        $param['nbAvis'] = $stats->avisCount();
        $param['nbNl'] = $stats->nlCount();
        $param['topventes'] = $stats->topVentes();
        $param['topclients'] = $stats->topClients();
        $param['topclientsrentables'] = $stats->topClientsRentables();

        $this->render('admin', $lang, $param);
    }



#===================
# GESTION SALLES
#===================

    public function gestionsalles(){
        global $url_request;
        global $lang;
        $lang .= DS .'admin';
        if(!isset($_SESSION['membre_id']) || $_SESSION['membre_statut'] < 5) $this->redirect('');

        $salleEntity = new SalleEntity();
        $param = array('title' => 'Administration', 'page-admin' => 'salles');

        //suppression d'une salle
        if(isset($url_request[3]) && isset($url_request[4]) && $url_request[3] == 'supprimer'){
            $id = $salleEntity->getIdByName(urldecode($url_request[4]));
            $salleEntity->deleteById($id);
            $this->redirect('admin/gestion-salles');
        }

        //ouverture popin edit
        if(isset($url_request[3]) && isset($url_request[4]) && $url_request[3] == 'editer' && empty($_POST)){
            $_SESSION['editerSalle'] = $url_request[4];
            $this->redirect('admin/gestion-salles');
        }
        if(isset($_SESSION['editerSalle'])){
            $param['salle'] = $salleEntity->getSalleById($_SESSION['editerSalle']);
            unset($_SESSION['editerSalle']);
        }

        //enregistrement des modifs de la salle
        if(isset($url_request[3]) && isset($url_request[4]) && $url_request[3] == 'editer' && !empty($_POST)){
            $validator = true;
            include ROOT . 'controllers' .DS .'form' .DS .'form_salle_edit.php';

            if($validator) {
                if(isset($_FILES)){
                    $photo = ROOT .'web' .DS .'img' .DS .'rooms' .DS .strtolower($_POST['salle_titre']) .'_preview.jpg';
                    move_uploaded_file($_FILES['salle_photo']['tmp_name'],$photo);
                }
                $salleEntity->updateById($url_request[4]);
            }
            else{
                $_SESSION['edit-error'] = true;
            }
            $this->redirect('admin/gestion-salles');
        }
        if(isset($_SESSION['edit-error'])){
            $param['edit-error'] = true;
            unset($_SESSION['edit-error']);
        }

        //ajout d'une salle
        if(isset($url_request[3]) && $url_request[3] == 'ajouter' && !empty($_POST)){
            $validator = true;
            include ROOT . 'controllers' .DS .'form' .DS .'form_salle_edit.php';

            if($validator) {
                if(isset($_FILES)){
                    $photo = ROOT .'web' .DS .'img' .DS .'rooms' .DS .strtolower($_POST['salle_titre']) .'_preview.jpg';
                    move_uploaded_file($_FILES['salle_photo']['tmp_name'],$photo);
                }
                $salleEntity->addSalle();
            }
            else{
                $_SESSION['edit-error'] = true;
            }
            $this->redirect('admin/gestion-salles');
        }

        $param['salles'] = $salleEntity->getSalles();

        $this->render('admin', $lang, $param);
    }



#===================
# GESTION AVIS
#===================

    public function gestionavis(){
        global $lang;
        global $url_request;
        $lang .= DS .'admin';
        if(!isset($_SESSION['membre_id']) || $_SESSION['membre_statut'] < 5) $this->redirect('');
        $param = array('title' => 'Administration', 'page-admin' => 'avis');

        $avis = new AvisEntity();

        //suppression d'une salle
        if(isset($url_request[3]) && isset($url_request[4]) && $url_request[3] == 'supprimer'){
            $avis->deleteById($url_request[4]);
            $this->redirect('admin/gestion-avis');
        }

        $param['avis'] = $avis->getAvis();
        $param['nbAvis'] = count($param['avis']);

        $this->render('admin', $lang, $param);
    }




#===================
# GESTION PRODUITS
#===================

    public function gestionproduits(){
        global $lang;
        global $url_request;
        $lang .= DS .'admin';
        if(!isset($_SESSION['membre_id']) || $_SESSION['membre_statut'] < 5) $this->redirect('');
        $param = array('title' => 'Administration', 'page-admin' => 'produits');

        $stats = new Stats();
        $salleEntity = new SalleEntity();
        $produitEntity = new ProduitEntity();

        //supression d'un produit
        if(isset($url_request[3]) && $url_request[3] == 'supprimer' && isset($url_request[4])){
            $produitEntity->deleteById($url_request[4]);
            $this->redirect('admin/gestion-produits');
        }

        //ouverture popin edit
        if(isset($url_request[3]) && isset($url_request[4]) && $url_request[3] == 'editer' && empty($_POST)){
            $_SESSION['editerProduit'] = $url_request[4];
            $this->redirect('admin/gestion-produits');
        }
        if(isset($_SESSION['editerProduit'])){
            $param['produit'] = $produitEntity->getProduitById($_SESSION['editerProduit']);
            unset($_SESSION['editerProduit']);
        }


        //enregistrement des modifs du produit
        if(isset($url_request[3]) && isset($url_request[4]) && $url_request[3] == 'editer' && !empty($_POST)){

            $arriveeEN = Tools::dateConvert($_POST['produit_arrivee'], 'en');
            $departEN = Tools::dateConvert($_POST['produit_depart'], 'en');

            if($departEN < $arriveeEN || $arriveeEN < date('Y-m-d')
                || $produitEntity->dateIsReserved($arriveeEN, $departEN, $_POST['produit_salle_id'], $url_request[4])){
                $_SESSION['edit-error'] = true;
            }
            else{
                $produitEntity->updateById($url_request[4]);
            }
             $this->redirect('admin/gestion-produits');
        }
        if(isset($_SESSION['edit-error'])){
            $param['edit-error'] = true;
            unset($_SESSION['edit-error']);
        }

        //ajout d'un produit
        if(isset($url_request[3]) && $url_request[3] == 'ajouter' && !empty($_POST)){

            $arriveeEN = Tools::dateConvert($_POST['produit_arrivee'], 'en');
            $departEN = Tools::dateConvert($_POST['produit_depart'], 'en');

            if($departEN < $arriveeEN || $arriveeEN < date('Y-m-d')
                || !empty($produitEntity->dateIsReserved($arriveeEN, $departEN, $_POST['produit_salle_id']))){
                $_SESSION['edit-error'] = true;
            }
            else{
                $produitEntity->addProduit();
            }
            $this->redirect('admin/gestion-produits');
        }


        //tri des produits
        if(isset($url_request[3]) && $url_request[3] == 'tri' && isset($url_request[4]) && isset($url_request[5])){
            $desc = true;
            if($url_request[5] == 'croissant'){
                $desc = false;
            }
            $param['produits'] = $produitEntity->getProduits(urldecode($url_request[4]), $desc);
        }
        else {
            $param['produits'] = $produitEntity->getProduits();
        }

        $param['nbProduits'] = count($param['produits']);
        $param['nbReserved'] = $stats->produitsReservedCount();
        $param['sallesListe'] = $salleEntity->getSalles();
        $param['annee'] = date('Y');
        $param['date'] = date('d/m/Y');

        $this->render('admin', $lang, $param);
    }




#===================
# GESTION COMMANDES
#===================

    public function gestioncommandes(){
        global $lang;
        $lang .= DS .'admin';
        if(!isset($_SESSION['membre_id']) || $_SESSION['membre_statut'] < 5) $this->redirect('');
        $param = array('title' => 'Administration', 'page-admin' => 'commandes');

        $commandes = new CommandeEntity();

        $param['commandes'] = $commandes->getAllCommandes();

        $param['chiffreAffaire'] = 0;
        foreach($param['commandes'] as $key => $commande){
            $param['chiffreAffaire'] += $commande->commande_montant;
            $param['commandes'][$key]->ref = Tools::keyGenerate($commande->commande_id);
        }
        $param['nbCommandes'] = count($param['commandes']);

        $this->render('admin', $lang, $param);
    }




#===================
# GESTION MEMBRES
#===================

    public function gestionmembres(){
        global $lang;
        global $url_request;
        $lang .= DS .'admin';
        if(!isset($_SESSION['membre_id']) || $_SESSION['membre_statut'] < 5) $this->redirect('');
        $param = array('title' => 'Administration', 'page-admin' => 'membres');


        $membreEntity = new MembreEntity();

        //suppression d'un membre
        if(isset($url_request[3]) && $url_request[3] == 'supprimer' && isset($url_request[4])){
            if($url_request[4] != $membreEntity->getAdmin()->membre_pseudo) {
                $membreEntity->deleteByPseudo(urldecode($url_request[4]));
            }
            $this->redirect('admin/gestion-membres');
        }

        //ajout d'un admin
        if(isset($url_request[3]) && $url_request[3] == 'ajouter-admin' && !empty($_POST)){
            $membreEntity->setField('statut', 5, $_POST['membre_id']);
            $this->redirect('admin/gestion-membres');
        }

        $param['membres'] = $membreEntity->getMembres();

        $this->render('admin', $lang, $param);
    }




#===================
# ENVOI NEWSLETTER
#===================

    public function envoinewsletter(){
        global $lang;
        global $url_request;
        $lang .= DS .'admin';
        if(!isset($_SESSION['membre_id']) || $_SESSION['membre_statut'] < 5) $this->redirect('');
        $param = array('title' => 'Administration', 'page-admin' => 'newsletter');

        $stats = new Stats();
        $newsletter = new NewsletterEntity();
        $mailer = new Mailer();

        //envoi
        if(isset($url_request[3]) && $url_request[3] == 'envoyer' && !empty($_POST)){
            $emails = $newsletter->getEmails();
            foreach($emails as $key => $mails){
               $mailer->sendNewsletter($mails->membre_email, $_POST['newsletter_message']);
            }
            $param['envoi'] = true;
        }

        $param['nbNl'] = $stats->nlCount();
        $param['pseudos'] = $newsletter->getPseudos();

        $this->render('admin', $lang, $param);
    }

}