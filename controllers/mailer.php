<?php

    namespace Controller;

    use \Model\MembreEntity as MembreEntity;

    class Mailer {


        public function sendContactMail($email, $subject, $message){

            $membre = new MembreEntity();
            $mail = $membre->getAdminEmail();

            $email = htmlspecialchars(trim($email));
            $message = Tools::specialCharsToHtml(nl2br(htmlspecialchars(trim($message))));

            $object = 'Lokisalle : ' .trim(htmlspecialchars($subject));

            $headers  = 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $headers .= 'Reply-to: ' .$email .'<'.$email .'>' ."\n";
            $headers .= 'From: Lokisalle <contact@lokisalle.com>' . "\n";


            $msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html>
                    <head>
                        <title>Message d\'un visiteur</title>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=8">
                    </head>


                    <body leftmargin="0" topmargin="0" bgcolor="#EFEEEE" marginheight="0" marginwidth="0">
                    <table border="0" height="100%" width="100%" cellPadding="0" cellSpacing="0" style="border-collapse:collapse">
                        <tbody>
                        <tr>
                            <td align="center" valign="top" style="font-family: Arial, sans-serif; color:#693b34; font-size:16px;">
                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" bgcolor="#F9F9F9">
                                    <tbody>
                                    <tr>
                                        <td colspan="3">
                                            <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                <tr height="80"><td style="color:#ffffff;font-size:28px;" align="center">Loki<span style="color:#e0b996;">salle</span></td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" height="30"></td>
                                    </tr>
                                    <tr>
                                        <td width="40"></td>
                                        <td width="520">Vous avez reçu un message d\'un membre ou d\'un visiteur : <br/><br/><br/>
                                        <strong>Adresse de l\'expéditeur : </strong>'.$email .'<br/><br/>
                                        <strong>Objet : </strong> '.$subject .'<br/><br/>
                                        <strong>Message : </strong><br/><br/>'.$message .'
                                        <td width="40"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" height="35"></td>
                                    </tr>
                                    <tr>
                                        <td width="40"></td>
                                        <td width="520" align="right"><strong>Lokisalle.com</strong> </td>
                                        <td width="40"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3" height="30"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="3">
                                            <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                <tr height="80"><td></td></td></tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <!-- TAILLE -->
                                    <tr border="0" bgcolor="#EDEDED">
                                        <td height="1" width="40" border="0"></td>
                                        <td height="1" width="520" border="0"></td>
                                        <td height="1" width="40" border="0"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </body>
                    </html>  ';

            mail($mail, $object, $msg, $headers);
        }


        public function sendInscriptionMail($donneesMembre){
            $pseudo = $donneesMembre[0]->membre_pseudo;
            $prenom = $donneesMembre[0]->membre_prenom;
            $sexe = trim($donneesMembre[0]->membre_sexe);
            $email = trim($donneesMembre[0]->membre_email);

            $pseudo = Tools::specialCharsToHtml(htmlspecialchars(trim($pseudo)));
            $prenom = Tools::specialCharsToHtml(htmlspecialchars(trim($prenom)));

            $cher = 'Cher';
            $seul = 'seul';
            if($sexe == 'f'){
                $cher = 'Ch&egrave;re';
                $seul = 'seule';
            }

            $object = 'Bienvenue sur Lokisalle !';

            $headers  = 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $headers .= 'Reply-to: Lokisalle <contact@lokisalle.com>' ."\n";
            $headers .= 'From: Lokisalle <contact@lokisalle.com>' . "\n";

            $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html>
                        <head>
                            <title>Bienvenue sur Lokisalle !</title>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=8">
                        </head>


                        <body leftmargin="0" topmargin="0" bgcolor="#EFEEEE" marginheight="0" marginwidth="0">
                        <table border="0" height="100%" width="100%" cellPadding="0" cellSpacing="0" style="border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td align="center" valign="top" style="font-family: Arial, sans-serif; color:#693b34; font-size:15px;">
                                    <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" bgcolor="#F9F9F9">
                                        <tbody>
                                        <tr>
                                            <td colspan="3">
                                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                    <tr height="80"><td style="color:#ffffff;font-size:28px;" align="center">Loki<span style="color:#e0b996;">salle</span></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td width="520">' .$cher .' ' .$prenom  .', <br/><br/>
                                                Toute l\'&eacute;quipe de <strong>Lokisalle</strong> vous souhaite la bienvenue sur notre site !<br/><br/>

                                                Connectez-vous et r&eacute;servez en quelques clics la salle id&eacute;ale pour vos r&eacute;unions professionnelles ou priv&eacute;es.
                                                Toutes nos &eacute;quipes sont à votre disposition pour vous permettre de trouver la perle rare.<br/><br/>
                                                Vos identifiants de connexion :<br/><br/>
                                                <strong>Votre pseudo :</strong> ' .$pseudo  .'<br/>
                                                <strong>Votre mot de passe :</strong> vous ' .$seul .' le connaissez.<br/><br/>

                                            &Agrave; bient&ocirc;t !</td>
                                            <td width="40"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" height="35"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td width="520" align="right"><a href="'.HOME .'" style="color:#693b34;text-decoration:none;font-size:20px;font-weight: bold;">Lokisalle.com</a> </td>
                                            <td width="40"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">
                                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                    <tr height="35"><td></td></tr>
                                                    <tr><td align="right" style="color:#ffffff;"><a href="'.HOME .'recherche" style="color:#ffffff;text-decoration:none;">Rechercher une salle</a></td>
                                                        <td width="60" align="center" style="color:#ffffff;">|</td>
                                                        <td align="left" style="color:#ffffff;"><a href="'.HOME .'reservation" style="color:#ffffff;text-decoration:none;">R&eacute;server une salle</a></td>
                                                    <tr height="35"><td></td></tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <!-- TAILLE -->
                                        <tr border="0" bgcolor="#EDEDED">
                                            <td height="1" width="40" border="0"></td>
                                            <td height="1" width="520" border="0"></td>
                                            <td height="1" width="40" border="0"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </body>
                        </html>  ';

            mail($email, $object, $message, $headers);
        }


        public function sendOubliMdpMail($email, $id, $securityKey){

            $object = 'Lokisalle : Demande de nouveau mot de passe';

            $headers  = 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $headers .= 'Reply-to: Lokisalle <contact@lokisalle.com>' ."\n";
            $headers .= 'From: Lokisalle <contact@lokisalle.com>' . "\n";

            $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html>
                        <head>
                            <title>Demande de nouveau mot de passe</title>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=8">
                        </head>


                        <body leftmargin="0" topmargin="0" bgcolor="#EFEEEE" marginheight="0" marginwidth="0">
                        <table border="0" height="100%" width="100%" cellPadding="0" cellSpacing="0" style="border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td align="center" valign="top" style="font-family: Arial, sans-serif; color:#693b34; font-size:15px;">
                                    <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" bgcolor="#F9F9F9">
                                        <tbody>
                                        <tr>
                                            <td colspan="3">
                                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                    <tr height="80"><td style="color:#ffffff;font-size:28px;" align="center">Loki<span style="color:#e0b996;">salle</span></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td width="520">Cher utilisateur, <br/><br/>
                                               Une demande de r&eacute;initialisation de votre mot de passe a &eacute;t&eacute; effectu&eacute;e depuis notre site.
                                            Si vous souhaitez obtenir un nouveau mot de passe, veuillez cliquer sur le lien ci-dessous :
                                            <br/><br/><a href="'.HOME .'oubli-mdp/reinitialiser/' .$id .'/' .$securityKey .'">Obtenir un nouveau mot de passe</a>
                                            <br/><br/>Si vous n\'&ecirc;tes pas à l\'origine de cette op&eacute;ration, merci d\'ignorer ce message.
                                            <br/><br/>Toute l\'&eacute;quipe de <strong>Lokisalle</strong> vous remercie de votre fidélit&eacute; !
                                            <td width="40"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" height="35"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td width="520" align="right"><a href="'.HOME .'" style="color:#693b34;text-decoration:none;font-size:20px;font-weight: bold;">Lokisalle.com</a> </td>
                                            <td width="40"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">
                                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                    <tr height="35"><td></td></tr>
                                                    <tr><td align="right" style="color:#ffffff;"><a href="'.HOME .'recherche" style="color:#ffffff;text-decoration:none;">Rechercher une salle</a></td>
                                                        <td width="60" align="center" style="color:#ffffff;">|</td>
                                                        <td align="left" style="color:#ffffff;"><a href="'.HOME .'reservation" style="color:#ffffff;text-decoration:none;">R&eacute;server une salle</a></td>
                                                    <tr height="35"><td></td></tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <!-- TAILLE -->
                                        <tr border="0" bgcolor="#EDEDED">
                                            <td height="1" width="40" border="0"></td>
                                            <td height="1" width="520" border="0"></td>
                                            <td height="1" width="40" border="0"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </body>
                        </html>  ';

            mail($email, $object, $message, $headers);
        }


        public function sendNouveauMdpMail($email, $mdp){

            $object = 'Lokisalle : Votre nouveau mot de passe';

            $headers  = 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $headers .= 'Reply-to: Lokisalle <contact@lokisalle.com>' ."\n";
            $headers .= 'From: Lokisalle <contact@lokisalle.com>' . "\n";

            $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html>
                        <head>
                            <title>Votre nouveau mot de passe</title>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=8">
                        </head>


                        <body leftmargin="0" topmargin="0" bgcolor="#EFEEEE" marginheight="0" marginwidth="0">
                        <table border="0" height="100%" width="100%" cellPadding="0" cellSpacing="0" style="border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td align="center" valign="top" style="font-family: Arial, sans-serif; color:#693b34; font-size:15px;">
                                    <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" bgcolor="#F9F9F9">
                                        <tbody>
                                        <tr>
                                            <td colspan="3">
                                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                    <tr height="80"><td style="color:#ffffff;font-size:28px;" align="center">Loki<span style="color:#e0b996;">salle</span></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>
                                            <td width="40"></td>
                                            <td width="520">Cher utilisateur, <br/><br/>
                                               Suite &agrave; votre demande, voici votre nouveau mot de passe :
                                            <br/><br/><strong>' .$mdp .'</strong>
                                            <br/><br/>Conservez-le bien.
                                                <br/>Vous pourrez &agrave; nouveau le modifier lors de votre prochaine connexion sur la page de votre profil.
                                            <td width="40"></td>

                                        <tr>
                                            <td colspan="3" height="35"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td width="520" align="right"><a href="'.HOME .'" style="color:#693b34;text-decoration:none;font-size:20px;font-weight: bold;">Lokisalle.com</a> </td>
                                            <td width="40"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">
                                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                    <tr height="35"><td></td></tr>
                                                    <tr><td align="right" style="color:#ffffff;"><a href="'.HOME .'recherche" style="color:#ffffff;text-decoration:none;">Rechercher une salle</a></td>
                                                        <td width="60" align="center" style="color:#ffffff;">|</td>
                                                        <td align="left" style="color:#ffffff;"><a href="'.HOME .'reservation" style="color:#ffffff;text-decoration:none;">R&eacute;server une salle</a></td>
                                                    <tr height="35"><td></td></tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <!-- TAILLE -->
                                        <tr border="0" bgcolor="#EDEDED">
                                            <td height="1" width="40" border="0"></td>
                                            <td height="1" width="520" border="0"></td>
                                            <td height="1" width="40" border="0"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </body>
                        </html>  ';

            mail($email, $object, $message, $headers);
        }


        public function sendCommandeMail($commande){

            $prenom = Tools::specialCharsToHtml($_SESSION['membre_prenom']);
            $sexe = $_SESSION['membre_sexe'];
            $email = $_SESSION['membre_email'];
            $ref = Tools::keyGenerate(($commande[0]->commande_id));

            $tableau = '';

            $total = 0;
            foreach($commande as $key => $article){
                $tableau .= '<tr>';
                $tableau .= '<td align="center"><strong>'.$article->salle_titre .'</strong></td>';
                $tableau .= '<td align="center">'.$article->salle_ville  .'<br>' .$article->salle_capacite .' <span style="font-size:13px;">pers.</span><br>' .ucfirst($article->salle_categorie) .'</td>';
                $tableau .= '<td align="center">Du '.$article->produit_arrivee_date .'<br>à <span style="font-size:13px;">'.$article->produit_arrivee_heure .'</span></td>';
                $tableau .= '<td align="center">Au '.$article->produit_depart_date .'<br>à <span style="font-size:13px;">'.$article->produit_depart_heure .'</span></td>';
                $tableau .= '<td align="right">'.number_format($article->produit_prix, 2, ',', ' ') .' &euro;</td></tr>';
                $tableau .= '<tr><td colspan="5" height="10"></td></tr>';
                $total += $article->produit_prix;
            }

            $tva = Tools::tvaGenerate($total)['tva'];
            $ttc = $total + $tva;

            $tableau = Tools::specialCharsToHtml($tableau);

            $cher = 'Cher';
            if($sexe == 'f'){
                $cher = 'Ch&egrave;re';
            }

            $object = 'Merci pour votre achat sur Lokisalle !';

            $headers  = 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $headers .= 'Reply-to: Lokisalle <contact@lokisalle.com>' ."\n";
            $headers .= 'From: Lokisalle <contact@lokisalle.com>' . "\n";

            $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html>
                        <head>
                            <title>Récapitulatif de votre commande</title>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=8">
                        </head>


                        <body leftmargin="0" topmargin="0" bgcolor="#EFEEEE" marginheight="0" marginwidth="0">
                        <table border="0" height="100%" width="100%" cellPadding="0" cellSpacing="0" style="border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td align="center" valign="top" style="font-family: Arial, sans-serif; color:#693b34; font-size:15px;">
                                    <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" bgcolor="#F9F9F9">
                                        <tbody>
                                        <tr>
                                            <td colspan="3">
                                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                    <tr height="80"><td style="color:#ffffff;font-size:28px;" align="center">Loki<span style="color:#e0b996;">salle</span></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td width="520">'.$cher .' <strong>' .$prenom .'</strong>, <br/>
                                                Merci pour votre achat sur notre site !<br/><br/>
                                                Voici le r&eacute;capitulatif de votre commande :<br/><br/>

                                                <strong>R&eacute;f : </strong>C0'.$ref .'<br/>
                                                <strong>Date : </strong>'.$commande[0]->commande_date .' - <span style="font-size:14px;">' .$commande[0]->commande_date_heure .'</span><br/><br/>
                                            </td>
                                            <td width="40"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td>
                                                <table border="0" width="500" cellPadding="0" cellSpacing="0" style="border-collapse:collapse">'.$tableau .'
                                                <tr>
                                                    <td colspan="5" height="20"></td>
                                                </tr>
                                                <tr><td colspan="5" align="right">
                                                    <strong>Total HT :</strong> '.number_format($total,2,',', ' ') .' &euro;<br>
                                                    <strong>TVA :</strong> '.number_format($tva,2,',', ' ') .' &euro;<br>
                                                </td></tr>
                                                <tr><td colspan="5" height="15"></td></tr>
                                                <tr><td colspan="5" align="right">
                                                    <strong>Total TTC :</strong> '.number_format($ttc,2,',', ' ') .' &euro;<br>
                                                </td></tr>
                                                </table>
                                            </td>
                                            <td width="40"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td>
                                                Vous pouvez retrouver toutes vos factures sur notre site depuis votre profil.<br/><br/>

                                                &Agrave; bient&ocirc;t !
                                            </td>
                                            <td width="40"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" height="35"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td width="520" align="right"><a href="'.HOME .'" style="color:#693b34;text-decoration:none;font-size:20px;font-weight: bold;">Lokisalle.com</a> </td>
                                            <td width="40"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">
                                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                    <tr height="35"><td></td></tr>
                                                    <tr><td align="right" style="color:#ffffff;"><a href="'.HOME .'recherche" style="color:#ffffff;text-decoration:none;">Rechercher une salle</a></td>
                                                        <td width="60" align="center" style="color:#ffffff;">|</td>
                                                        <td align="left" style="color:#ffffff;"><a href="'.HOME .'reservation" style="color:#ffffff;text-decoration:none;">R&eacute;server une salle</a></td>
                                                    <tr height="35"><td></td></tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <!-- TAILLE -->
                                        <tr border="0" bgcolor="#EDEDED">
                                            <td height="1" width="40" border="0"></td>
                                            <td height="1" width="520" border="0"></td>
                                            <td height="1" width="40" border="0"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </body>
                        </html>  ';

            mail($email, $object, $message, $headers);
        }





        public function sendNewsletter($email, $news){

            $email = trim($email);
            $news = Tools::specialCharsToHtml(nl2br(htmlspecialchars(trim($news))));

            $object = 'Actualité Lokisalle';

            $headers  = 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\n";
            $headers .= 'Reply-to: Lokisalle <contact@lokisalle.com>' ."\n";
            $headers .= 'From: Lokisalle <contact@lokisalle.com>' . "\n";

            $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html>
                        <head>
                            <title>Demande de nouveau mot de passe</title>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=8">
                        </head>


                        <body leftmargin="0" topmargin="0" bgcolor="#EFEEEE" marginheight="0" marginwidth="0">
                        <table border="0" height="100%" width="100%" cellPadding="0" cellSpacing="0" style="border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td align="center" valign="top" style="font-family: Arial, sans-serif; color:#693b34; font-size:15px;">
                                    <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" bgcolor="#F9F9F9">
                                        <tbody>
                                        <tr>
                                            <td colspan="3">
                                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                    <tr height="80"><td style="color:#ffffff;font-size:28px;" align="center">Loki<span style="color:#e0b996;">salle</span></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td width="520">Du nouveau sur Lokisalle !<br/><br/>
                                            '.$news .'
                                            <td width="40"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" height="35"></td>
                                        </tr>
                                        <tr>
                                            <td width="40"></td>
                                            <td width="520" align="right"><a href="'.HOME .'" style="color:#693b34;text-decoration:none;font-size:20px;font-weight: bold;">Lokisalle.com</a> </td>
                                            <td width="40"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3" height="30"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">
                                                <table width="600" border="0" cellPadding="0" cellSpacing="0" marginheight="0" marginwidth="0" align="center" style="background-color:#5e342f;">
                                                    <tr height="35"><td></td></tr>
                                                    <tr><td align="right" style="color:#ffffff;"><a href="'.HOME .'recherche" style="color:#ffffff;text-decoration:none;">Rechercher une salle</a></td>
                                                        <td width="60" align="center" style="color:#ffffff;">|</td>
                                                        <td align="left" style="color:#ffffff;"><a href="'.HOME .'reservation" style="color:#ffffff;text-decoration:none;">R&eacute;server une salle</a></td>
                                                    <tr height="35"><td></td></tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <!-- TAILLE -->
                                        <tr border="0" bgcolor="#EDEDED">
                                            <td height="1" width="40" border="0"></td>
                                            <td height="1" width="520" border="0"></td>
                                            <td height="1" width="40" border="0"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </body>
                        </html>  ';

            mail($email, $object, $message, $headers);
        }

    }