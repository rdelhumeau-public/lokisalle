<?php

    /* -------- SERVER CONTROL ---------- */


    $validator = true;

    #===================
    # TITRE
    #===================

    if(isset($_POST['salle_titre'])){
        $titre = trim($_POST['salle_titre']);

        $titre_valid = true;

        $regexp = '#^[a-z 0-9 ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-]{2,30}$#i';
        if (!preg_match($regexp, $titre)) {
            $validator = false;
        }

        if(!$titre_valid){
            $validator = false;
        }

    }


    #===================
    # DESCRIPTION
    #===================

    if(isset($_POST['salle_description'])){
        $description = trim($_POST['salle_description']);

        $description_valid = true;

        $regexp = '#^[a-z 0-9 ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-\^\)\:\,\;!?.\_\=\'\(\]\+\"\&\\r\\n]{6,600}$#i';
        if (!preg_match($regexp, $description)) {
            $validator = false;
        }

        if(!$description_valid){
            $validator = false;
        }

    }


    #===================
    # CATEGORIE
    #===================

    if(isset($_POST['salle_categorie'])){
        $categorie = trim($_POST['salle_categorie']);

        $categorie_valid = true;

        $regexp = '#^[a-z 0-9 ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-]{2,30}$#i';
        if (!preg_match($regexp, $categorie)) {
            $validator = false;
        }

        if(!$categorie_valid){
            $validator = false;
        }

    }


    /*
    #===================
    # CAPACITE
    #===================
    */

    if(isset($_POST['salle_capacite'])){
        $capacite = trim($_POST['salle_capacite']);

        if (!is_int($capacite) && $capacite < 2) {
            $validator = false;
        }
    }


    /*
    #===================
    # VILLE
    #===================
    */

    if(isset($_POST['salle_ville'])){
        $ville = trim($_POST['salle_ville']);

        $regexp = '#^[a-z ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-\'.]{2,30}$#i';
        if (!preg_match($regexp, $ville)) {
            $validator = false;
        }

    }


    /*
    #===================
    # CODE POSTAL
    #===================
    */

    if(isset($_POST['salle_cp'])){
        $cp = trim($_POST['salle_cp']);

        $regexp = '/^[0-9]{5}$/';
        if (!preg_match($regexp, $cp)) {
            $validator = false;
        }

    }



    /*
    #===================
    # ADRESSE
    #===================
    */

    if(isset($_POST['salle_adresse'])){
        $adresse = trim($_POST['salle_adresse']);

        $regexp = '#^[a-z 0-9 ÉéèçÇêôîûïöëùäàüÈÊËÀÁÂÃÄÅÌÍÎÏÑÒÓÔÕÖðñòóôõö\-\^\)\:\,\;!?.\_\=\'\(\]\+\"\&\\r\\n]{6,600}$#i';
        if (!preg_match($regexp, $adresse)) {
            $validator = false;
        }

    }

?>