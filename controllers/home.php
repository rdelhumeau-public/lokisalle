<?php

    namespace Controller;

    class Home extends Controller{


        public function view(){
            global $lang;

            $param = array('first_visit' => false);

            if(!isset($_SESSION['first_visit'])){
                $param['first_visit'] = true;
                $_SESSION['first_visit'] = false;
            }

            if(isset($_SESSION['mdp_changed']) && $_SESSION['mdp_changed']){
                $param['mdp_changed'] = true;
                unset($_SESSION['mdp_changed']);
            }

            if(isset($_SESSION['mdp_session_expired']) && $_SESSION['mdp_session_expired']){
                $param['mdp_session_expired'] = true;
                unset($_SESSION['mdp_session_expired']);
            }

            if(isset($_SESSION['newsletter'])){
                $param['newsletter'] = true;
                unset($_SESSION['newsletter']);
            }

            $produitEntity = new \Model\ProduitEntity();
            $param['lastOffers'] = $produitEntity->getLastProduits(3);

            $this->render('index', $lang, $param);

        }

    }