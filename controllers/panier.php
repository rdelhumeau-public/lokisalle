<?php

namespace Controller;

use \Model\ProduitEntity as Produit;
use \Model\CommandeEntity as Commande;

class Panier extends Controller{

    public function view(){
        global $lang;

        if(!isset($_SESSION['membre_id'])){
            $_SESSION['panier-request'] = true;
            $this->redirect('connexion');
        }
        else{
            $param = array('title' => 'Mon panier');

            if(isset($_SESSION['panier']['articles'])){
                $produitEntity = new Produit();
                foreach($_SESSION['panier']['articles'] as $key => $produitId){
                    $param['produits'][] = $produitEntity->getProduitById($produitId);
                }
            }

            if(isset($_SESSION['already-panier'])){
                $param['already-panier'] = true;
                unset($_SESSION['already-panier']);
            }

            if(isset($_SESSION['panier']['echec'])){
                $produitEntity = new Produit();
                foreach($_SESSION['panier']['echec'] as $key => $produitId) {
                    $param['echec'][] = $produitEntity->getProduitById($produitId);
                }
                unset($_SESSION['panier']['echec']);
            }

            if(isset($_SESSION['paye']) && isset($_SESSION['factureId'])){
                $param['paye'] = true;
                $param['factureId'] = $_SESSION['factureId'];
                unset($_SESSION['factureId']);
            }

            $this->render('panier', $lang, $param);
        }

    }


    public function ajouter(){
        global $url_request;

        if(isset($url_request[3])){
            $produitId = $url_request[3];
        }

        if(isset($_POST['produit_id'])){
            $produitId = $_POST['produit_id'];
        }

        if(isset($produitId)) {
            $produitEntity = new Produit();
            $produit = $produitEntity->getProduitById($produitId);

            if (!empty($produit)) {

                //check si produit déjà dans le panier
                if(isset($_SESSION['panier']['articles'])){
                    foreach($_SESSION['panier']['articles'] as $key => $pid){
                        if($pid == $produitId){
                            $_SESSION['already-panier'] = true;
                            $this->redirect('panier');
                        }
                    }
                }

                $_SESSION['panier']['articles'][] = $produitId;

                if(isset($_SESSION['panier']['total'])){
                    $_SESSION['panier']['total'] += $produit->produit_prix;
                }
                else{
                    $_SESSION['panier']['total'] = $produit->produit_prix;
                }
                $_SESSION['panier']['tva'] = Tools::tvaGenerate(($_SESSION['panier']['total']))['tva'];
                $_SESSION['panier']['ttc'] = $_SESSION['panier']['total'] + $_SESSION['panier']['tva'];


                if(isset($_SESSION['membre_id'])){
                    $this->redirect('panier');
                }
                else{
                    $this->redirect('connexion');
                }
            }
        }

        $this->redirect('');
    }


    public function vider(){
        unset($_SESSION['panier']);
        $this->redirect('panier');
    }


    public function retirer(){
        global $url_request;

        if(isset($url_request[3])){
            $indice = $url_request[3];
            $produitId = $_SESSION['panier']['articles'][$indice];
            unset($_SESSION['panier']['articles'][$indice]);

            //réindexation du panier
            $_SESSION['panier']['articles'] = array_merge($_SESSION['panier']['articles']);

            //recalcul du total du panier
            $produitEntity = new Produit();
            $produit = $produitEntity->getProduitById($produitId);
            $_SESSION['panier']['total'] -= $produit->produit_prix;

            $_SESSION['panier']['tva'] = Tools::tvaGenerate(($_SESSION['panier']['total']))['tva'];
            $_SESSION['panier']['ttc'] = $_SESSION['panier']['total'] + $_SESSION['panier']['tva'];

            //on supprime le panier s'il est vide
            if(empty($_SESSION['panier']['articles'])){
                unset($_SESSION['panier']);
            }

            $this->redirect('panier');
        }
        else{
            $this->redirect('');
        }
    }


    public function payer(){

        $produitEntity = new Produit();
        $validator = true;

        //vérification de la disponibilité des articles
        foreach($_SESSION['panier']['articles'] as $key => $pid){
            if($produitEntity->isReserved($pid)) {
                $validator = false;
                $_SESSION['panier']['echec'][] = $pid;
                unset($_SESSION['panier']['articles'][$key]);
            }
        }

        //si tous les articles sont disponibles, on procède au paiement
        if($validator) {
            foreach($_SESSION['panier']['articles'] as $key => $pid){
                $produitEntity->setReserved($pid);
            }
            $commande = new Commande();
            $commande->enregistrer();

            $_SESSION['factureId'] = $commande->getId();
            $_SESSION['paye'] = true;
            unset($_SESSION['panier']);

            $commandeMail = $commande->getFullCommande($_SESSION['factureId']);
            $mailer = new Mailer();
            $mailer->sendCommandeMail($commandeMail);
        }

        $this->redirect('panier');
    }
}