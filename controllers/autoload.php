<?php

  class Autoload{

      public static function myAutoload($className){

          $check = explode('\\', $className);

          if ($check[0] == 'Model') {
              $path = ROOT . 'models' .DS . ucfirst($check[1]) . '.php';
              require_once $path;
          }
          elseif($check[0] == 'Controller') {
              $path = ROOT . 'controllers' .DS . strtolower($check[1]) . '.php';

              require_once $path;
          }
      }
  }

    spl_autoload_register(array('Autoload', 'myAutoload'));
